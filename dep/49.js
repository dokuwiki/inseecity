/*
  Sources :
  https://www.insee.fr/fr/information/4316069
  http://www.geonames.org/export/codes.html
  http://download.geonames.org/export/dump/
*/

var inseeCityNameLatLonDep;
if (inseeCityNameLatLonDep === undefined)
    inseeCityNameLatLonDep = new Map ();

inseeCityNameLatLonDep[49] = {
    49001: ["Les Alleuds", 47.3148, -0.4166],
    49002: ["Allonnes", 47.3034, 0.0111],
    49003: ["Ambillou-Château", 47.2573, -0.3374],
    49004: ["Andard", 47.4817, -0.3909],
    49005: ["Andigné", 47.6655, -0.7894],
    49006: ["Andrezé", 47.1686, -0.9611],
    49007: ["Angers", 47.4768, -0.5561],
    49008: ["Angrie", 47.5778, -0.9681],
    49009: ["Antoigné", 47.0784, -0.1317],
    49010: ["Armaillé", 47.7134, -1.1378],
    49011: ["Artannes-sur-Thouet", 47.2019, -0.0969],
    49012: ["Aubigné-sur-Layon", 47.2124, -0.4729],
    49013: ["Auverse", 47.5005, 0.0389],
    49014: ["Aviré", 47.7118, -0.7916],
    49015: ["Avrillé", 47.5057, -0.6007],
    49017: ["Baracé", 47.6427, -0.3577],
    49018: ["Baugé", 47.5466, -0.1090],
    49019: ["Bauné", 47.5102, -0.3392],
    49020: ["Beaucouzé", 47.4744, -0.6340],
    49021: ["Beaufort-en-Vallée", 47.4363, -0.2069],
    49022: ["Beaulieu-sur-Layon", 47.3187, -0.6000],
    49023: ["Beaupréau", 47.2125, -0.9836],
    49024: ["Beausse", 47.3243, -0.9360],
    49025: ["Beauvau", 47.5945, -0.2540],
    49026: ["Bécon-les-Granits", 47.4995, -0.7991],
    49027: ["Bégrolles-en-Mauges", 47.1332, -0.9316],
    49028: ["Béhuard", 47.3766, -0.6539],
    49029: ["Blaison-Gohier", 47.3908, -0.3800],
    49030: ["Blou", 47.3630, -0.0536],
    49031: ["Bocé", 47.5102, -0.0810],
    49032: ["La Bohalle", 47.4265, -0.3877],
    49033: ["La Boissière-sur-èvre", 47.3038, -1.0888],
    49034: ["Botz-en-Mauges", 47.3084, -0.9961],
    49035: ["Bouchemaine", 47.4307, -0.6271],
    49036: ["Bouillé-Ménard", 47.7381, -0.9693],
    49037: ["Le Bourg-d'Iré", 47.6705, -0.9691],
    49038: ["Bourg-l'évêque", 47.7329, -1.0148],
    49039: ["Bourgneuf-en-Mauges", 47.3091, -0.8588],
    49040: ["Bouzillé", 47.3390, -1.1079],
    49041: ["Brain-sur-Allonnes", 47.3056, 0.0710],
    49042: ["Brain-sur-l'Authion", 47.4579, -0.4075],
    49043: ["Brain-sur-Longuenée", 47.5817, -0.7735],
    49044: ["Breil", 47.4737, 0.1675],
    49045: ["La Breille-les-Pins", 47.3510, 0.0835],
    49046: ["Brézé", 47.1659, -0.0543],
    49047: ["Brigné", 47.2441, -0.3784],
    49048: ["Briollay", 47.5763, -0.4979],
    49049: ["Brion", 47.4394, -0.1437],
    49050: ["Brissac-Quincé", 47.3416, -0.4413],
    49051: ["Brissarthe", 47.7115, -0.4630],
    49052: ["Broc", 47.5898, 0.1974],
    49053: ["Brossay", 47.1680, -0.2162],
    49054: ["Candé", 47.5585, -1.0350],
    49055: ["Cantenay-épinard", 47.5380, -0.5664],
    49056: ["Carbay", 47.7315, -1.2261],
    49057: ["Cernusson", 47.1678, -0.4821],
    49058: ["Les Cerqueux", 47.0057, -0.6412],
    49059: ["Les Cerqueux-sous-Passavant", 47.1079, -0.4737],
    49060: ["Chacé", 47.2066, -0.0708],
    49061: ["Challain-la-Potherie", 47.6319, -1.0714],
    49062: ["Chalonnes-sous-le-Lude", 47.5572, 0.1760],
    49063: ["Chalonnes-sur-Loire", 47.3494, -0.7726],
    49064: ["Chambellay", 47.6887, -0.6935],
    49065: ["Champigné", 47.6653, -0.5704],
    49066: ["Champ-sur-Layon", 47.2634, -0.5693],
    49067: ["Champteussé-sur-Baconne", 47.6694, -0.6542],
    49068: ["Champtocé-sur-Loire", 47.4299, -0.8746],
    49069: ["Champtoceaux", 47.3218, -1.2605],
    49070: ["Chanteloup-les-Bois", 47.0848, -0.6915],
    49071: ["Chanzeaux", 47.2677, -0.6510],
    49072: ["La Chapelle-du-Genêt", 47.1788, -1.0118],
    49073: ["La Chapelle-Hullin", 47.7577, -1.0674],
    49074: ["La Chapelle-Rousselin", 47.1940, -0.8024],
    49075: ["La Chapelle-Saint-Florent", 47.3266, -1.0573],
    49076: ["La Chapelle-Saint-Laud", 47.6139, -0.2981],
    49077: ["La Chapelle-sur-Oudon", 47.6665, -0.8293],
    49078: ["Charcé-Saint-Ellier-sur-Aubance", 47.3522, -0.4032],
    49079: ["Chartrené", 47.4887, -0.1282],
    49080: ["Châteauneuf-sur-Sarthe", 47.6841, -0.4959],
    49081: ["Châtelais", 47.7621, -0.9347],
    49082: ["Chaudefonds-sur-Layon", 47.3253, -0.7217],
    49083: ["Chaudron-en-Mauges", 47.2830, -0.9640],
    49084: ["Chaumont-d'Anjou", 47.5389, -0.2928],
    49085: ["La Chaussaire", 47.2087, -1.1533],
    49086: ["Chavagnes", 47.2740, -0.4398],
    49087: ["Chavaignes", 47.5440, 0.0423],
    49088: ["Chazé-Henry", 47.7595, -1.1111],
    49089: ["Chazé-sur-Argos", 47.6147, -0.8979],
    49090: ["Cheffes", 47.6253, -0.5199],
    49091: ["Chemellier", 47.3369, -0.3589],
    49092: ["Chemillé", 47.2272, -0.7301],
    49093: ["Chemiré-sur-Sarthe", 47.7535, -0.4327],
    49094: ["Chênehutte-Trèves-Cunault", 47.3060, -0.1916],
    49095: ["Chenillé-Changé", 47.6908, -0.6627],
    49096: ["Cherré", 47.7085, -0.5543],
    49097: ["Cheviré-le-Rouge", 47.5895, -0.1719],
    49098: ["Chigné", 47.5762, 0.1111],
    49099: ["Cholet", 47.0454, -0.8778],
    49100: ["Cizay-la-Madeleine", 47.1826, -0.1849],
    49101: ["Clefs", 47.6270, -0.0666],
    49102: ["Cléré-sur-Layon", 47.0857, -0.4303],
    49103: ["Combrée", 47.7071, -1.0207],
    49104: ["Concourson-sur-Layon", 47.1738, -0.3505],
    49105: ["Contigné", 47.7267, -0.5021],
    49106: ["Corné", 47.4690, -0.3505],
    49107: ["Cornillé-les-Caves", 47.4923, -0.3036],
    49108: ["La Cornuaille", 47.5247, -0.9956],
    49109: ["Coron", 47.1203, -0.6363],
    49110: ["Corzé", 47.5424, -0.3694],
    49111: ["Cossé-d'Anjou", 47.1839, -0.6629],
    49112: ["Le Coudray-Macouard", 47.1825, -0.1294],
    49113: ["Courchamps", 47.1979, -0.1572],
    49114: ["Courléon", 47.3809, 0.1469],
    49115: ["Coutures", 47.3723, -0.3520],
    49116: ["Cuon", 47.4783, -0.0900],
    49117: ["La Daguenière", 47.4273, -0.4430],
    49119: ["Daumeray", 47.6849, -0.3624],
    49120: ["Denée", 47.3777, -0.6099],
    49121: ["Dénezé-sous-Doué", 47.2528, -0.2598],
    49122: ["Dénezé-sous-le-Lude", 47.5381, 0.1231],
    49123: ["Distré", 47.2232, -0.1237],
    49125: ["Doué-la-Fontaine", 47.1914, -0.2783],
    49126: ["Drain", 47.3266, -1.2095],
    49127: ["Durtal", 47.6780, -0.2546],
    49128: ["échemiré", 47.5530, -0.1748],
    49129: ["écouflant", 47.5286, -0.5184],
    49130: ["écuillé", 47.6151, -0.5608],
    49131: ["épieds", 47.1348, -0.0385],
    49132: ["étriché", 47.6598, -0.4522],
    49133: ["Faveraye-Mâchelles", 47.2318, -0.5070],
    49134: ["Faye-d'Anjou", 47.3048, -0.5294],
    49135: ["Feneu", 47.5840, -0.6008],
    49136: ["La Ferrière-de-Flée", 47.7365, -0.8397],
    49137: ["Le Fief-Sauvin", 47.2179, -1.0608],
    49138: ["Fontaine-Guérin", 47.4829, -0.1741],
    49139: ["Fontaine-Milon", 47.5051, -0.2536],
    49140: ["Fontevraud-l'Abbaye", 47.1843, 0.0338],
    49141: ["Forges", 47.2144, -0.2371],
    49142: ["La Fosse-de-Tigné", 47.1756, -0.4356],
    49143: ["Fougeré", 47.6223, -0.1520],
    49144: ["Freigné", 47.5362, -1.1059],
    49145: ["Le Fuilet", 47.2833, -1.1216],
    49147: ["Gée", 47.4675, -0.2305],
    49148: ["Gené", 47.6305, -0.8064],
    49149: ["Gennes", 47.3145, -0.2534],
    49150: ["Genneteil", 47.5836, 0.0465],
    49151: ["Gesté", 47.1822, -1.1125],
    49153: ["Valanjou", 47.2178, -0.6059],
    49154: ["Grézillé", 47.3211, -0.3352],
    49155: ["Grez-Neuville", 47.5951, -0.6959],
    49156: ["Grugé-l'Hôpital", 47.7529, -1.0264],
    49157: ["Le Guédéniau", 47.4967, -0.0375],
    49158: ["L'Hôtellerie-de-Flée", 47.7471, -0.8920],
    49159: ["Huillé", 47.6551, -0.3098],
    49160: ["Ingrandes", 47.4154, -0.9203],
    49161: ["La Jaille-Yvon", 47.7267, -0.6875],
    49162: ["Jallais", 47.1958, -0.8568],
    49163: ["Jarzé", 47.5576, -0.2400],
    49165: ["La Jubaudière", 47.1679, -0.8868],
    49167: ["Juigné-sur-Loire", 47.4001, -0.4845],
    49169: ["La Jumellière", 47.2885, -0.7293],
    49170: ["Juvardeil", 47.6592, -0.5142],
    49171: ["La Lande-Chasles", 47.4614, -0.0676],
    49172: ["Landemont", 47.2641, -1.2379],
    49173: ["Lasse", 47.5442, 0.0059],
    49174: ["Lézigné", 47.6367, -0.2929],
    49175: ["Linières-Bouton", 47.4501, 0.0666],
    49176: ["Le Lion-d'Angers", 47.6307, -0.7498],
    49177: ["Liré", 47.3302, -1.1528],
    49178: ["Loiré", 47.6264, -0.9646],
    49179: ["Le Longeron", 47.0248, -1.0412],
    49180: ["Longué-Jumelles", 47.3984, -0.1075],
    49181: ["Louerre", 47.2963, -0.3158],
    49182: ["Louresse-Rochemenier", 47.2417, -0.3070],
    49183: ["Le Louroux-Béconnais", 47.5190, -0.9013],
    49184: ["Louvaines", 47.6906, -0.8029],
    49185: ["Lué-en-Baugeois", 47.5195, -0.2746],
    49186: ["Luigné", 47.2858, -0.4008],
    49187: ["Marans", 47.6416, -0.8496],
    49188: ["Marcé", 47.5822, -0.3002],
    49189: ["Marigné", 47.7139, -0.6234],
    49190: ["Le Marillais", 47.3591, -1.0669],
    49191: ["Martigné-Briand", 47.2399, -0.4370],
    49192: ["Maulévrier", 47.0185, -0.7681],
    49193: ["Le May-sur-èvre", 47.1327, -0.8752],
    49194: ["Mazé", 47.4603, -0.2842],
    49195: ["Mazières-en-Mauges", 47.0598, -0.7955],
    49196: ["La Meignanne", 47.5216, -0.6698],
    49197: ["Meigné-le-Vicomte", 47.5125, 0.1880],
    49198: ["Meigné", 47.2424, -0.2156],
    49199: ["Melay", 47.1816, -0.7106],
    49200: ["La Membrolle-sur-Longuenée", 47.5607, -0.6834],
    49201: ["La Ménitré", 47.4070, -0.2566],
    49202: ["Méon", 47.4730, 0.1135],
    49204: ["Le Mesnil-en-Vallée", 47.3693, -0.9246],
    49205: ["Miré", 47.7620, -0.4950],
    49206: ["Montfaucon-Montigné", 47.0826, -1.1252],
    49207: ["Montfort", 47.1888, -0.2155],
    49208: ["Montguillon", 47.7346, -0.7547],
    49209: ["Montigné-lès-Rairies", 47.6256, -0.2140],
    49211: ["Montilliers", 47.1873, -0.5164],
    49212: ["Montjean-sur-Loire", 47.3840, -0.8682],
    49213: ["Montpollin", 47.5874, -0.1102],
    49214: ["Montreuil-Juigné", 47.5411, -0.6164],
    49215: ["Montreuil-Bellay", 47.1287, -0.1252],
    49216: ["Montreuil-sur-Loir", 47.5935, -0.3916],
    49217: ["Montreuil-sur-Maine", 47.6603, -0.7114],
    49218: ["Montrevault", 47.2553, -1.0516],
    49219: ["Montsoreau", 47.2135, 0.0519],
    49220: ["Morannes", 47.7255, -0.4031],
    49221: ["Mouliherne", 47.4531, 0.0130],
    49222: ["Mozé-sur-Louet", 47.3588, -0.5689],
    49223: ["Mûrs-Erigné", 47.3952, -0.5535],
    49224: ["Neuillé", 47.3363, -0.0184],
    49225: ["Neuvy-en-Mauges", 47.2681, -0.8125],
    49226: ["Noëllet", 47.6939, -1.0897],
    49227: ["Notre-Dame-d'Allençon", 47.2987, -0.4664],
    49228: ["Noyant", 47.5070, 0.1097],
    49229: ["Noyant-la-Gravoyère", 47.7066, -0.9595],
    49230: ["Noyant-la-Plaine", 47.2765, -0.3599],
    49231: ["Nuaillé", 47.0863, -0.7773],
    49232: ["Nueil-sur-Layon", 47.1268, -0.3922],
    49233: ["Nyoiseau", 47.7107, -0.9075],
    49234: ["Parçay-les-Pins", 47.4314, 0.1444],
    49235: ["Parnay", 47.2209, 0.0040],
    49236: ["Passavant-sur-Layon", 47.1007, -0.3834],
    49237: ["La Pellerine", 47.4591, 0.1314],
    49238: ["Pellouailles-les-Vignes", 47.5323, -0.4492],
    49239: ["Le Pin-en-Mauges", 47.2560, -0.9003],
    49240: ["La Plaine", 47.0673, -0.6288],
    49241: ["Le Plessis-Grammoire", 47.4923, -0.4362],
    49242: ["Le Plessis-Macé", 47.5517, -0.6545],
    49243: ["La Poitevinière", 47.2311, -0.8821],
    49244: ["La Pommeraye", 47.3429, -0.8691],
    49245: ["Pontigné", 47.5501, -0.0485],
    49246: ["Les Ponts-de-Cé", 47.4272, -0.5126],
    49247: ["La Possonnière", 47.3814, -0.7087],
    49248: ["Pouancé", 47.7629, -1.1930],
    49249: ["La Pouëze", 47.5523, -0.8057],
    49250: ["La Prévière", 47.7196, -1.1784],
    49251: ["Pruillé", 47.5847, -0.6590],
    49252: ["Le Puiset-Doré", 47.2364, -1.1425],
    49253: ["Le Puy-Notre-Dame", 47.1140, -0.2290],
    49254: ["Querré", 47.6744, -0.6138],
    49256: ["Rablay-sur-Layon", 47.2836, -0.5879],
    49257: ["Les Rairies", 47.6517, -0.2168],
    49258: ["La Renaudière", 47.1259, -1.0657],
    49259: ["Rochefort-sur-Loire", 47.3508, -0.6504],
    49260: ["La Romagne", 47.0543, -1.0099],
    49261: ["Les Rosiers-sur-Loire", 47.3741, -0.2225],
    49262: ["Rou-Marson", 47.2445, -0.1515],
    49263: ["Roussay", 47.0857, -1.0565],
    49264: ["Saint-André-de-la-Marche", 47.0963, -1.0026],
    49265: ["Saint-Aubin-de-Luigné", 47.3211, -0.6770],
    49266: ["Saint-Augustin-des-Bois", 47.4545, -0.8003],
    49267: ["Saint-Barthélemy-d'Anjou", 47.4738, -0.4855],
    49268: ["Sainte-Christine", 47.2890, -0.8367],
    49269: ["Saint-Christophe-du-Bois", 47.0291, -0.9658],
    49270: ["Saint-Christophe-la-Couperie", 47.2601, -1.1787],
    49271: ["Saint-Clément-de-la-Place", 47.5263, -0.7341],
    49272: ["Saint-Clément-des-Levées", 47.3443, -0.1825],
    49273: ["Saint-Crespin-sur-Moine", 47.1128, -1.1982],
    49274: ["Saint-Cyr-en-Bourg", 47.1946, -0.0504],
    49276: ["Saint-Florent-le-Vieil", 47.3444, -1.0031],
    49277: ["Sainte-Gemmes-d'Andigné", 47.6660, -0.8952],
    49278: ["Sainte-Gemmes-sur-Loire", 47.4281, -0.5757],
    49279: ["Saint-Georges-des-Sept-Voies", 47.3483, -0.2987],
    49280: ["Saint-Georges-du-Bois", 47.4945, -0.2214],
    49281: ["Saint-Georges-des-Gardes", 47.1675, -0.7641],
    49282: ["Saint-Georges-sur-Layon", 47.1958, -0.3827],
    49283: ["Saint-Georges-sur-Loire", 47.4024, -0.7560],
    49284: ["Saint-Germain-des-Prés", 47.4104, -0.8120],
    49285: ["Saint-Germain-sur-Moine", 47.1178, -1.1228],
    49288: ["Saint-Jean-de-la-Croix", 47.4108, -0.5883],
    49289: ["Saint-Jean-de-Linières", 47.4586, -0.6746],
    49290: ["Saint-Jean-des-Mauvrets", 47.3849, -0.4603],
    49291: ["Saint-Just-sur-Dive", 47.1722, -0.0995],
    49292: ["Saint-Lambert-du-Lattay", 47.3002, -0.6403],
    49294: ["Saint-Lambert-la-Potherie", 47.4862, -0.6908],
    49295: ["Saint-Laurent-de-la-Plaine", 47.3073, -0.8003],
    49296: ["Saint-Laurent-des-Autels", 47.2882, -1.1839],
    49297: ["Saint-Laurent-du-Mottay", 47.3506, -0.9560],
    49298: ["Saint-Léger-des-Bois", 47.4625, -0.7276],
    49299: ["Saint-Léger-sous-Cholet", 47.1022, -0.9041],
    49300: ["Saint-Lézin", 47.2504, -0.7750],
    49301: ["Saint-Macaire-en-Mauges", 47.1256, -0.9983],
    49302: ["Saint-Macaire-du-Bois", 47.1133, -0.2896],
    49303: ["Saint-Martin-d'Arcé", 47.5730, -0.0861],
    49304: ["Saint-Martin-de-la-Place", 47.3188, -0.1376],
    49305: ["Saint-Martin-du-Bois", 47.6987, -0.7359],
    49306: ["Saint-Martin-du-Fouilloux", 47.4370, -0.7087],
    49307: ["Saint-Mathurin-sur-Loire", 47.4219, -0.3196],
    49308: ["Saint-Melaine-sur-Aubance", 47.3818, -0.5019],
    49309: ["Saint-Michel-et-Chanveaux", 47.6724, -1.1424],
    49310: ["Saint-Paul-du-Bois", 47.0892, -0.5419],
    49311: ["Saint-Philbert-du-Peuple", 47.3908, -0.0435],
    49312: ["Saint-Philbert-en-Mauges", 47.1556, -1.0201],
    49313: ["Saint-Pierre-Montlimart", 47.2757, -1.0335],
    49314: ["Saint-Quentin-en-Mauges", 47.2888, -0.8988],
    49315: ["Saint-Quentin-lès-Beaurepaire", 47.6371, -0.1061],
    49316: ["Saint-Rémy-en-Mauges", 47.2668, -1.0843],
    49317: ["Saint-Rémy-la-Varenne", 47.3878, -0.3192],
    49318: ["Saint-Saturnin-sur-Loire", 47.3839, -0.4291],
    49319: ["Saint-Sauveur-de-Flée", 47.7500, -0.8078],
    49320: ["Saint-Sauveur-de-Landemont", 47.2925, -1.2440],
    49321: ["Saint-Sigismond", 47.4512, -0.9427],
    49322: ["Saint-Sulpice", 47.4027, -0.4169],
    49323: ["Saint-Sylvain-d'Anjou", 47.5123, -0.4758],
    49324: ["La Salle-et-Chapelle-Aubry", 47.2489, -0.9712],
    49325: ["La Salle-de-Vihiers", 47.1688, -0.6282],
    49326: ["Sarrigné", 47.5024, -0.3857],
    49327: ["Saulgé-l'Hôpital", 47.3040, -0.3799],
    49328: ["Saumur", 47.2673, -0.0830],
    49329: ["Savennières", 47.4058, -0.6671],
    49330: ["Sceaux-d'Anjou", 47.6313, -0.6096],
    49331: ["Segré", 47.7016, -0.8633],
    49332: ["La Séguinière", 47.0803, -0.9577],
    49333: ["Seiches-sur-le-Loir", 47.6039, -0.3588],
    49334: ["Sermaise", 47.5226, -0.2159],
    49335: ["Soeurdres", 47.7408, -0.5709],
    49336: ["Somloire", 47.0353, -0.5956],
    49337: ["Soucelles", 47.5827, -0.4253],
    49338: ["Soulaines-sur-Aubance", 47.3590, -0.5196],
    49339: ["Soulaire-et-Bourg", 47.5798, -0.5429],
    49341: ["Souzay-Champigny", 47.2170, -0.0207],
    49342: ["Tancoigné", 47.1685, -0.4136],
    49343: ["La Tessoualle", 47.0080, -0.8372],
    49344: ["Thorigné-d'Anjou", 47.6350, -0.6635],
    49345: ["Thouarcé", 47.2592, -0.5179],
    49346: ["Le Thoureil", 47.3700, -0.2732],
    49347: ["Tiercé", 47.6245, -0.4434],
    49348: ["Tigné", 47.2001, -0.4394],
    49349: ["Tillières", 47.1463, -1.1704],
    49350: ["Torfou", 47.0477, -1.0896],
    49351: ["La Tourlandry", 47.1427, -0.7006],
    49352: ["Toutlemonde", 47.0574, -0.7584],
    49353: ["Trélazé", 47.4513, -0.4734],
    49354: ["Le Tremblay", 47.6704, -1.0435],
    49355: ["Trémentines", 47.1227, -0.8014],
    49356: ["Trémont", 47.1555, -0.4536],
    49358: ["Turquant", 47.2114, 0.0218],
    49359: ["Les Ulmes", 47.2234, -0.1823],
    49360: ["La Varenne", 47.3173, -1.3079],
    49361: ["Varennes-sur-Loire", 47.2478, 0.0416],
    49362: ["Varrains", 47.2277, -0.0662],
    49363: ["Vauchrétien", 47.3384, -0.4851],
    49364: ["Vaudelnay", 47.1411, -0.2128],
    49365: ["Les Verchers-sur-Layon", 47.1469, -0.3020],
    49366: ["Vergonnes", 47.7250, -1.0783],
    49367: ["Vern-d'Anjou", 47.5882, -0.8496],
    49368: ["Vernantes", 47.3942, 0.0285],
    49369: ["Vernoil-le-Fourrier", 47.3961, 0.0973],
    49370: ["Verrie", 47.2685, -0.1866],
    49371: ["Vezins", 47.1152, -0.7212],
    49372: ["Le Vieil-Baugé", 47.5209, -0.1442],
    49373: ["Vihiers", 47.1373, -0.5594],
    49374: ["Villebernier", 47.2624, -0.0208],
    49375: ["Villedieu-la-Blouère", 47.1623, -1.0553],
    49376: ["Villemoisan", 47.4710, -0.8900],
    49377: ["Villevêque", 47.5486, -0.4469],
    49378: ["Vivy", 47.3228, -0.0669],
    49380: ["Vaulandry", 47.6069, -0.0238],
    49381: ["Yzernay", 47.0344, -0.6968]
};
