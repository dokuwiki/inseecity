<?php
/**
 * @license    http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html
 * @author     Francois Merciol <dokuplugin@merciol.fr>
 *
 * English language file
 */
 
// for the configuration manager
$lang['allDep']       = "Tous les départments (code lourd et lent)";
$lang['selectedDeps'] = 'Sélection de départments séparés par des virgules (",")';
$lang['maxAutocomplete'] = 'taille maximum de la liste de villes';
