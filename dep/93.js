/*
  Sources :
  https://www.insee.fr/fr/information/4316069
  http://www.geonames.org/export/codes.html
  http://download.geonames.org/export/dump/
*/

var inseeCityNameLatLonDep;
if (inseeCityNameLatLonDep === undefined)
    inseeCityNameLatLonDep = new Map ();

inseeCityNameLatLonDep[93] = {
    93001: ["Aubervilliers", 48.9121, 2.3844],
    93005: ["Aulnay-sous-Bois", 48.9458, 2.4932],
    93006: ["Bagnolet", 48.8690, 2.4227],
    93007: ["Le Blanc-Mesnil", 48.9395, 2.4611],
    93008: ["Bobigny", 48.9076, 2.4386],
    93010: ["Bondy", 48.9023, 2.4837],
    93013: ["Le Bourget", 48.9361, 2.4282],
    93014: ["Clichy-sous-Bois", 48.9076, 2.5462],
    93015: ["Coubron", 48.9176, 2.5763],
    93027: ["La Courneuve", 48.9322, 2.3997],
    93029: ["Drancy", 48.9234, 2.4449],
    93030: ["Dugny", 48.9503, 2.4238],
    93031: ["épinay-sur-Seine", 48.9550, 2.3145],
    93032: ["Gagny", 48.8820, 2.5449],
    93033: ["Gournay-sur-Marne", 48.8605, 2.5754],
    93039: ["L'île-Saint-Denis", 48.9395, 2.3254],
    93045: ["Les Lilas", 48.8819, 2.4196],
    93046: ["Livry-Gargan", 48.9197, 2.5348],
    93047: ["Montfermeil", 48.8982, 2.5671],
    93048: ["Montreuil", 48.8633, 2.4481],
    93049: ["Neuilly-Plaisance", 48.8643, 2.5104],
    93050: ["Neuilly-sur-Marne", 48.8624, 2.5400],
    93051: ["Noisy-le-Grand", 48.8361, 2.5644],
    93053: ["Noisy-le-Sec", 48.8913, 2.4591],
    93055: ["Pantin", 48.8983, 2.4087],
    93057: ["Les Pavillons-sous-Bois", 48.9082, 2.5029],
    93059: ["Pierrefitte-sur-Seine", 48.9609, 2.3632],
    93061: ["Le Pré-Saint-Gervais", 48.8846, 2.4054],
    93062: ["Le Raincy", 48.8967, 2.5197],
    93063: ["Romainville", 48.8852, 2.4376],
    93064: ["Rosny-sous-Bois", 48.8745, 2.4863],
    93066: ["Saint-Denis", 48.9295, 2.3592],
    93070: ["Saint-Ouen", 48.9098, 2.3325],
    93071: ["Sevran", 48.9386, 2.5312],
    93072: ["Stains", 48.9567, 2.3854],
    93073: ["Tremblay-en-France", 48.9784, 2.5546],
    93074: ["Vaujours", 48.9324, 2.5810],
    93077: ["Villemomble", 48.8848, 2.5089],
    93078: ["Villepinte", 48.9590, 2.5363],
    93079: ["Villetaneuse", 48.9572, 2.3450]
};
