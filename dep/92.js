/*
  Sources :
  https://www.insee.fr/fr/information/4316069
  http://www.geonames.org/export/codes.html
  http://download.geonames.org/export/dump/
*/

var inseeCityNameLatLonDep;
if (inseeCityNameLatLonDep === undefined)
    inseeCityNameLatLonDep = new Map ();

inseeCityNameLatLonDep[92] = {
    92002: ["Antony", 48.7503, 2.2993],
    92004: ["Asnières-sur-Seine", 48.9153, 2.2880],
    92007: ["Bagneux", 48.7983, 2.3098],
    92009: ["Bois-Colombes", 48.9153, 2.2673],
    92012: ["Boulogne-Billancourt", 48.8365, 2.2391],
    92014: ["Bourg-la-Reine", 48.7799, 2.3164],
    92019: ["Châtenay-Malabry", 48.7681, 2.2628],
    92020: ["Châtillon", 48.8034, 2.2879],
    92022: ["Chaville", 48.8076, 2.1923],
    92023: ["Clamart", 48.7964, 2.2547],
    92024: ["Clichy", 48.9035, 2.3057],
    92025: ["Colombes", 48.9225, 2.2467],
    92026: ["Courbevoie", 48.8984, 2.2557],
    92032: ["Fontenay-aux-Roses", 48.7897, 2.2872],
    92033: ["Garches", 48.8450, 2.1866],
    92035: ["La Garenne-Colombes", 48.9067, 2.2446],
    92036: ["Gennevilliers", 48.9345, 2.2938],
    92040: ["Issy-les-Moulineaux", 48.8234, 2.2644],
    92044: ["Levallois-Perret", 48.8950, 2.2868],
    92046: ["Malakoff", 48.8169, 2.2969],
    92047: ["Marnes-la-Coquette", 48.8302, 2.1664],
    92048: ["Meudon", 48.8037, 2.2269],
    92049: ["Montrouge", 48.8153, 2.3164],
    92050: ["Nanterre", 48.8960, 2.2067],
    92051: ["Neuilly-sur-Seine", 48.8853, 2.2664],
    92060: ["Le Plessis-Robinson", 48.7804, 2.2606],
    92062: ["Puteaux", 48.8837, 2.2383],
    92063: ["Rueil-Malmaison", 48.8691, 2.1773],
    92064: ["Saint-Cloud", 48.8428, 2.2086],
    92071: ["Sceaux", 48.7768, 2.2952],
    92072: ["Sèvres", 48.8214, 2.2105],
    92073: ["Suresnes", 48.8698, 2.2196],
    92075: ["Vanves", 48.8215, 2.2873],
    92076: ["Vaucresson", 48.8410, 2.1602],
    92077: ["Ville-d'Avray", 48.8215, 2.1776],
    92078: ["Villeneuve-la-Garenne", 48.9364, 2.3244]
};
