/**
 * @license    http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html
 * @author     Francois Merciol <dokuplugin@merciol.fr>
 *
 * INSEE city: code database
 *
 * Sources :
 *   https://www.insee.fr/fr/information/4316069
 *   http://www.geonames.org/export/codes.html
 *   http://download.geonames.org/export/dump/
 */

var inseeCityDep;
var inseeCityName;
var inseeCityNameInsee;
var inseeCityNameLatLon;
var inseeCityNameLatLonDep;
if (inseeCityNameLatLonDep === undefined)
    inseeCityNameLatLonDep = new Map ();

jQuery (function () {
    inseeCityDep = JSINFO['inseeCity']['selectedDeps'];
    inseeCityMaxAutocomplete = JSINFO['inseeCity']['maxAutocomplete'];
    inseeCityName = new Array ();
    inseeCityNameInsee = new Array ();
    inseeCityNameLatLon = new Map ();

    inseeCityDep.forEach ( (dep, index) => {
	for (var cityCode in inseeCityNameLatLonDep[dep]) {
	    inseeCityNameLatLon [cityCode] = inseeCityNameLatLonDep[dep][cityCode];
	    inseeCityName.push (inseeCityNameLatLon[cityCode][0]+" ("+cityCode+")");
	    inseeCityNameInsee [inseeCityNameLatLon[cityCode][0].toLowerCase ()] = cityCode;
	}
    });
    inseeCityName.sort ();

    /* autocomplete */
    jQuery ('.insee input[name="city"]').autocomplete ({
	source: inseeCityName,
	source: function (request, response) {
	    var upper = request.term.toUpperCase ();
            var results = jQuery.ui.autocomplete.filter (inseeCityName, request.term);
	    var startsWith = [];
	    jQuery.each (results, function (i, v) {
	    	if (v.toUpperCase ().indexOf (upper) == 0)
	    	    startsWith.push (v);
	    });
	    results = results.slice (0, inseeCityMaxAutocomplete);
	    jQuery.each (results, function (i, v) {
	    	if (v.toUpperCase ().indexOf (upper) != 0)
	    	    startsWith.push (v);
	    });
	    response (startsWith.slice (0, inseeCityMaxAutocomplete));
	}
    });
});

