<?php
/**
 * @license    http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html
 * @author     Francois Merciol <dokuplugin@merciol.fr>
 *
 * English language file
 */
 
// for the configuration manager
$lang['allDep']       = "All departments (huge and slow code)";
$lang['selectedDeps'] = 'Selected departments (administrative division) comma separated (",")';
$lang['maxAutocomplete'] = 'max city list size';
