/*
  Sources :
  https://www.insee.fr/fr/information/4316069
  http://www.geonames.org/export/codes.html
  http://download.geonames.org/export/dump/
*/

var inseeCityNameLatLonDep;
if (inseeCityNameLatLonDep === undefined)
    inseeCityNameLatLonDep = new Map ();

inseeCityNameLatLonDep[31] = {
    31001: ["Agassac", 43.3720, 0.8853],
    31002: ["Aignes", 43.3349, 1.5864],
    31003: ["Aigrefeuille", 43.5693, 1.5842],
    31004: ["Ayguesvives", 43.4290, 1.5943],
    31005: ["Alan", 43.2200, 0.9276],
    31006: ["Albiac", 43.5558, 1.7826],
    31007: ["Ambax", 43.3680, 0.9370],
    31008: ["Anan", 43.3522, 0.8100],
    31009: ["Antichan-de-Frontignes", 42.9696, 0.6769],
    31010: ["Antignac", 42.8311, 0.5877],
    31011: ["Arbas", 42.9925, 0.9056],
    31012: ["Arbon", 43.0009, 0.7310],
    31013: ["Ardiège", 43.0667, 0.6440],
    31014: ["Arguenos", 42.9592, 0.7269],
    31015: ["Argut-Dessous", 42.8924, 0.7155],
    31017: ["Arlos", 42.8742, 0.6952],
    31018: ["Arnaud-Guilhem", 43.1465, 0.8991],
    31019: ["Artigue", 42.8264, 0.6397],
    31020: ["Aspet", 43.0114, 0.8059],
    31021: ["Aspret-Sarrat", 43.0698, 0.7200],
    31022: ["Aucamville", 43.6713, 1.4237],
    31023: ["Aulon", 43.1900, 0.8134],
    31024: ["Auragne", 43.3912, 1.5090],
    31025: ["Aureville", 43.4804, 1.4571],
    31026: ["Auriac-sur-Vendinelle", 43.5194, 1.8244],
    31027: ["Auribail", 43.3464, 1.3752],
    31028: ["Aurignac", 43.2137, 0.8714],
    31029: ["Aurin", 43.5351, 1.6812],
    31030: ["Ausseing", 43.1540, 1.0312],
    31031: ["Ausson", 43.0915, 0.5899],
    31032: ["Aussonne", 43.6836, 1.3288],
    31033: ["Auterive", 43.3595, 1.4774],
    31034: ["Auzas", 43.1744, 0.8941],
    31035: ["Auzeville-Tolosane", 43.5286, 1.4872],
    31036: ["Auzielle", 43.5401, 1.5683],
    31037: ["Avignonet-Lauragais", 43.3769, 1.7727],
    31038: ["Azas", 43.7200, 1.6743],
    31039: ["Bachas", 43.2487, 0.9417],
    31040: ["Bachos", 42.8985, 0.6047],
    31041: ["Bagiry", 42.9798, 0.6244],
    31042: ["Bagnères-de-Luchon", 42.7361, 0.6266],
    31043: ["Balesta", 43.2022, 0.5634],
    31044: ["Balma", 43.6115, 1.5045],
    31045: ["Barbazan", 43.0349, 0.6218],
    31046: ["Baren", 42.8676, 0.6349],
    31047: ["Bax", 43.2246, 1.2882],
    31048: ["Baziège", 43.4622, 1.6279],
    31049: ["Bazus", 43.7357, 1.5196],
    31050: ["Beauchalot", 43.1144, 0.8697],
    31051: ["Beaufort", 43.4548, 1.1065],
    31052: ["Beaumont-sur-Lèze", 43.3798, 1.3561],
    31053: ["Beaupuy", 43.6525, 1.5586],
    31054: ["Beauteville", 43.3491, 1.7328],
    31055: ["Beauville", 43.4675, 1.7707],
    31056: ["Beauzelle", 43.6677, 1.3740],
    31057: ["Belberaud", 43.5037, 1.5697],
    31058: ["Belbèze-de-Lauragais", 43.4370, 1.5567],
    31059: ["Belbèze-en-Comminges", 43.1338, 1.0296],
    31060: ["Bélesta-en-Lauragais", 43.4395, 1.8316],
    31061: ["Bellegarde-Sainte-Marie", 43.6757, 1.0996],
    31062: ["Bellesserre", 43.7873, 1.1012],
    31063: ["Benque", 43.2590, 0.9179],
    31064: ["Benque-Dessous-et-Dessus", 42.8204, 0.5417],
    31065: ["Bérat", 43.3779, 1.1713],
    31066: ["Bessières", 43.7969, 1.5861],
    31067: ["Bezins-Garraux", 42.9389, 0.7020],
    31068: ["Billière", 42.8163, 0.5285],
    31069: ["Blagnac", 43.6421, 1.3788],
    31070: ["Blajan", 43.2538, 0.6478],
    31071: ["Bois-de-la-Pierre", 43.3411, 1.1619],
    31072: ["Boissède", 43.4071, 0.8262],
    31073: ["Bondigoux", 43.8426, 1.5434],
    31074: ["Bonrepos-Riquet", 43.6764, 1.6225],
    31075: ["Bonrepos-sur-Aussonnelle", 43.5441, 1.1370],
    31076: ["Bordes-de-Rivière", 43.1236, 0.6349],
    31077: ["Le Born", 43.8958, 1.5390],
    31078: ["Boudrac", 43.1873, 0.5173],
    31079: ["Bouloc", 43.7958, 1.4191],
    31080: ["Boulogne-sur-Gesse", 43.2904, 0.6506],
    31081: ["Bourg-d'Oueil", 42.8605, 0.4868],
    31082: ["Bourg-Saint-Bernard", 43.6062, 1.6987],
    31083: ["Boussan", 43.2416, 0.8751],
    31084: ["Boussens", 43.1763, 0.9603],
    31085: ["Boutx", 42.9153, 0.7703],
    31086: ["Bouzin", 43.1908, 0.8830],
    31087: ["Bragayrac", 43.4884, 1.0700],
    31088: ["Brax", 43.6156, 1.2319],
    31089: ["Bretx", 43.7059, 1.1962],
    31090: ["Brignemont", 43.7692, 0.9983],
    31091: ["Bruguières", 43.7271, 1.4091],
    31092: ["Burgalays", 42.8843, 0.6365],
    31093: ["Le Burgaud", 43.7897, 1.1461],
    31094: ["Buzet-sur-Tarn", 43.7685, 1.6137],
    31095: ["Cabanac-Cazaux", 43.0344, 0.7398],
    31096: ["Cabanac-Séguenville", 43.7914, 1.0198],
    31097: ["Le Cabanial", 43.5220, 1.8689],
    31098: ["Cadours", 43.7262, 1.0467],
    31099: ["Caignac", 43.3198, 1.7072],
    31100: ["Calmont", 43.2841, 1.6263],
    31101: ["Cambernard", 43.4619, 1.1787],
    31102: ["Cambiac", 43.4906, 1.7895],
    31103: ["Canens", 43.2165, 1.3327],
    31104: ["Capens", 43.3349, 1.2598],
    31105: ["Caragoudes", 43.5007, 1.7134],
    31106: ["Caraman", 43.5291, 1.7529],
    31107: ["Carbonne", 43.3014, 1.2184],
    31108: ["Cardeilhac", 43.1959, 0.6763],
    31109: ["Cassagnabère-Tournas", 43.2311, 0.7991],
    31110: ["Cassagne", 43.1242, 0.9914],
    31111: ["Castagnac", 43.2357, 1.3434],
    31112: ["Castagnède", 43.0514, 0.9773],
    31113: ["Castanet-Tolosan", 43.5136, 1.5038],
    31114: ["Castelbiague", 43.0356, 0.9273],
    31115: ["Castelgaillard", 43.3520, 0.9019],
    31116: ["Castelginest", 43.6974, 1.4355],
    31117: ["Castelmaurou", 43.6845, 1.5374],
    31118: ["Castelnau-d'Estrétefonds", 43.7938, 1.3616],
    31119: ["Castelnau-Picampeau", 43.3086, 1.0151],
    31120: ["Le Castéra", 43.6709, 1.1447],
    31121: ["Castéra-Vignoles", 43.2764, 0.7825],
    31122: ["Casties-Labrande", 43.3277, 0.9934],
    31123: ["Castillon-de-Larboust", 42.7401, 0.5576],
    31124: ["Castillon-de-Saint-Martory", 43.1370, 0.8510],
    31125: ["Cathervielle", 42.8234, 0.5075],
    31126: ["Caubiac", 43.7125, 1.0739],
    31127: ["Caubous", 42.8472, 0.5219],
    31128: ["Caujac", 43.2953, 1.4585],
    31129: ["Cazaril-Laspènes", 42.8031, 0.5814],
    31130: ["Cazaril-Tambourès", 43.1788, 0.5425],
    31131: ["Cazaunous", 42.9882, 0.7247],
    31132: ["Cazaux-Layrisse", 42.8715, 0.5969],
    31133: ["Cazeaux-de-Larboust", 42.7515, 0.5322],
    31134: ["Cazeneuve-Montaut", 43.1817, 0.8620],
    31135: ["Cazères", 43.2203, 1.0930],
    31136: ["Cépet", 43.7456, 1.4366],
    31137: ["Cessales", 43.4578, 1.7444],
    31138: ["Charlas", 43.2348, 0.6925],
    31139: ["Chaum", 42.9406, 0.6651],
    31140: ["Chein-Dessus", 43.0047, 0.8754],
    31141: ["Ciadoux", 43.2542, 0.7420],
    31142: ["Cier-de-Luchon", 42.8554, 0.5872],
    31143: ["Cier-de-Rivière", 43.0582, 0.6265],
    31144: ["Cierp-Gaud", 42.9126, 0.6346],
    31145: ["Cintegabelle", 43.3068, 1.5357],
    31146: ["Cirès", 42.8523, 0.5092],
    31147: ["Clarac", 43.1137, 0.6161],
    31148: ["Clermont-le-Fort", 43.4584, 1.4426],
    31149: ["Colomiers", 43.6115, 1.3270],
    31150: ["Cornebarrieu", 43.6486, 1.3224],
    31151: ["Corronsac", 43.4715, 1.4898],
    31152: ["Coueilles", 43.3467, 0.8783],
    31153: ["Couladère", 43.1961, 1.0942],
    31155: ["Couret", 43.0492, 0.8168],
    31156: ["Cox", 43.7604, 1.0420],
    31157: ["Cugnaux", 43.5449, 1.3428],
    31158: ["Cuguron", 43.1043, 0.5156],
    31159: ["Le Cuing", 43.1471, 0.6119],
    31160: ["Daux", 43.6872, 1.2663],
    31161: ["Deyme", 43.4823, 1.5326],
    31162: ["Donneville", 43.4727, 1.5512],
    31163: ["Drémil-Lafage", 43.5917, 1.6031],
    31164: ["Drudas", 43.7622, 1.0979],
    31165: ["Eaunes", 43.4276, 1.3589],
    31166: ["Empeaux", 43.5369, 1.0790],
    31167: ["Encausse-les-Thermes", 43.0532, 0.7462],
    31168: ["Eoux", 43.2774, 0.8823],
    31169: ["Escalquens", 43.5209, 1.5524],
    31170: ["Escanecrabe", 43.2716, 0.7572],
    31171: ["Espanès", 43.4509, 1.4870],
    31172: ["Esparron", 43.2611, 0.8115],
    31173: ["Esperce", 43.2993, 1.4006],
    31174: ["Estadens", 43.0338, 0.8441],
    31175: ["Estancarbon", 43.1088, 0.7811],
    31176: ["Esténos", 42.9408, 0.6355],
    31177: ["Eup", 42.9263, 0.6844],
    31178: ["Fabas", 43.3119, 0.8931],
    31179: ["Le Faget", 43.5648, 1.8260],
    31180: ["Falga", 43.4805, 1.8460],
    31181: ["Le Fauga", 43.3984, 1.2940],
    31182: ["Fenouillet", 43.6848, 1.3905],
    31183: ["Figarol", 43.0861, 0.9051],
    31184: ["Flourens", 43.5977, 1.5539],
    31185: ["Folcarde", 43.4059, 1.7949],
    31186: ["Fonbeauzard", 43.6798, 1.4349],
    31187: ["Fonsorbes", 43.5317, 1.2386],
    31188: ["Fontenilles", 43.5554, 1.1973],
    31189: ["Forgues", 43.4311, 1.0432],
    31190: ["Fos", 42.8605, 0.7475],
    31191: ["Fougaron", 42.9816, 0.9354],
    31192: ["Fourquevaux", 43.4981, 1.6217],
    31193: ["Le Fousseret", 43.2863, 1.0649],
    31194: ["Francarville", 43.5867, 1.7479],
    31195: ["Francazal", 43.0080, 0.9969],
    31196: ["Francon", 43.2618, 0.9880],
    31197: ["Franquevielle", 43.1310, 0.5341],
    31198: ["Le Fréchet", 43.1898, 0.9318],
    31199: ["Fronsac", 42.9540, 0.6622],
    31200: ["Frontignan-de-Comminges", 42.9631, 0.6658],
    31201: ["Frontignan-Savès", 43.3992, 0.9126],
    31202: ["Fronton", 43.8517, 1.3792],
    31203: ["Frouzins", 43.5213, 1.3130],
    31204: ["Fustignac", 43.3012, 0.9838],
    31205: ["Gagnac-sur-Garonne", 43.7075, 1.3635],
    31206: ["Gaillac-Toulza", 43.2567, 1.4562],
    31207: ["Galié", 42.9928, 0.6337],
    31208: ["Ganties", 43.0640, 0.8369],
    31209: ["Garac", 43.6892, 1.0916],
    31210: ["Gardouch", 43.3848, 1.6782],
    31211: ["Gargas", 43.7592, 1.4592],
    31212: ["Garidech", 43.7133, 1.5516],
    31213: ["Garin", 42.8073, 0.4979],
    31215: ["Gauré", 43.6176, 1.6369],
    31216: ["Gémil", 43.7390, 1.5961],
    31217: ["Génos", 42.9989, 0.6688],
    31218: ["Gensac-de-Boulogne", 43.2589, 0.6035],
    31219: ["Gensac-sur-Garonne", 43.2158, 1.1429],
    31220: ["Gibel", 43.3026, 1.6763],
    31221: ["Gouaux-de-Larboust", 42.7805, 0.4773],
    31222: ["Gouaux-de-Luchon", 42.8507, 0.6418],
    31223: ["Goudex", 43.3790, 0.9567],
    31224: ["Gourdan-Polignan", 43.0693, 0.5756],
    31225: ["Goutevernisse", 43.2140, 1.1737],
    31226: ["Gouzens", 43.1791, 1.1818],
    31227: ["Goyrans", 43.4816, 1.4265],
    31228: ["Gragnague", 43.6830, 1.5823],
    31229: ["Gratens", 43.3258, 1.1226],
    31230: ["Gratentour", 43.7204, 1.4343],
    31231: ["Grazac", 43.3114, 1.4487],
    31232: ["Grenade", 43.7640, 1.2803],
    31233: ["Grépiac", 43.4078, 1.4527],
    31234: ["Le Grès", 43.7188, 1.0998],
    31235: ["Guran", 42.8861, 0.6070],
    31236: ["Herran", 42.9719, 0.8856],
    31237: ["His", 43.0649, 0.9658],
    31238: ["Huos", 43.0693, 0.6001],
    31239: ["L'Isle-en-Dodon", 43.3811, 0.8338],
    31240: ["Issus", 43.4227, 1.5027],
    31241: ["Izaut-de-l'Hôtel", 43.0131, 0.7497],
    31242: ["Jurvielle", 42.8293, 0.4750],
    31243: ["Juzes", 43.4471, 1.7934],
    31244: ["Juzet-de-Luchon", 42.8030, 0.6299],
    31245: ["Juzet-d'Izaut", 42.9699, 0.7573],
    31246: ["Labarthe-Inard", 43.1078, 0.8324],
    31247: ["Labarthe-Rivière", 43.0809, 0.6714],
    31248: ["Labarthe-sur-Lèze", 43.4585, 1.4008],
    31249: ["Labastide-Beauvoir", 43.4833, 1.6636],
    31250: ["Labastide-Clermont", 43.3495, 1.1061],
    31251: ["Labastide-Paumès", 43.3331, 0.9356],
    31252: ["Labastide-Saint-Sernin", 43.7357, 1.4659],
    31253: ["Labastidette", 43.4572, 1.2371],
    31254: ["Labège", 43.5391, 1.5206],
    31255: ["Labroquère", 43.0422, 0.5915],
    31256: ["Labruyère-Dorsa", 43.4049, 1.4715],
    31258: ["Lacaugne", 43.2812, 1.2824],
    31259: ["Lacroix-Falgarde", 43.5008, 1.4239],
    31260: ["Laffite-Toupière", 43.1654, 0.9132],
    31261: ["Lafitte-Vigordane", 43.3031, 1.1622],
    31262: ["Lagarde", 43.3452, 1.6966],
    31263: ["Lagardelle-sur-Lèze", 43.4146, 1.3925],
    31264: ["Lagrâce-Dieu", 43.3373, 1.4052],
    31265: ["Lagraulet-Saint-Nicolas", 43.7888, 1.0731],
    31266: ["Lahage", 43.4402, 1.0657],
    31267: ["Lahitère", 43.1417, 1.1856],
    31268: ["Lalouret-Laffiteau", 43.1867, 0.7002],
    31269: ["Lamasquère", 43.4845, 1.2483],
    31270: ["Landorthe", 43.1295, 0.7778],
    31271: ["Lanta", 43.5615, 1.6680],
    31272: ["Lapeyrère", 43.2048, 1.3113],
    31273: ["Lapeyrouse-Fossat", 43.7016, 1.5103],
    31274: ["Larcan", 43.1712, 0.7217],
    31275: ["Laréole", 43.7417, 1.0242],
    31276: ["Larroque", 43.1954, 0.6136],
    31277: ["Lasserre", 43.6358, 1.1912],
    31278: ["Latoue", 43.1721, 0.7821],
    31279: ["Latour", 43.2018, 1.2819],
    31280: ["Latrape", 43.2493, 1.2909],
    31281: ["Launac", 43.7503, 1.1707],
    31282: ["Launaguet", 43.6700, 1.4551],
    31283: ["Lautignac", 43.3790, 1.0634],
    31284: ["Lauzerville", 43.5583, 1.5669],
    31285: ["Lavalette", 43.6393, 1.5916],
    31286: ["Lavelanet-de-Comminges", 43.2526, 1.1147],
    31287: ["Lavernose-Lacasse", 43.3913, 1.2463],
    31288: ["Layrac-sur-Tarn", 43.8338, 1.5628],
    31289: ["Lécussan", 43.1552, 0.4926],
    31290: ["Lège", 42.8790, 0.5998],
    31291: ["Léguevin", 43.5919, 1.2290],
    31292: ["Lescuns", 43.2452, 1.0046],
    31293: ["Lespinasse", 43.7158, 1.3833],
    31294: ["Lespiteau", 43.0657, 0.7623],
    31295: ["Lespugue", 43.2296, 0.6669],
    31296: ["Lestelle-de-Saint-Martory", 43.1187, 0.9021],
    31297: ["Lévignac", 43.6593, 1.2038],
    31298: ["Lez", 42.9086, 0.7059],
    31299: ["Lherm", 43.4293, 1.2233],
    31300: ["Lieoux", 43.1495, 0.7735],
    31301: ["Lilhac", 43.2848, 0.8095],
    31302: ["Lodes", 43.1663, 0.6575],
    31303: ["Longages", 43.3574, 1.2142],
    31304: ["Loubens-Lauragais", 43.5744, 1.7833],
    31305: ["Loudet", 43.1399, 0.5763],
    31306: ["Lourde", 42.9847, 0.6586],
    31307: ["Lunax", 43.3389, 0.6942],
    31308: ["Luscan", 43.0133, 0.6269],
    31309: ["Lussan-Adeilhac", 43.2961, 0.9519],
    31310: ["Lux", 43.4310, 1.7813],
    31311: ["La Magdelaine-sur-Tarn", 43.8136, 1.5350],
    31312: ["Mailholas", 43.2454, 1.2513],
    31313: ["Malvezie", 43.0069, 0.7001],
    31314: ["Mancioux", 43.1668, 0.9417],
    31315: ["Mane", 43.0801, 0.9447],
    31316: ["Marignac", 42.8948, 0.6660],
    31317: ["Marignac-Lasclares", 43.3049, 1.1066],
    31318: ["Marignac-Laspeyres", 43.2127, 0.9617],
    31319: ["Marliac", 43.2236, 1.4724],
    31320: ["Marquefave", 43.3095, 1.2594],
    31321: ["Marsoulas", 43.1065, 0.9934],
    31322: ["Martisserre", 43.3947, 0.8853],
    31323: ["Martres-de-Rivière", 43.0829, 0.6448],
    31324: ["Martres-Tolosane", 43.2020, 1.0011],
    31325: ["Mascarville", 43.5587, 1.7538],
    31326: ["Massabrac", 43.2218, 1.3671],
    31327: ["Mauran", 43.1846, 1.0227],
    31328: ["Mauremont", 43.4559, 1.6815],
    31329: ["Maurens", 43.4663, 1.7977],
    31330: ["Mauressac", 43.3212, 1.4342],
    31331: ["Maureville", 43.5296, 1.7144],
    31332: ["Mauvaisin", 43.3594, 1.5386],
    31333: ["Mauvezin", 43.3839, 0.9206],
    31334: ["Mauzac", 43.3756, 1.3028],
    31335: ["Mayrègne", 42.8481, 0.5400],
    31336: ["Mazères-sur-Salat", 43.1324, 0.9635],
    31337: ["Melles", 42.8670, 0.8089],
    31338: ["Menville", 43.6792, 1.1874],
    31339: ["Mérenvielle", 43.6206, 1.1676],
    31340: ["Mervilla", 43.5063, 1.4731],
    31341: ["Merville", 43.7221, 1.2959],
    31342: ["Milhas", 42.9775, 0.8275],
    31343: ["Mirambeau", 43.4059, 0.8630],
    31344: ["Miramont-de-Comminges", 43.0914, 0.7556],
    31345: ["Miremont", 43.3768, 1.4172],
    31346: ["Mirepoix-sur-Tarn", 43.8243, 1.5774],
    31347: ["Molas", 43.4003, 0.7792],
    31348: ["Moncaup", 42.9729, 0.7016],
    31349: ["Mondavezan", 43.2413, 1.0424],
    31350: ["Mondilhan", 43.2903, 0.7079],
    31351: ["Mondonville", 43.6630, 1.2812],
    31352: ["Mondouzil", 43.6315, 1.5621],
    31353: ["Monès", 43.4181, 1.0332],
    31354: ["Monestrol", 43.3322, 1.6627],
    31355: ["Mons", 43.6112, 1.5739],
    31356: ["Montaigut-sur-Save", 43.6803, 1.2345],
    31357: ["Montastruc-de-Salies", 43.0375, 0.9014],
    31358: ["Montastruc-la-Conseillère", 43.7221, 1.5890],
    31359: ["Montastruc-Savès", 43.3616, 1.0190],
    31360: ["Montauban-de-Luchon", 42.7905, 0.6233],
    31361: ["Montaut", 43.3455, 1.3106],
    31362: ["Montberaud", 43.1595, 1.1551],
    31363: ["Montbernard", 43.3042, 0.7678],
    31364: ["Montberon", 43.7203, 1.4889],
    31365: ["Montbrun-Bocage", 43.1195, 1.2493],
    31366: ["Montbrun-Lauragais", 43.4558, 1.5198],
    31367: ["Montclar-de-Comminges", 43.1709, 1.0293],
    31368: ["Montclar-Lauragais", 43.3627, 1.7136],
    31369: ["Mont-de-Galié", 42.9926, 0.6467],
    31370: ["Montégut-Bourjac", 43.2815, 0.9851],
    31371: ["Montégut-Lauragais", 43.4775, 1.9314],
    31372: ["Montespan", 43.0856, 0.8597],
    31373: ["Montesquieu-Guittaut", 43.3386, 0.7672],
    31374: ["Montesquieu-Lauragais", 43.4073, 1.6276],
    31375: ["Montesquieu-Volvestre", 43.1925, 1.2178],
    31376: ["Montgaillard-de-Salies", 43.0589, 0.9322],
    31377: ["Montgaillard-Lauragais", 43.4284, 1.7026],
    31378: ["Montgaillard-sur-Save", 43.2545, 0.7174],
    31379: ["Montgazin", 43.3054, 1.3033],
    31380: ["Montgeard", 43.3434, 1.6415],
    31381: ["Montgiscard", 43.4551, 1.5705],
    31382: ["Montgras", 43.4499, 1.0683],
    31383: ["Montjoire", 43.7787, 1.5266],
    31384: ["Montlaur", 43.4853, 1.5806],
    31385: ["Montmaurin", 43.2268, 0.6361],
    31386: ["Montoulieu-Saint-Bernard", 43.2309, 0.9064],
    31387: ["Montoussin", 43.2800, 1.0111],
    31388: ["Montpitol", 43.7047, 1.6424],
    31389: ["Montrabé", 43.6424, 1.5288],
    31390: ["Montréjeau", 43.0885, 0.5584],
    31391: ["Montsaunès", 43.1102, 0.9322],
    31392: ["Mourvilles-Basses", 43.4889, 1.6991],
    31393: ["Mourvilles-Hautes", 43.4244, 1.8197],
    31394: ["Moustajon", 42.8148, 0.5919],
    31395: ["Muret", 43.4491, 1.3078],
    31396: ["Nailloux", 43.3652, 1.6131],
    31397: ["Nénigan", 43.3532, 0.7088],
    31398: ["Nizan-Gesse", 43.2304, 0.5964],
    31399: ["Noé", 43.3605, 1.2705],
    31400: ["Nogaret", 43.4925, 1.9337],
    31401: ["Noueilles", 43.4150, 1.5258],
    31402: ["Odars", 43.5219, 1.5932],
    31403: ["Ondes", 43.7861, 1.3040],
    31404: ["Oô", 42.7417, 0.4964],
    31405: ["Ore", 42.9736, 0.6462],
    31406: ["Palaminy", 43.1942, 1.0603],
    31407: ["Paulhac", 43.7542, 1.5547],
    31408: ["Payssous", 43.0343, 0.7171],
    31409: ["Péchabou", 43.5015, 1.5104],
    31410: ["Pechbonnieu", 43.7112, 1.4587],
    31411: ["Pechbusque", 43.5246, 1.4572],
    31412: ["Péguilhan", 43.3182, 0.7036],
    31413: ["Pelleport", 43.7414, 1.1186],
    31414: ["Peyrissas", 43.2881, 0.9116],
    31415: ["Peyrouzet", 43.2079, 0.8321],
    31416: ["Peyssies", 43.3257, 1.1848],
    31417: ["Pibrac", 43.6274, 1.2613],
    31418: ["Pin-Balma", 43.6223, 1.5357],
    31419: ["Le Pin-Murelet", 43.3973, 1.0213],
    31420: ["Pinsaguel", 43.5030, 1.3939],
    31421: ["Pins-Justaret", 43.4824, 1.3908],
    31422: ["Plagne", 43.1609, 1.0589],
    31423: ["Plagnole", 43.4114, 1.0638],
    31424: ["Plaisance-du-Touch", 43.5575, 1.2858],
    31425: ["Le Plan", 43.1672, 1.1222],
    31426: ["Pointis-de-Rivière", 43.0908, 0.6258],
    31427: ["Pointis-Inard", 43.0835, 0.7969],
    31428: ["Polastron", 43.3196, 0.9516],
    31429: ["Pompertuzat", 43.4942, 1.5210],
    31430: ["Ponlat-Taillebourg", 43.1122, 0.5962],
    31431: ["Portet-d'Aspet", 42.9423, 0.8555],
    31432: ["Portet-de-Luchon", 42.8114, 0.4710],
    31433: ["Portet-sur-Garonne", 43.5293, 1.4025],
    31434: ["Poubeau", 42.8277, 0.4927],
    31435: ["Poucharramet", 43.4230, 1.1668],
    31436: ["Pouy-de-Touges", 43.3360, 1.0493],
    31437: ["Pouze", 43.4340, 1.5341],
    31438: ["Pradère-les-Bourguets", 43.6467, 1.1551],
    31439: ["Préserville", 43.5204, 1.6318],
    31440: ["Proupiary", 43.1583, 0.8679],
    31441: ["Prunet", 43.5724, 1.7297],
    31442: ["Puydaniel", 43.3315, 1.4227],
    31443: ["Puymaurin", 43.3702, 0.7525],
    31444: ["Puysségur", 43.7519, 1.0661],
    31445: ["Quint-Fonsegrives", 43.5815, 1.5421],
    31446: ["Ramonville-Saint-Agne", 43.5441, 1.4778],
    31447: ["Razecueillé", 42.9630, 0.8141],
    31448: ["Rebigue", 43.4900, 1.4776],
    31449: ["Régades", 43.0533, 0.7176],
    31450: ["Renneville", 43.3790, 1.7213],
    31451: ["Revel", 43.4656, 1.9968],
    31452: ["Rieucazé", 43.0758, 0.7522],
    31453: ["Rieumajou", 43.4120, 1.8027],
    31454: ["Rieumes", 43.4121, 1.1145],
    31455: ["Rieux-Volvestre", 43.2555, 1.2063],
    31456: ["Riolas", 43.3508, 0.9192],
    31457: ["Roquefort-sur-Garonne", 43.1598, 0.9867],
    31458: ["Roques", 43.5074, 1.3545],
    31459: ["Roquesérière", 43.7337, 1.6377],
    31460: ["Roquettes", 43.4947, 1.3741],
    31461: ["Rouède", 43.0564, 0.8827],
    31462: ["Rouffiac-Tolosan", 43.6638, 1.5254],
    31463: ["Roumens", 43.4646, 1.9357],
    31464: ["Sabonnères", 43.4686, 1.0572],
    31465: ["Saccourvielle", 42.8184, 0.5681],
    31466: ["Saiguède", 43.5233, 1.1423],
    31467: ["Saint-Alban", 43.6927, 1.4126],
    31468: ["Saint-André", 43.2742, 0.8471],
    31469: ["Saint-Araille", 43.3524, 1.0039],
    31470: ["Saint-Aventin", 42.7594, 0.5766],
    31471: ["Saint-Béat", 42.9045, 0.6852],
    31472: ["Saint-Bertrand-de-Comminges", 43.0239, 0.5524],
    31473: ["Saint-Cézert", 43.7809, 1.1915],
    31474: ["Saint-Christaud", 43.1924, 1.1280],
    31475: ["Saint-Clar-de-Rivière", 43.4758, 1.2089],
    31476: ["Saint-élix-le-Château", 43.2818, 1.1377],
    31477: ["Saint-élix-Séglan", 43.1955, 0.8491],
    31478: ["Saint-Félix-Lauragais", 43.4505, 1.9017],
    31479: ["Saint-Ferréol-de-Comminges", 43.3386, 0.7360],
    31480: ["Sainte-Foy-d'Aigrefeuille", 43.5509, 1.6030],
    31481: ["Sainte-Foy-de-Peyrolières", 43.4808, 1.1369],
    31482: ["Saint-Frajou", 43.3310, 0.8502],
    31483: ["Saint-Gaudens", 43.1199, 0.7266],
    31484: ["Saint-Geniès-Bellevue", 43.6848, 1.4804],
    31485: ["Saint-Germier", 43.4690, 1.7252],
    31486: ["Saint-Hilaire", 43.4206, 1.2687],
    31487: ["Saint-Ignan", 43.1568, 0.6898],
    31488: ["Saint-Jean", 43.6632, 1.5008],
    31489: ["Saint-Jean-Lherm", 43.6977, 1.6179],
    31490: ["Saint-Jory", 43.7393, 1.3562],
    31491: ["Saint-Julia", 43.4940, 1.8941],
    31492: ["Saint-Julien-sur-Garonne", 43.2495, 1.1467],
    31493: ["Saint-Lary-Boujean", 43.2218, 0.7400],
    31494: ["Saint-Laurent", 43.3254, 0.8003],
    31495: ["Saint-Léon", 43.3970, 1.5628],
    31496: ["Sainte-Livrade", 43.6486, 1.1119],
    31497: ["Saint-Loup-Cammas", 43.6978, 1.4799],
    31498: ["Saint-Loup-en-Comminges", 43.2423, 0.5732],
    31499: ["Saint-Lys", 43.5100, 1.1995],
    31500: ["Saint-Mamet", 42.7721, 0.6300],
    31501: ["Saint-Marcel-Paulel", 43.6566, 1.6147],
    31502: ["Saint-Marcet", 43.1967, 0.7455],
    31503: ["Saint-Martory", 43.1434, 0.9367],
    31504: ["Saint-Médard", 43.1279, 0.8293],
    31505: ["Saint-Michel", 43.1590, 1.0857],
    31506: ["Saint-Orens-de-Gameville", 43.5590, 1.5351],
    31507: ["Saint-Paul-sur-Save", 43.6995, 1.2205],
    31508: ["Saint-Paul-d'Oueil", 42.8382, 0.5556],
    31509: ["Saint-Pé-d'Ardet", 42.9860, 0.6799],
    31510: ["Saint-Pé-Delbosc", 43.2673, 0.6942],
    31511: ["Saint-Pierre", 43.6368, 1.6416],
    31512: ["Saint-Pierre-de-Lages", 43.5681, 1.6268],
    31513: ["Saint-Plancard", 43.1679, 0.5700],
    31514: ["Saint-Rome", 43.4150, 1.6743],
    31515: ["Saint-Rustice", 43.8031, 1.3281],
    31516: ["Saint-Sauveur", 43.7513, 1.3957],
    31517: ["Saint-Sulpice-sur-Lèze", 43.3245, 1.3412],
    31518: ["Saint-Thomas", 43.5147, 1.0888],
    31519: ["Saint-Vincent", 43.4405, 1.7581],
    31520: ["Sajas", 43.3752, 1.0252],
    31521: ["Saleich", 43.0283, 0.9682],
    31522: ["Salerm", 43.3071, 0.8249],
    31523: ["Salies-du-Salat", 43.1013, 0.9629],
    31524: ["Salles-et-Pratviel", 42.8332, 0.6067],
    31525: ["Salles-sur-Garonne", 43.2764, 1.1723],
    31526: ["La Salvetat-Saint-Gilles", 43.5754, 1.2661],
    31527: ["La Salvetat-Lauragais", 43.5477, 1.8004],
    31528: ["Saman", 43.2385, 0.7219],
    31529: ["Samouillan", 43.2672, 0.9497],
    31530: ["Sana", 43.2274, 1.0111],
    31531: ["Sarrecave", 43.2147, 0.5949],
    31532: ["Sarremezan", 43.2114, 0.6621],
    31533: ["Saubens", 43.4793, 1.3595],
    31534: ["Saussens", 43.5908, 1.7244],
    31535: ["Sauveterre-de-Comminges", 43.0336, 0.6726],
    31536: ["Saux-et-Pomarède", 43.1487, 0.7048],
    31537: ["Savarthès", 43.1222, 0.8039],
    31538: ["Savères", 43.3725, 1.1000],
    31539: ["Sédeilhac", 43.1477, 0.5427],
    31540: ["Ségreville", 43.4914, 1.7472],
    31541: ["Seilh", 43.6900, 1.3567],
    31542: ["Seilhan", 43.0527, 0.5768],
    31543: ["Sénarens", 43.3519, 0.9759],
    31544: ["Sengouagnet", 42.9551, 0.7842],
    31545: ["Sepx", 43.1553, 0.8370],
    31546: ["Seyre", 43.3641, 1.6635],
    31547: ["Seysses", 43.4988, 1.2869],
    31548: ["Signac", 42.9101, 0.6175],
    31549: ["Sode", 42.8110, 0.6419],
    31550: ["Soueich", 43.0509, 0.7824],
    31551: ["Tarabel", 43.5123, 1.6755],
    31552: ["Terrebasse", 43.2393, 0.9724],
    31553: ["Thil", 43.7101, 1.1530],
    31554: ["Touille", 43.0835, 0.9817],
    31555: ["Toulouse", 43.5963, 1.4316],
    31556: ["Les Tourreilles", 43.1120, 0.5498],
    31557: ["Tournefeuille", 43.5781, 1.3350],
    31558: ["Toutens", 43.4752, 1.7462],
    31559: ["Trébons-de-Luchon", 42.8038, 0.5689],
    31560: ["Trébons-sur-la-Grasse", 43.4488, 1.7213],
    31561: ["L'Union", 43.6542, 1.4839],
    31562: ["Urau", 43.0005, 0.9555],
    31563: ["Vacquiers", 43.7908, 1.4836],
    31564: ["Valcabrère", 43.0300, 0.5861],
    31565: ["Valentine", 43.0883, 0.7013],
    31566: ["Vallègue", 43.4256, 1.7544],
    31567: ["Vallesvilles", 43.5947, 1.6498],
    31568: ["Varennes", 43.4764, 1.6924],
    31569: ["Vaudreuille", 43.4249, 2.0011],
    31570: ["Vaux", 43.4605, 1.8284],
    31571: ["Vendine", 43.5907, 1.7654],
    31572: ["Venerque", 43.4337, 1.4666],
    31573: ["Verfeil", 43.6637, 1.6773],
    31574: ["Vernet", 43.4279, 1.4213],
    31575: ["Vieille-Toulouse", 43.5275, 1.4383],
    31576: ["Vieillevigne", 43.4072, 1.6591],
    31577: ["Vignaux", 43.6872, 1.0669],
    31578: ["Vigoulet-Auzil", 43.5076, 1.4476],
    31579: ["Villariès", 43.7508, 1.4909],
    31580: ["Villate", 43.4676, 1.3819],
    31581: ["Villaudric", 43.8317, 1.4357],
    31582: ["Villefranche-de-Lauragais", 43.4067, 1.7196],
    31583: ["Villematier", 43.8287, 1.4968],
    31584: ["Villemur-sur-Tarn", 43.8644, 1.4904],
    31585: ["Villeneuve-de-Rivière", 43.1223, 0.6700],
    31586: ["Villeneuve-Lécussan", 43.1343, 0.4851],
    31587: ["Villeneuve-lès-Bouloc", 43.7737, 1.4238],
    31588: ["Villeneuve-Tolosane", 43.5266, 1.3464],
    31589: ["Villenouvelle", 43.4379, 1.6630],
    31590: ["Binos", 42.9047, 0.6084],
    31591: ["Escoulis", 43.1114, 1.0313],
    31592: ["Larra", 43.7340, 1.2242],
    31593: ["Cazac", 43.3475, 0.9525]
};
