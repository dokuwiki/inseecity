<?php
/**
 * @license    http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html
 * @author     Francois Merciol <dokuplugin@merciol.fr>
 *
 * Metadata for configuration manager plugin
 * Additions for the INSEE city plugin
 */
$meta['allDep']       = array ('onoff');
$meta['selectedDeps'] = array ('string');
$meta['maxAutocomplete'] = array ('numeric');
