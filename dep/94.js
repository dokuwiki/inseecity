/*
  Sources :
  https://www.insee.fr/fr/information/4316069
  http://www.geonames.org/export/codes.html
  http://download.geonames.org/export/dump/
*/

var inseeCityNameLatLonDep;
if (inseeCityNameLatLonDep === undefined)
    inseeCityNameLatLonDep = new Map ();

inseeCityNameLatLonDep[94] = {
    94001: ["Ablon-sur-Seine", 48.7238, 2.4211],
    94002: ["Alfortville", 48.7960, 2.4212],
    94003: ["Arcueil", 48.8058, 2.3335],
    94004: ["Boissy-Saint-Léger", 48.7470, 2.5252],
    94011: ["Bonneuil-sur-Marne", 48.7733, 2.4879],
    94015: ["Bry-sur-Marne", 48.8384, 2.5231],
    94016: ["Cachan", 48.7916, 2.3315],
    94017: ["Champigny-sur-Marne", 48.8172, 2.5170],
    94018: ["Charenton-le-Pont", 48.8228, 2.4076],
    94019: ["Chennevières-sur-Marne", 48.7976, 2.5415],
    94021: ["Chevilly-Larue", 48.7668, 2.3530],
    94022: ["Choisy-le-Roi", 48.7643, 2.4174],
    94028: ["Créteil", 48.7837, 2.4546],
    94033: ["Fontenay-sous-Bois", 48.8511, 2.4739],
    94034: ["Fresnes", 48.7571, 2.3261],
    94037: ["Gentilly", 48.8132, 2.3442],
    94038: ["L'Haÿ-les-Roses", 48.7763, 2.3377],
    94041: ["Ivry-sur-Seine", 48.8128, 2.3877],
    94042: ["Joinville-le-Pont", 48.8193, 2.4704],
    94043: ["Le Kremlin-Bicêtre", 48.8092, 2.3565],
    94044: ["Limeil-Brévannes", 48.7450, 2.4893],
    94046: ["Maisons-Alfort", 48.8063, 2.4381],
    94047: ["Mandres-les-Roses", 48.7058, 2.5483],
    94048: ["Marolles-en-Brie", 48.7394, 2.5549],
    94052: ["Nogent-sur-Marne", 48.8366, 2.4816],
    94053: ["Noiseau", 48.7743, 2.5554],
    94054: ["Orly", 48.7429, 2.3946],
    94055: ["Ormesson-sur-Marne", 48.7854, 2.5392],
    94056: ["Périgny", 48.6968, 2.5616],
    94058: ["Le Perreux-sur-Marne", 48.8423, 2.5040],
    94059: ["Le Plessis-Trévise", 48.8060, 2.5761],
    94060: ["La Queue-en-Brie", 48.7775, 2.5838],
    94065: ["Rungis", 48.7494, 2.3528],
    94067: ["Saint-Mandé", 48.8424, 2.4192],
    94068: ["Saint-Maur-des-Fossés", 48.7990, 2.4938],
    94069: ["Saint-Maurice", 48.8184, 2.4374],
    94070: ["Santeny", 48.7356, 2.5763],
    94071: ["Sucy-en-Brie", 48.7656, 2.5330],
    94073: ["Thiais", 48.7607, 2.3853],
    94074: ["Valenton", 48.7528, 2.4612],
    94075: ["Villecresnes", 48.7208, 2.5315],
    94076: ["Villejuif", 48.7928, 2.3600],
    94077: ["Villeneuve-le-Roi", 48.7320, 2.4103],
    94078: ["Villeneuve-Saint-Georges", 48.7413, 2.4489],
    94079: ["Villiers-sur-Marne", 48.8259, 2.5452],
    94080: ["Vincennes", 48.8473, 2.4379],
    94081: ["Vitry-sur-Seine", 48.7882, 2.3941]
};
