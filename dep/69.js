/*
  Sources :
  https://www.insee.fr/fr/information/4316069
  http://www.geonames.org/export/codes.html
  http://download.geonames.org/export/dump/
*/

var inseeCityNameLatLonDep;
if (inseeCityNameLatLonDep === undefined)
    inseeCityNameLatLonDep = new Map ();

inseeCityNameLatLonDep[69] = {
    69001: ["Affoux", 45.8440, 4.4139],
    69002: ["Aigueperse", 46.2801, 4.4233],
    69003: ["Albigny-sur-Saône", 45.8642, 4.8307],
    69004: ["Alix", 45.9168, 4.6550],
    69005: ["Ambérieux", 45.9288, 4.7410],
    69006: ["Amplepuis", 45.9651, 4.3267],
    69007: ["Ampuis", 45.5032, 4.8031],
    69008: ["Ancy", 45.8321, 4.5037],
    69009: ["Anse", 45.9422, 4.7079],
    69010: ["L'Arbresle", 45.8356, 4.6093],
    69012: ["Les Ardillats", 46.1887, 4.5364],
    69013: ["Arnas", 46.0186, 4.7105],
    69014: ["Aveize", 45.6794, 4.4755],
    69015: ["Avenas", 46.1958, 4.6002],
    69016: ["Azolette", 46.1918, 4.4132],
    69017: ["Bagnols", 45.9087, 4.6201],
    69018: ["Beaujeu", 46.1564, 4.5839],
    69019: ["Belleville", 46.1021, 4.7310],
    69020: ["Belmont-d'Azergues", 45.8674, 4.6681],
    69021: ["Bessenay", 45.7743, 4.5473],
    69022: ["Bibost", 45.7969, 4.5489],
    69023: ["Blacé", 46.0346, 4.6356],
    69024: ["Le Bois-d'Oingt", 45.9188, 4.5787],
    69025: ["Bourg-de-Thizy", 46.0408, 4.2880],
    69026: ["Le Breuil", 45.8908, 4.5910],
    69027: ["Brignais", 45.6781, 4.7526],
    69028: ["Brindas", 45.7203, 4.7029],
    69029: ["Bron", 45.7344, 4.9116],
    69030: ["Brullioles", 45.7635, 4.4926],
    69031: ["Brussieu", 45.7472, 4.5171],
    69032: ["Bully", 45.8591, 4.5810],
    69033: ["Cailloux-sur-Fontaines", 45.8603, 4.8837],
    69034: ["Caluire-et-Cuire", 45.7974, 4.8512],
    69035: ["Cenves", 46.2792, 4.6609],
    69036: ["Cercié", 46.1259, 4.6671],
    69037: ["Chambost-Allières", 46.0150, 4.5100],
    69038: ["Chambost-Longessaigne", 45.7739, 4.3795],
    69039: ["Chamelet", 45.9847, 4.4986],
    69040: ["Champagne-au-Mont-d'Or", 45.7983, 4.7866],
    69041: ["La Chapelle-de-Mardore", 46.0501, 4.3585],
    69042: ["La Chapelle-sur-Coise", 45.6501, 4.4960],
    69043: ["Chaponost", 45.7088, 4.7454],
    69044: ["Charbonnières-les-Bains", 45.7797, 4.7432],
    69045: ["Charentay", 46.0840, 4.6893],
    69046: ["Charly", 45.6508, 4.7897],
    69047: ["Charnay", 45.8911, 4.6606],
    69048: ["Chassagny", 45.6052, 4.7309],
    69049: ["Chasselay", 45.8749, 4.7708],
    69050: ["Châtillon", 45.8705, 4.6421],
    69051: ["Chaussan", 45.6374, 4.6344],
    69052: ["Chazay-d'Azergues", 45.8763, 4.7092],
    69053: ["Chénas", 46.2160, 4.7084],
    69054: ["Chénelette", 46.1691, 4.4912],
    69055: ["Les Chères", 45.8931, 4.7432],
    69056: ["Chessy", 45.8906, 4.6163],
    69057: ["Chevinay", 45.7735, 4.5966],
    69058: ["Chiroubles", 46.1855, 4.6550],
    69059: ["Civrieux-d'Azergues", 45.8549, 4.7113],
    69060: ["Claveisolles", 46.0976, 4.5110],
    69061: ["Cogny", 45.9877, 4.6102],
    69062: ["Coise", 45.6138, 4.4690],
    69063: ["Collonges-au-Mont-d'Or", 45.8220, 4.8419],
    69064: ["Condrieu", 45.4743, 4.7567],
    69065: ["Corcelles-en-Beaujolais", 46.1538, 4.7250],
    69066: ["Cours-la-Ville", 46.1087, 4.3369],
    69067: ["Courzieu", 45.7410, 4.5702],
    69068: ["Couzon-au-Mont-d'Or", 45.8482, 4.8223],
    69069: ["Craponne", 45.7462, 4.7267],
    69070: ["Cublize", 46.0245, 4.3873],
    69071: ["Curis-au-Mont-d'Or", 45.8694, 4.8184],
    69072: ["Dardilly", 45.8120, 4.7466],
    69073: ["Dareizé", 45.9054, 4.4883],
    69074: ["Denicé", 46.0045, 4.6514],
    69075: ["Dième", 45.9618, 4.4609],
    69076: ["Dommartin", 45.8357, 4.7156],
    69077: ["Dracé", 46.1537, 4.7731],
    69078: ["Duerne", 45.6813, 4.5231],
    69080: ["échalas", 45.5506, 4.7299],
    69081: ["écully", 45.7821, 4.7721],
    69082: ["émeringes", 46.2255, 4.6733],
    69083: ["éveux", 45.8241, 4.6222],
    69084: ["Fleurie", 46.1951, 4.6922],
    69085: ["Fleurieu-sur-Saône", 45.8589, 4.8503],
    69086: ["Fleurieux-sur-l'Arbresle", 45.8393, 4.6522],
    69087: ["Fontaines-Saint-Martin", 45.8433, 4.8541],
    69088: ["Fontaines-sur-Saône", 45.8282, 4.8566],
    69089: ["Francheville", 45.7378, 4.7550],
    69090: ["Frontenas", 45.9219, 4.6323],
    69091: ["Givors", 45.5816, 4.7572],
    69092: ["Gleizé", 45.9910, 4.6879],
    69093: ["Grandris", 46.0333, 4.4593],
    69094: ["Grézieu-la-Varenne", 45.7477, 4.6895],
    69095: ["Grézieu-le-Marché", 45.6625, 4.4212],
    69096: ["Grigny", 45.6080, 4.7864],
    69097: ["Les Haies", 45.5127, 4.7386],
    69098: ["Les Halles", 45.7161, 4.4348],
    69099: ["Haute-Rivoire", 45.7143, 4.4056],
    69100: ["Irigny", 45.6748, 4.8181],
    69101: ["Jarnioux", 45.9694, 4.6322],
    69102: ["Joux", 45.8905, 4.3673],
    69103: ["Juliénas", 46.2415, 4.7040],
    69104: ["Jullié", 46.2435, 4.6644],
    69105: ["Lacenas", 45.9863, 4.6474],
    69106: ["Lachassagne", 45.9270, 4.6790],
    69107: ["Lamure-sur-Azergues", 46.0528, 4.5088],
    69108: ["Lancié", 46.1730, 4.7257],
    69109: ["Lantignié", 46.1557, 4.6186],
    69110: ["Larajasse", 45.6079, 4.5145],
    69111: ["Légny", 45.9039, 4.5741],
    69112: ["Lentilly", 45.8125, 4.6710],
    69113: ["Létra", 45.9718, 4.5261],
    69114: ["Liergues", 45.9680, 4.6642],
    69115: ["Limas", 45.9720, 4.7191],
    69116: ["Limonest", 45.8303, 4.7716],
    69117: ["Lissieu", 45.8549, 4.7461],
    69118: ["Loire-sur-Rhône", 45.5468, 4.7829],
    69119: ["Longes", 45.5046, 4.6801],
    69120: ["Longessaigne", 45.7855, 4.4264],
    69121: ["Lozanne", 45.8530, 4.6854],
    69122: ["Lucenay", 45.9134, 4.7093],
    69124: ["Marchampt", 46.1084, 4.5610],
    69125: ["Marcilly-d'Azergues", 45.8679, 4.7309],
    69126: ["Marcy", 45.9139, 4.6760],
    69127: ["Marcy-l'étoile", 45.7852, 4.7125],
    69128: ["Mardore", 46.0701, 4.3420],
    69129: ["Marnand", 46.0320, 4.3303],
    69130: ["Meaux-la-Montagne", 46.0459, 4.4216],
    69131: ["Messimy", 45.6988, 4.6714],
    69132: ["Meys", 45.6818, 4.4007],
    69133: ["Millery", 45.6304, 4.7797],
    69134: ["Moiré", 45.9262, 4.6017],
    69135: ["Monsols", 46.2227, 4.5139],
    69136: ["Montagny", 45.6247, 4.7503],
    69137: ["Montmelas-Saint-Sorlin", 46.0177, 4.6018],
    69138: ["Montromant", 45.7109, 4.5355],
    69139: ["Montrottier", 45.8005, 4.4636],
    69140: ["Morancé", 45.8941, 4.6997],
    69141: ["Mornant", 45.6159, 4.6737],
    69142: ["La Mulatière", 45.7303, 4.8122],
    69143: ["Neuville-sur-Saône", 45.8829, 4.8437],
    69144: ["Nuelles", 45.8480, 4.6271],
    69145: ["Odenas", 46.0937, 4.6450],
    69146: ["Oingt", 45.9483, 4.5859],
    69147: ["Les Olmes", 45.8839, 4.5239],
    69148: ["Orliénas", 45.6609, 4.7207],
    69149: ["Oullins", 45.7149, 4.8029],
    69150: ["Ouroux", 46.2252, 4.5818],
    69151: ["Le Perréon", 46.0696, 4.5801],
    69152: ["Pierre-Bénite", 45.7022, 4.8270],
    69153: ["Poleymieux-au-Mont-d'Or", 45.8552, 4.7947],
    69154: ["Pollionnay", 45.7685, 4.6586],
    69155: ["Pomeys", 45.6462, 4.4457],
    69156: ["Pommiers", 45.9582, 4.6884],
    69157: ["Pontcharra-sur-Turdine", 45.8770, 4.4957],
    69158: ["Pont-Trambouze", 46.0691, 4.3120],
    69159: ["Pouilly-le-Monial", 45.9554, 4.6455],
    69160: ["Poule-les-écharmeaux", 46.1426, 4.4607],
    69161: ["Propières", 46.1927, 4.4513],
    69162: ["Quincié-en-Beaujolais", 46.1132, 4.6080],
    69163: ["Quincieux", 45.9136, 4.7737],
    69164: ["Ranchal", 46.1234, 4.4024],
    69165: ["Régnié-Durette", 46.1466, 4.6417],
    69166: ["Riverie", 45.5992, 4.5890],
    69167: ["Rivolet", 46.0065, 4.5801],
    69168: ["Rochetaillée-sur-Saône", 45.8431, 4.8385],
    69169: ["Ronno", 45.9873, 4.3898],
    69170: ["Rontalon", 45.6566, 4.6183],
    69171: ["Sain-Bel", 45.8121, 4.5992],
    69172: ["Salles-Arbuissonnas-en-Beaujolais", 46.0453, 4.6308],
    69173: ["Sarcey", 45.8850, 4.5550],
    69174: ["Les Sauvages", 45.9238, 4.3739],
    69175: ["Savigny", 45.8200, 4.5658],
    69176: ["Soucieu-en-Jarrest", 45.6769, 4.6922],
    69177: ["Sourcieux-les-Mines", 45.7982, 4.6313],
    69178: ["Souzy", 45.7040, 4.4510],
    69179: ["Saint-Andéol-le-Château", 45.5895, 4.6955],
    69180: ["Saint-André-la-Côte", 45.6322, 4.5966],
    69181: ["Saint-Appolinaire", 45.9769, 4.4266],
    69182: ["Saint-Bonnet-des-Bruyères", 46.2663, 4.4720],
    69183: ["Saint-Bonnet-le-Troncy", 46.0764, 4.4197],
    69184: ["Sainte-Catherine", 45.6030, 4.5727],
    69185: ["Saint-Christophe", 46.2545, 4.5335],
    69186: ["Saint-Clément-de-Vers", 46.2265, 4.4085],
    69187: ["Saint-Clément-les-Places", 45.7561, 4.4206],
    69188: ["Saint-Clément-sur-Valsonne", 45.9245, 4.4598],
    69189: ["Sainte-Colombe", 45.5250, 4.8593],
    69190: ["Sainte-Consorce", 45.7744, 4.6935],
    69191: ["Saint-Cyr-au-Mont-d'Or", 45.8194, 4.8181],
    69192: ["Saint-Cyr-le-Chatoux", 46.0314, 4.5386],
    69193: ["Saint-Cyr-sur-le-Rhône", 45.5166, 4.8325],
    69194: ["Saint-Didier-au-Mont-d'Or", 45.8147, 4.7984],
    69195: ["Saint-Didier-sous-Riverie", 45.5926, 4.6054],
    69196: ["Saint-Didier-sur-Beaujeu", 46.1460, 4.5349],
    69197: ["Saint-étienne-des-Oullières", 46.0632, 4.6560],
    69198: ["Saint-étienne-la-Varenne", 46.0771, 4.6221],
    69199: ["Saint-Fons", 45.7011, 4.8504],
    69200: ["Saint-Forgeux", 45.8538, 4.4633],
    69201: ["Sainte-Foy-l'Argentière", 45.7046, 4.4705],
    69202: ["Sainte-Foy-lès-Lyon", 45.7359, 4.7934],
    69203: ["Saint-Genis-l'Argentière", 45.7133, 4.4953],
    69204: ["Saint-Genis-Laval", 45.6936, 4.7891],
    69205: ["Saint-Genis-les-Ollières", 45.7610, 4.7260],
    69206: ["Saint-Georges-de-Reneins", 46.0576, 4.7206],
    69207: ["Saint-Germain-au-Mont-d'Or", 45.8822, 4.8014],
    69208: ["Saint-Germain-sur-l'Arbresle", 45.8643, 4.6129],
    69209: ["Saint-Igny-de-Vers", 46.2368, 4.4415],
    69210: ["Saint-Jacques-des-Arrêts", 46.2560, 4.6071],
    69211: ["Saint-Jean-d'Ardières", 46.1270, 4.7221],
    69212: ["Saint-Jean-des-Vignes", 45.8746, 4.6824],
    69213: ["Saint-Jean-de-Touslas", 45.5728, 4.6720],
    69214: ["Saint-Jean-la-Bussière", 46.0024, 4.3330],
    69215: ["Saint-Julien", 46.0224, 4.6515],
    69216: ["Saint-Julien-sur-Bibost", 45.8002, 4.5099],
    69217: ["Saint-Just-d'Avray", 45.9996, 4.4526],
    69218: ["Saint-Lager", 46.1119, 4.6760],
    69219: ["Saint-Laurent-d'Agny", 45.6427, 4.6843],
    69220: ["Saint-Laurent-de-Chamousset", 45.7378, 4.4647],
    69221: ["Saint-Laurent-de-Vaux", 45.7105, 4.6285],
    69222: ["Saint-Laurent-d'Oingt", 45.9392, 4.5637],
    69223: ["Saint-Loup", 45.8936, 4.4894],
    69224: ["Saint-Mamert", 46.2470, 4.5710],
    69225: ["Saint-Marcel-l'éclairé", 45.8745, 4.4224],
    69227: ["Saint-Martin-en-Haut", 45.6582, 4.5582],
    69228: ["Saint-Maurice-sur-Dargoire", 45.5781, 4.6411],
    69229: ["Saint-Nizier-d'Azergues", 46.0832, 4.4623],
    69230: ["Sainte-Paule", 45.9721, 4.5622],
    69231: ["Saint-Pierre-la-Palud", 45.7818, 4.6162],
    69233: ["Saint-Romain-au-Mont-d'Or", 45.8355, 4.8208],
    69234: ["Saint-Romain-de-Popey", 45.8557, 4.5294],
    69235: ["Saint-Romain-en-Gal", 45.5357, 4.8261],
    69236: ["Saint-Romain-en-Gier", 45.5657, 4.7040],
    69237: ["Saint-Sorlin", 45.6194, 4.6299],
    69238: ["Saint-Symphorien-sur-Coise", 45.6308, 4.4504],
    69239: ["Saint-Vérand", 45.9219, 4.5196],
    69240: ["Saint-Vincent-de-Reins", 46.0739, 4.3862],
    69241: ["Taluyers", 45.6362, 4.7243],
    69242: ["Taponas", 46.1258, 4.7626],
    69243: ["Tarare", 45.9036, 4.4237],
    69244: ["Tassin-la-Demi-Lune", 45.7628, 4.7554],
    69245: ["Ternand", 45.9482, 4.5217],
    69246: ["Theizé", 45.9407, 4.6273],
    69247: ["Thel", 46.1152, 4.3729],
    69248: ["Thizy", 46.0302, 4.3139],
    69249: ["Thurins", 45.6838, 4.6261],
    69250: ["La Tour-de-Salvagny", 45.8104, 4.7123],
    69251: ["Trades", 46.2738, 4.5661],
    69252: ["Trèves", 45.5348, 4.6896],
    69253: ["Tupin-et-Semons", 45.4902, 4.7745],
    69254: ["Valsonne", 45.9469, 4.4216],
    69255: ["Vaugneray", 45.7318, 4.6437],
    69256: ["Vaulx-en-Velin", 45.7858, 4.9263],
    69257: ["Vaux-en-Beaujolais", 46.0460, 4.5769],
    69258: ["Vauxrenard", 46.2172, 4.6381],
    69259: ["Vénissieux", 45.7037, 4.8813],
    69260: ["Vernaison", 45.6493, 4.8093],
    69261: ["Vernay", 46.1645, 4.5266],
    69263: ["Villechenève", 45.8168, 4.4073],
    69264: ["Villefranche-sur-Saône", 45.9874, 4.7317],
    69265: ["Ville-sur-Jarnioux", 45.9699, 4.5978],
    69266: ["Villeurbanne", 45.7707, 4.8884],
    69267: ["Villié-Morgon", 46.1597, 4.6711],
    69268: ["Vourles", 45.6578, 4.7681],
    69269: ["Yzeron", 45.7095, 4.5954],
    69270: ["Chaponnay", 45.6282, 4.9502],
    69271: ["Chassieu", 45.7376, 4.9617],
    69272: ["Communay", 45.5981, 4.8386],
    69273: ["Corbas", 45.6680, 4.9084],
    69275: ["Décines-Charpieu", 45.7717, 4.9614],
    69276: ["Feyzin", 45.6730, 4.8572],
    69277: ["Genas", 45.7297, 5.0159],
    69278: ["Genay", 45.8980, 4.8386],
    69279: ["Jonage", 45.7911, 5.0415],
    69280: ["Jons", 45.8001, 5.0784],
    69281: ["Marennes", 45.6230, 4.9075],
    69282: ["Meyzieu", 45.7770, 5.0063],
    69283: ["Mions", 45.6647, 4.9491],
    69284: ["Montanay", 45.8771, 4.8677],
    69285: ["Pusignan", 45.7538, 5.0670],
    69286: ["Rillieux-la-Pape", 45.8205, 4.8982],
    69287: ["Saint-Bonnet-de-Mure", 45.6961, 5.0222],
    69288: ["Saint-Laurent-de-Mure", 45.6834, 5.0608],
    69289: ["Saint-Pierre-de-Chandieu", 45.6448, 5.0087],
    69290: ["Saint-Priest", 45.7014, 4.9488],
    69291: ["Saint-Symphorien-d'Ozon", 45.6421, 4.8688],
    69292: ["Sathonay-Camp", 45.8257, 4.8736],
    69293: ["Sathonay-Village", 45.8402, 4.8856],
    69294: ["Sérézin-du-Rhône", 45.6275, 4.8248],
    69295: ["Simandres", 45.6115, 4.8710],
    69296: ["Solaize", 45.6460, 4.8374],
    69297: ["Ternay", 45.6055, 4.8075],
    69298: ["Toussieu", 45.6577, 4.9818],
    69299: ["Colombier-Saugnieu", 45.7178, 5.1033],
    69381: ["1er arrondissement de Lyon", 45.7699, 4.8292],
    69382: ["2e arrondissement de Lyon", 45.7492, 4.8261],
    69383: ["3e arrondissement de Lyon", 45.7533, 4.8691],
    69384: ["4e arrondissement de Lyon", 45.7786, 4.8239],
    69385: ["5e arrondissement de Lyon", 45.7558, 4.8022],
    69386: ["6e arrondissement de Lyon", 45.7728, 4.8520],
    69387: ["7e arrondissement de Lyon", 45.7334, 4.8375],
    69388: ["8e arrondissement de Lyon", 45.7342, 4.8693],
    69389: ["9e arrondissement de Lyon", 45.7817, 4.8081]
};
