/*
  Sources :
  https://www.insee.fr/fr/information/4316069
  http://www.geonames.org/export/codes.html
  http://download.geonames.org/export/dump/
*/

var inseeCityNameLatLonDep;
if (inseeCityNameLatLonDep === undefined)
    inseeCityNameLatLonDep = new Map ();

inseeCityNameLatLonDep[75] = {
    75101: ["Paris 1er", 48.86263, 2.33629],
    75102: ["Paris 2e", 48.86790, 2.34410],
    75103: ["Paris 3e", 48.86305, 2.35936],
    75104: ["Paris 4e", 48.85422, 2.35736],
    75105: ["Paris 5e", 48.84450, 2.34985],
    75106: ["Paris 6e", 48.84896, 2.33267],
    75107: ["Paris 7e", 48.85608, 2.31243],
    75108: ["Paris 8e", 48.87252, 2.31258],
    75109: ["Paris 9e", 48.87689, 2.33746],
    75110: ["Paris 10e", 48.87602, 2.36111],
    75111: ["Paris 11e", 48.85941, 2.37874],
    75112: ["Paris 12e", 48.83515, 2.41980],
    75113: ["Paris 13e", 48.82871, 2.36246],
    75114: ["Paris 14e", 48.82899, 2.32710],
    75115: ["Paris 15e", 48.84015, 2.29355],
    75116: ["Paris 16e", 48.86039, 2.26209],
    75117: ["Paris 17e", 48.88733, 2.30748],
    75118: ["Paris 18e", 48.89273, 2.34871],
    75119: ["Paris 19e", 48.88686, 2.38469],
    75120: ["Paris 20e", 48.86318, 2.40081]
};
