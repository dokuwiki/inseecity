/*
  Sources :
  https://www.insee.fr/fr/information/4316069
  http://www.geonames.org/export/codes.html
  http://download.geonames.org/export/dump/
*/

var inseeCityNameLatLonDep;
if (inseeCityNameLatLonDep === undefined)
    inseeCityNameLatLonDep = new Map ();

inseeCityNameLatLonDep[83] = {
    83001: ["Les Adrets-de-l'Estérel", 43.5356, 6.8110],
    83002: ["Aiguines", 43.7284, 6.3043],
    83003: ["Ampus", 43.6283, 6.3718],
    83004: ["Les Arcs", 43.4509, 6.4897],
    83005: ["Artignosc-sur-Verdon", 43.6940, 6.1076],
    83006: ["Artigues", 43.5765, 5.8023],
    83007: ["Aups", 43.6246, 6.2213],
    83008: ["Bagnols-en-Forêt", 43.5330, 6.7085],
    83009: ["Bandol", 43.1473, 5.7478],
    83010: ["Bargème", 43.7301, 6.5708],
    83011: ["Bargemon", 43.6333, 6.5424],
    83012: ["Barjols", 43.5523, 5.9877],
    83013: ["La Bastide", 43.7479, 6.6326],
    83014: ["Baudinard-sur-Verdon", 43.7138, 6.1335],
    83015: ["Bauduen", 43.7157, 6.2020],
    83016: ["Le Beausset", 43.2069, 5.8202],
    83017: ["Belgentier", 43.2399, 5.9966],
    83018: ["Besse-sur-Issole", 43.3399, 6.1777],
    83019: ["Bormes-les-Mimosas", 43.1622, 6.3495],
    83020: ["Le Bourguet", 43.7800, 6.5034],
    83021: ["Bras", 43.4704, 5.9585],
    83022: ["Brenon", 43.7658, 6.5377],
    83023: ["Brignoles", 43.3992, 6.0774],
    83025: ["Brue-Auriac", 43.5279, 5.9366],
    83026: ["Cabasse", 43.4270, 6.2320],
    83027: ["La Cadière-d'Azur", 43.2032, 5.7247],
    83028: ["Callas", 43.5655, 6.5732],
    83029: ["Callian", 43.6125, 6.7570],
    83030: ["Camps-la-Source", 43.3725, 6.1212],
    83031: ["Le Cannet-des-Maures", 43.3737, 6.3755],
    83032: ["Carcès", 43.4690, 6.1763],
    83033: ["Carnoules", 43.2945, 6.1966],
    83034: ["Carqueiranne", 43.0971, 6.0717],
    83035: ["Le Castellet", 43.2276, 5.7656],
    83036: ["Cavalaire-sur-Mer", 43.1819, 6.5212],
    83037: ["La Celle", 43.3843, 5.9986],
    83038: ["Châteaudouble", 43.6234, 6.4356],
    83039: ["Châteauvert", 43.5012, 6.0112],
    83040: ["Châteauvieux", 43.7886, 6.5956],
    83041: ["Claviers", 43.5988, 6.5856],
    83042: ["Cogolin", 43.2464, 6.5219],
    83043: ["Collobrières", 43.2414, 6.3403],
    83044: ["Comps-sur-Artuby", 43.7035, 6.5029],
    83045: ["Correns", 43.4864, 6.0682],
    83046: ["Cotignac", 43.5233, 6.1449],
    83047: ["La Crau", 43.1635, 6.0926],
    83048: ["La Croix-Valmer", 43.1951, 6.5862],
    83049: ["Cuers", 43.2411, 6.0685],
    83050: ["Draguignan", 43.5357, 6.4543],
    83051: ["Entrecasteaux", 43.5119, 6.2347],
    83052: ["Esparron", 43.5874, 5.8487],
    83053: ["évenos", 43.1862, 5.8730],
    83054: ["La Farlède", 43.1615, 6.0427],
    83055: ["Fayence", 43.6103, 6.6784],
    83056: ["Figanières", 43.5672, 6.4987],
    83057: ["Flassans-sur-Issole", 43.3760, 6.2216],
    83058: ["Flayosc", 43.5422, 6.3627],
    83059: ["Forcalqueiret", 43.3366, 6.0914],
    83060: ["Fox-Amphoux", 43.5955, 6.1090],
    83061: ["Fréjus", 43.4719, 6.7636],
    83062: ["La Garde", 43.1263, 6.0189],
    83063: ["La Garde-Freinet", 43.3234, 6.4669],
    83064: ["Garéoult", 43.3403, 6.0408],
    83065: ["Gassin", 43.2392, 6.5861],
    83066: ["Ginasservis", 43.6592, 5.8375],
    83067: ["Gonfaron", 43.3206, 6.3003],
    83068: ["Grimaud", 43.2820, 6.5330],
    83069: ["Hyères", 43.1018, 6.1889],
    83070: ["Le Lavandou", 43.1656, 6.4123],
    83071: ["La Londe-les-Maures", 43.1689, 6.2428],
    83072: ["Lorgues", 43.4814, 6.3589],
    83073: ["Le Luc", 43.3804, 6.3059],
    83074: ["La Martre", 43.7635, 6.5926],
    83075: ["Les Mayons", 43.3069, 6.3599],
    83076: ["Mazaugues", 43.3448, 5.8958],
    83077: ["Méounes-lès-Montrieux", 43.2739, 5.9667],
    83078: ["Moissac-Bellevue", 43.6471, 6.1670],
    83079: ["La Môle", 43.2159, 6.4765],
    83080: ["Mons", 43.6989, 6.7089],
    83081: ["Montauroux", 43.5990, 6.7898],
    83082: ["Montferrat", 43.6363, 6.4784],
    83083: ["Montfort-sur-Argens", 43.4812, 6.1256],
    83084: ["Montmeyan", 43.6482, 6.0612],
    83085: ["La Motte", 43.5078, 6.5505],
    83086: ["Le Muy", 43.4687, 6.5769],
    83087: ["Nans-les-Pins", 43.3790, 5.7848],
    83088: ["Néoules", 43.2945, 6.0259],
    83089: ["Ollières", 43.5101, 5.8164],
    83090: ["Ollioules", 43.1386, 5.8536],
    83091: ["Pierrefeu-du-Var", 43.2345, 6.1795],
    83092: ["Pignans", 43.2968, 6.2544],
    83093: ["Plan-d'Aups-Sainte-Baume", 43.3337, 5.7385],
    83094: ["Le Plan-de-la-Tour", 43.3384, 6.5474],
    83095: ["Pontevès", 43.5507, 6.0629],
    83096: ["Pourcieux", 43.4646, 5.7886],
    83097: ["Pourrières", 43.4938, 5.7471],
    83098: ["Le Pradet", 43.0994, 6.0284],
    83099: ["Puget-sur-Argens", 43.4717, 6.6862],
    83100: ["Puget-Ville", 43.2763, 6.1489],
    83101: ["Ramatuelle", 43.2186, 6.6375],
    83102: ["Régusse", 43.6676, 6.1181],
    83103: ["Le Revest-les-Eaux", 43.1824, 5.9456],
    83104: ["Rians", 43.6055, 5.7413],
    83105: ["Riboux", 43.3081, 5.7572],
    83106: ["Rocbaron", 43.3066, 6.0836],
    83107: ["Roquebrune-sur-Argens", 43.4285, 6.6521],
    83108: ["La Roquebrussanne", 43.3371, 5.9839],
    83109: ["La Roque-Esclapon", 43.7213, 6.6481],
    83110: ["Rougiers", 43.3818, 5.8514],
    83111: ["Sainte-Anastasie-sur-Issole", 43.3334, 6.1249],
    83112: ["Saint-Cyr-sur-Mer", 43.1720, 5.7083],
    83113: ["Saint-Julien", 43.6999, 5.9149],
    83114: ["Saint-Martin-de-Pallières", 43.5741, 5.8879],
    83115: ["Sainte-Maxime", 43.3564, 6.6117],
    83116: ["Saint-Maximin-la-Sainte-Baume", 43.4509, 5.8596],
    83117: ["Saint-Paul-en-Forêt", 43.5647, 6.6826],
    83118: ["Saint-Raphaël", 43.4574, 6.8473],
    83119: ["Saint-Tropez", 43.2623, 6.6634],
    83120: ["Saint-Zacharie", 43.3775, 5.7182],
    83121: ["Salernes", 43.5621, 6.2287],
    83122: ["Les Salles-sur-Verdon", 43.7670, 6.2065],
    83123: ["Sanary-sur-Mer", 43.1382, 5.7958],
    83124: ["Seillans", 43.6466, 6.6130],
    83125: ["Seillons-Source-d'Argens", 43.5150, 5.8769],
    83126: ["La Seyne-sur-Mer", 43.0880, 5.8708],
    83127: ["Signes", 43.2776, 5.8571],
    83128: ["Sillans-la-Cascade", 43.5658, 6.1651],
    83129: ["Six-Fours-les-Plages", 43.0868, 5.8292],
    83130: ["Solliès-Pont", 43.1907, 6.0632],
    83131: ["Solliès-Toucas", 43.2128, 5.9869],
    83132: ["Solliès-Ville", 43.1784, 6.0172],
    83133: ["Tanneron", 43.5748, 6.8567],
    83134: ["Taradeau", 43.4612, 6.4306],
    83135: ["Tavernes", 43.6002, 6.0209],
    83136: ["Le Thoronet", 43.4589, 6.2826],
    83137: ["Toulon", 43.1361, 5.9323],
    83138: ["Tourrettes", 43.6140, 6.7237],
    83139: ["Tourtour", 43.5935, 6.3106],
    83140: ["Tourves", 43.4088, 5.9271],
    83141: ["Trans-en-Provence", 43.5024, 6.4894],
    83142: ["Trigance", 43.7414, 6.4364],
    83143: ["Le Val", 43.4452, 6.0517],
    83144: ["La Valette-du-Var", 43.1499, 5.9922],
    83145: ["Varages", 43.5957, 5.9444],
    83146: ["La Verdière", 43.6461, 5.9523],
    83147: ["Vérignon", 43.6625, 6.2765],
    83148: ["Vidauban", 43.4015, 6.4490],
    83149: ["Villecroze", 43.5644, 6.2854],
    83150: ["Vinon-sur-Verdon", 43.7224, 5.8074],
    83151: ["Vins-sur-Caramy", 43.4330, 6.1500],
    83152: ["Rayol-Canadel-sur-Mer", 43.1642, 6.4747],
    83153: ["Saint-Mandrier-sur-Mer", 43.0754, 5.9264],
    83154: ["Saint-Antonin-du-Var", 43.5112, 6.2900]
};
