/*
  Sources :
  https://www.insee.fr/fr/information/4316069
  http://www.geonames.org/export/codes.html
  http://download.geonames.org/export/dump/
*/

var inseeCityNameLatLonDep;
if (inseeCityNameLatLonDep === undefined)
    inseeCityNameLatLonDep = new Map ();

inseeCityNameLatLonDep[24] = {
    24001: ["Abjat-sur-Bandiat", 45.5817, 0.7599],
    24002: ["Agonac", 45.2815, 0.7453],
    24004: ["Ajat", 45.1611, 1.0276],
    24005: ["Alles-sur-Dordogne", 44.8613, 0.8746],
    24006: ["Allas-les-Mines", 44.8280, 1.0737],
    24007: ["Allemans", 45.2884, 0.3052],
    24008: ["Angoisse", 45.4299, 1.1505],
    24009: ["Anlhiac", 45.3214, 1.1208],
    24010: ["Annesse-et-Beaulieu", 45.1826, 0.5953],
    24011: ["Antonne-et-Trigonant", 45.2300, 0.8316],
    24012: ["Archignac", 45.0157, 1.2916],
    24013: ["Atur", 45.1363, 0.7562],
    24014: ["Aubas", 45.0793, 1.1980],
    24015: ["Audrix", 44.8830, 0.9484],
    24016: ["Augignac", 45.5838, 0.7023],
    24018: ["Auriac-du-Périgord", 45.1125, 1.1272],
    24019: ["Azerat", 45.1597, 1.1063],
    24020: ["La Bachellerie", 45.1339, 1.1637],
    24021: ["Badefols-d'Ans", 45.2288, 1.2037],
    24022: ["Badefols-sur-Dordogne", 44.8391, 0.8005],
    24023: ["Baneuil", 44.8529, 0.6934],
    24024: ["Bardou", 44.7409, 0.6860],
    24025: ["Bars", 45.0948, 1.0507],
    24026: ["Bassillac", 45.1789, 0.8209],
    24027: ["Bayac", 44.8047, 0.7227],
    24028: ["Beaumont-du-Périgord", 44.7738, 0.7540],
    24029: ["Beaupouyet", 44.9961, 0.2852],
    24030: ["Beauregard-de-Terrasson", 45.1558, 1.2286],
    24031: ["Beauregard-et-Bassac", 44.9936, 0.6326],
    24032: ["Beauronne", 45.1003, 0.3631],
    24033: ["Beaussac", 45.5097, 0.4820],
    24034: ["Beleymas", 44.9869, 0.4913],
    24035: ["Belvès", 44.7462, 0.9804],
    24036: ["Berbiguières", 44.8394, 1.0451],
    24037: ["Bergerac", 44.8543, 0.4865],
    24038: ["Bertric-Burée", 45.3090, 0.3545],
    24039: ["Besse", 44.6649, 1.1131],
    24040: ["Beynac-et-Cazenac", 44.8572, 1.1322],
    24041: ["Bézenac", 44.8475, 1.0863],
    24042: ["Biras", 45.2868, 0.6389],
    24043: ["Biron", 44.6225, 0.8741],
    24044: ["Blis-et-Born", 45.1694, 0.9081],
    24045: ["Boisse", 44.7162, 0.6565],
    24046: ["Boisseuilh", 45.2814, 1.1780],
    24047: ["La Boissière-d'Ans", 45.2320, 0.9737],
    24048: ["Bonneville-et-Saint-Avit-de-Fumadières", 44.8838, 0.0812],
    24050: ["Borrèze", 44.9746, 1.3910],
    24051: ["Bosset", 44.9518, 0.3622],
    24052: ["Bouillac", 44.7652, 0.9204],
    24053: ["Boulazac", 45.1737, 0.7693],
    24054: ["Bouniagues", 44.7568, 0.5279],
    24055: ["Bourdeilles", 45.3187, 0.5811],
    24056: ["Le Bourdeix", 45.5790, 0.6355],
    24057: ["Bourg-des-Maisons", 45.3372, 0.4344],
    24058: ["Bourg-du-Bost", 45.2670, 0.2632],
    24059: ["Bourgnac", 45.0173, 0.3979],
    24060: ["Bourniquel", 44.8081, 0.7704],
    24061: ["Bourrou", 45.0481, 0.6042],
    24062: ["Bouteilles-Saint-Sébastien", 45.3484, 0.2893],
    24063: ["Bouzic", 44.7210, 1.2171],
    24064: ["Brantôme", 45.3601, 0.6559],
    24065: ["Breuilh", 45.0576, 0.7529],
    24066: ["Brouchaud", 45.2049, 0.9971],
    24067: ["Le Bugue", 44.9264, 0.9246],
    24068: ["Le Buisson-de-Cadouin", 44.8209, 0.8900],
    24069: ["Bussac", 45.2666, 0.6027],
    24070: ["Busserolles", 45.6716, 0.6461],
    24071: ["Bussière-Badil", 45.6446, 0.6018],
    24073: ["Calès", 44.8631, 0.8150],
    24074: ["Calviac-en-Périgord", 44.8610, 1.3221],
    24075: ["Campagnac-lès-Quercy", 44.6954, 1.1751],
    24076: ["Campagne", 44.8978, 0.9775],
    24077: ["Campsegret", 44.9423, 0.5676],
    24079: ["Cantillac", 45.4000, 0.6436],
    24080: ["Capdrot", 44.6788, 0.9451],
    24081: ["Carlux", 44.8869, 1.3598],
    24082: ["Carsac-Aillac", 44.8504, 1.2742],
    24083: ["Carsac-de-Gurson", 44.9404, 0.0946],
    24084: ["Carves", 44.7854, 1.0605],
    24085: ["La Cassagne", 45.0565, 1.3064],
    24086: ["Castelnaud-la-Chapelle", 44.8017, 1.1305],
    24087: ["Castels", 44.8705, 1.0803],
    24088: ["Cause-de-Clérans", 44.8732, 0.6727],
    24089: ["Cazoulès", 44.8822, 1.4289],
    24090: ["Celles", 45.2925, 0.4124],
    24091: ["Cénac-et-Saint-Julien", 44.7868, 1.2034],
    24092: ["Cendrieux", 44.9992, 0.8269],
    24093: ["Cercles", 45.3671, 0.4695],
    24094: ["Chalagnac", 45.0928, 0.6807],
    24095: ["Chalais", 45.5032, 0.9403],
    24096: ["Champagnac-de-Belair", 45.4004, 0.6950],
    24097: ["Champagne-et-Fontaine", 45.4290, 0.3362],
    24098: ["Champcevinel", 45.2212, 0.7250],
    24099: ["Champeaux-et-la-Chapelle-Pommier", 45.4680, 0.5810],
    24100: ["Champniers-et-Reilhac", 45.6717, 0.7212],
    24101: ["Champs-Romain", 45.5404, 0.7715],
    24102: ["Chancelade", 45.2100, 0.6552],
    24103: ["Le Change", 45.2019, 0.8912],
    24104: ["Chantérac", 45.1674, 0.4381],
    24105: ["Chapdeuil", 45.3426, 0.4668],
    24106: ["La Chapelle-Aubareil", 45.0104, 1.1894],
    24107: ["La Chapelle-Faucher", 45.3743, 0.7617],
    24108: ["La Chapelle-Gonaguet", 45.2301, 0.6190],
    24109: ["La Chapelle-Grésignac", 45.3896, 0.3393],
    24110: ["La Chapelle-Montabourlet", 45.3949, 0.4576],
    24111: ["La Chapelle-Montmoreau", 45.4499, 0.6442],
    24113: ["La Chapelle-Saint-Jean", 45.1954, 1.1643],
    24114: ["Chassaignes", 45.2543, 0.2498],
    24115: ["Château-l'évêque", 45.2584, 0.6864],
    24116: ["Châtres", 45.1873, 1.1947],
    24117: ["Chavagnac", 45.0871, 1.3700],
    24118: ["Chenaud", 45.2126, 0.0785],
    24119: ["Cherval", 45.3879, 0.3849],
    24120: ["Cherveix-Cubas", 45.2878, 1.1111],
    24121: ["Chourgnac", 45.2344, 1.0535],
    24122: ["Cladech", 44.8088, 1.0741],
    24123: ["Clermont-de-Beauregard", 44.9493, 0.6477],
    24124: ["Clermont-d'Excideuil", 45.3645, 1.0471],
    24126: ["Colombier", 44.7822, 0.5179],
    24127: ["Coly", 45.0864, 1.2650],
    24128: ["Comberanche-et-épeluche", 45.2768, 0.2800],
    24129: ["Condat-sur-Trincou", 45.3674, 0.7158],
    24130: ["Condat-sur-Vézère", 45.1064, 1.2290],
    24131: ["Connezac", 45.5150, 0.5303],
    24132: ["Conne-de-Labarde", 44.7817, 0.5533],
    24133: ["La Coquille", 45.5444, 0.9656],
    24134: ["Corgnac-sur-l'Isle", 45.3733, 0.9446],
    24135: ["Cornille", 45.2457, 0.7790],
    24136: ["Coubjours", 45.2416, 1.2535],
    24137: ["Coulaures", 45.3003, 0.9738],
    24138: ["Coulounieix-Chamiers", 45.1650, 0.6875],
    24139: ["Coursac", 45.1270, 0.6428],
    24140: ["Cours-de-Pile", 44.8364, 0.5576],
    24141: ["Coutures", 45.3275, 0.3954],
    24142: ["Coux-et-Bigaroque", 44.8481, 0.9678],
    24143: ["Couze-et-Saint-Front", 44.8255, 0.7221],
    24144: ["Creyssac", 45.3128, 0.5498],
    24145: ["Creysse", 44.8643, 0.5489],
    24146: ["Creyssensac-et-Pissot", 45.0708, 0.6680],
    24147: ["Cubjac", 45.2253, 0.9390],
    24148: ["Cunèges", 44.7797, 0.3740],
    24150: ["Daglan", 44.7481, 1.1872],
    24151: ["Doissat", 44.7217, 1.0856],
    24152: ["Domme", 44.8035, 1.2443],
    24153: ["La Dornac", 45.0734, 1.3432],
    24154: ["Douchapt", 45.2320, 0.4456],
    24155: ["Douville", 45.0019, 0.5951],
    24156: ["La Douze", 45.0680, 0.8623],
    24157: ["Douzillac", 45.0903, 0.4077],
    24158: ["Dussac", 45.3955, 1.0731],
    24159: ["échourgnac", 45.1290, 0.2230],
    24160: ["église-Neuve-de-Vergt", 45.0887, 0.7326],
    24161: ["église-Neuve-d'Issac", 44.9811, 0.4278],
    24162: ["Escoire", 45.2075, 0.8514],
    24163: ["étouars", 45.6088, 0.6157],
    24164: ["Excideuil", 45.3293, 1.0621],
    24165: ["Eygurande-et-Gardedeuil", 45.0789, 0.1212],
    24166: ["Eyliac", 45.1550, 0.8557],
    24167: ["Eymet", 44.6734, 0.3946],
    24168: ["Plaisance", 44.6984, 0.5555],
    24170: ["Eyvirat", 45.3287, 0.7380],
    24171: ["Eyzerac", 45.3874, 0.9025],
    24172: ["Les Eyzies-de-Tayac-Sireuil", 44.9350, 1.0403],
    24174: ["Fanlac", 45.0690, 1.0930],
    24175: ["Les Farges", 45.1147, 1.1931],
    24176: ["Faurilles", 44.7025, 0.6870],
    24177: ["Faux", 44.7855, 0.6463],
    24178: ["Festalemps", 45.2174, 0.2422],
    24179: ["La Feuillade", 45.1156, 1.3975],
    24180: ["Firbeix", 45.5924, 0.9420],
    24181: ["Flaugeac", 44.7544, 0.4408],
    24182: ["Le Fleix", 44.8809, 0.2625],
    24183: ["Fleurac", 44.9972, 1.0048],
    24184: ["Florimont-Gaumier", 44.7082, 1.2386],
    24186: ["Fonroque", 44.7047, 0.4098],
    24188: ["Fossemagne", 45.1187, 0.9881],
    24189: ["Fougueyrolles", 44.8684, 0.1887],
    24190: ["Fouleix", 44.9755, 0.6736],
    24191: ["Fraisse", 44.9336, 0.3033],
    24192: ["Gabillou", 45.2059, 1.0322],
    24193: ["Gageac-et-Rouillac", 44.8038, 0.3576],
    24194: ["Gardonne", 44.8311, 0.3309],
    24195: ["Gaugeac", 44.6662, 0.8790],
    24196: ["Génis", 45.3306, 1.1636],
    24197: ["Ginestet", 44.9040, 0.4376],
    24198: ["La Gonterie-Boulouneix", 45.3817, 0.5844],
    24199: ["Gout-Rossignol", 45.4163, 0.4055],
    24200: ["Grand-Brassac", 45.3001, 0.4857],
    24202: ["Granges-d'Ans", 45.2106, 1.1163],
    24203: ["Les Graulges", 45.5010, 0.4449],
    24204: ["Grèzes", 45.1084, 1.3610],
    24205: ["Grignols", 45.0847, 0.5397],
    24206: ["Grives", 44.7629, 1.0776],
    24207: ["Groléjac", 44.8096, 1.2926],
    24208: ["Grun-Bordas", 45.0461, 0.6420],
    24209: ["Hautefaye", 45.5360, 0.5082],
    24210: ["Hautefort", 45.2589, 1.1354],
    24211: ["Issac", 45.0150, 0.4517],
    24212: ["Issigeac", 44.7277, 0.6054],
    24213: ["Jaure", 45.0547, 0.5570],
    24214: ["Javerlhac-et-la-Chapelle-Saint-Robert", 45.5610, 0.5504],
    24215: ["Jayac", 45.0299, 1.3515],
    24216: ["La Jemaye", 45.1605, 0.2625],
    24217: ["Journiac", 44.9659, 0.8857],
    24218: ["Jumilhac-le-Grand", 45.5051, 1.0806],
    24219: ["Labouquerie", 44.7470, 0.7934],
    24220: ["Lacropte", 45.0488, 0.8312],
    24221: ["Rudeau-Ladosse", 45.4905, 0.5330],
    24222: ["La Force", 44.8787, 0.3595],
    24223: ["Lalinde", 44.8557, 0.7415],
    24224: ["Lamonzie-Montastruc", 44.9014, 0.5883],
    24225: ["Lamonzie-Saint-Martin", 44.8334, 0.3877],
    24226: ["Lamothe-Montravel", 44.8527, 0.0166],
    24227: ["Lanouaille", 45.3777, 1.1320],
    24228: ["Lanquais", 44.8142, 0.6689],
    24229: ["Le Lardin-Saint-Lazare", 45.1363, 1.2324],
    24230: ["Larzac", 44.7393, 1.0000],
    24231: ["Lavalade", 44.6942, 0.8603],
    24232: ["Lavaur", 44.6249, 1.0245],
    24233: ["Laveyssière", 44.9353, 0.4392],
    24234: ["Les Lèches", 44.9811, 0.3866],
    24235: ["Léguillac-de-Cercles", 45.3875, 0.5192],
    24236: ["Léguillac-de-l'Auche", 45.1872, 0.5530],
    24237: ["Lembras", 44.8921, 0.5214],
    24238: ["Lempzours", 45.3549, 0.8237],
    24239: ["Ligueux", 45.3091, 0.8126],
    24240: ["Limeuil", 44.8968, 0.8959],
    24241: ["Limeyrat", 45.1652, 0.9773],
    24242: ["Liorac-sur-Louyre", 44.8959, 0.6415],
    24243: ["Lisle", 45.2711, 0.5572],
    24244: ["Lolme", 44.7104, 0.8462],
    24245: ["Loubejac", 44.6047, 1.0745],
    24246: ["Lunas", 44.9246, 0.3990],
    24247: ["Lusignac", 45.3253, 0.3154],
    24248: ["Lussas-et-Nontronneau", 45.5207, 0.5805],
    24249: ["Manaurie", 44.9627, 0.9882],
    24251: ["Manzac-sur-Vern", 45.0844, 0.5904],
    24252: ["Marcillac-Saint-Quentin", 44.9556, 1.1998],
    24253: ["Mareuil", 45.4407, 0.4479],
    24254: ["Marnac", 44.8296, 1.0287],
    24255: ["Marquay", 44.9410, 1.1283],
    24256: ["Marsac-sur-l'Isle", 45.1820, 0.6514],
    24257: ["Marsalès", 44.6997, 0.8906],
    24258: ["Marsaneix", 45.0905, 0.7790],
    24259: ["Maurens", 44.9268, 0.4822],
    24260: ["Mauzac-et-Grand-Castang", 44.8823, 0.7803],
    24261: ["Mauzens-et-Miremont", 44.9907, 0.9291],
    24262: ["Mayac", 45.2792, 0.9515],
    24263: ["Mazeyrolles", 44.6732, 1.0075],
    24264: ["Ménesplet", 45.0036, 0.1054],
    24266: ["Mensignac", 45.2211, 0.5588],
    24267: ["Mescoules", 44.7414, 0.4257],
    24268: ["Meyrals", 44.9077, 1.0703],
    24269: ["Mialet", 45.5689, 0.8978],
    24270: ["Milhac-d'Auberoche", 45.1139, 0.9332],
    24271: ["Milhac-de-Nontron", 45.4728, 0.7914],
    24272: ["Minzac", 44.9616, 0.0299],
    24273: ["Molières", 44.8119, 0.8247],
    24274: ["Monbazillac", 44.7946, 0.4809],
    24276: ["Monestier", 44.7765, 0.3152],
    24277: ["Monfaucon", 44.9145, 0.2556],
    24278: ["Monmadalès", 44.7682, 0.6175],
    24279: ["Monmarvès", 44.7052, 0.6113],
    24280: ["Monpazier", 44.6807, 0.8940],
    24281: ["Monsac", 44.7795, 0.6925],
    24282: ["Monsaguel", 44.7389, 0.5714],
    24283: ["Monsec", 45.4247, 0.5438],
    24284: ["Montagnac-d'Auberoche", 45.1876, 0.9538],
    24285: ["Montagnac-la-Crempse", 44.9785, 0.5429],
    24286: ["Montagrier", 45.2718, 0.4861],
    24287: ["Montaut", 44.7504, 0.6461],
    24288: ["Montazeau", 44.9026, 0.1308],
    24289: ["Montcaret", 44.8618, 0.0660],
    24290: ["Montferrand-du-Périgord", 44.7585, 0.8749],
    24291: ["Montignac", 45.0627, 1.1542],
    24292: ["Montpeyroux", 44.9152, 0.0627],
    24293: ["Monplaisant", 44.7933, 0.9951],
    24294: ["Montpon-Ménestérol", 45.0196, 0.1555],
    24295: ["Montrem", 45.1310, 0.5809],
    24296: ["Mouleydier", 44.8599, 0.6172],
    24297: ["Moulin-Neuf", 45.0000, 0.0583],
    24298: ["Mouzens", 44.8505, 1.0043],
    24299: ["Mussidan", 45.0296, 0.3647],
    24300: ["Nabirat", 44.7651, 1.2907],
    24301: ["Nadaillac", 45.0316, 1.4003],
    24302: ["Nailhac", 45.2237, 1.1548],
    24303: ["Nanteuil-Auriac-de-Bourzac", 45.3801, 0.2876],
    24304: ["Nantheuil", 45.4263, 0.9562],
    24305: ["Nanthiat", 45.4081, 0.9850],
    24306: ["Nastringues", 44.8721, 0.1500],
    24307: ["Naussannes", 44.7501, 0.7187],
    24308: ["Négrondes", 45.3402, 0.8786],
    24309: ["Neuvic", 45.0893, 0.4661],
    24310: ["Nojals-et-Clotte", 44.7275, 0.7637],
    24311: ["Nontron", 45.5303, 0.6839],
    24312: ["Notre-Dame-de-Sanilhac", 45.1273, 0.7127],
    24313: ["Orliac", 44.7131, 1.0617],
    24314: ["Orliaguet", 44.9107, 1.3815],
    24316: ["Parcoul", 45.1915, 0.0356],
    24317: ["Paulin", 44.9952, 1.3341],
    24318: ["Paunat", 44.9027, 0.8545],
    24319: ["Paussac-et-Saint-Vivien", 45.3463, 0.5395],
    24320: ["Payzac", 45.4219, 1.2266],
    24321: ["Pazayac", 45.1245, 1.3751],
    24322: ["Périgueux", 45.1918, 0.7118],
    24323: ["Petit-Bersac", 45.2695, 0.2272],
    24324: ["Peyrignac", 45.1587, 1.1956],
    24325: ["Peyrillac-et-Millac", 44.8912, 1.4075],
    24326: ["Peyzac-le-Moustier", 44.9822, 1.0799],
    24327: ["Pezuls", 44.9095, 0.8050],
    24328: ["Piégut-Pluviers", 45.6318, 0.6999],
    24329: ["Le Pizou", 45.0367, 0.0721],
    24330: ["Plazac", 45.0437, 1.0381],
    24331: ["Pomport", 44.7950, 0.4129],
    24333: ["Ponteyraud", 45.1884, 0.2399],
    24334: ["Pontours", 44.8272, 0.7719],
    24335: ["Port-Sainte-Foy-et-Ponchapt", 44.8622, 0.2088],
    24336: ["Prats-de-Carlux", 44.8917, 1.3087],
    24337: ["Prats-du-Périgord", 44.6850, 1.0671],
    24338: ["Pressignac-Vicq", 44.8939, 0.7276],
    24339: ["Preyssac-d'Excideuil", 45.3419, 1.1072],
    24340: ["Prigonrieux", 44.8690, 0.4098],
    24341: ["Proissans", 44.9314, 1.2443],
    24343: ["Puymangou", 45.1845, 0.0791],
    24344: ["Puyrenier", 45.4810, 0.4713],
    24345: ["Queyssac", 44.9151, 0.5323],
    24346: ["Quinsac", 45.4362, 0.7013],
    24347: ["Rampieux", 44.7033, 0.8007],
    24348: ["Razac-d'Eymet", 44.6976, 0.4573],
    24349: ["Razac-de-Saussignac", 44.8135, 0.2901],
    24350: ["Razac-sur-l'Isle", 45.1605, 0.6158],
    24351: ["Ribagnac", 44.7574, 0.4855],
    24352: ["Ribérac", 45.2457, 0.3347],
    24353: ["La Rochebeaucourt-et-Argentine", 45.4660, 0.3826],
    24354: ["La Roche-Chalais", 45.1355, 0.0550],
    24355: ["La Roque-Gageac", 44.8177, 1.1986],
    24356: ["Rouffignac-Saint-Cernin-de-Reilhac", 45.0511, 0.9662],
    24357: ["Rouffignac-de-Sigoulès", 44.7794, 0.4443],
    24359: ["Sadillac", 44.7297, 0.4903],
    24360: ["Sagelat", 44.7848, 1.0230],
    24361: ["Saint-Agne", 44.8351, 0.6236],
    24362: ["Sainte-Alvère", 44.9471, 0.8104],
    24363: ["Saint-Amand-de-Belvès", 44.7565, 1.0433],
    24364: ["Saint-Amand-de-Coly", 45.0521, 1.2465],
    24365: ["Saint-Amand-de-Vergt", 44.9939, 0.6957],
    24366: ["Saint-André-d'Allas", 44.8912, 1.1476],
    24367: ["Saint-André-de-Double", 45.1409, 0.3253],
    24368: ["Saint-Antoine-Cumond", 45.2460, 0.2019],
    24369: ["Saint-Antoine-d'Auberoche", 45.1402, 0.9421],
    24370: ["Saint-Antoine-de-Breuilh", 44.8413, 0.1447],
    24371: ["Saint-Aquilin", 45.1911, 0.4887],
    24372: ["Saint-Astier", 45.1484, 0.5196],
    24373: ["Saint-Aubin-de-Cadelech", 44.6857, 0.4920],
    24374: ["Saint-Aubin-de-Lanquais", 44.7969, 0.6021],
    24375: ["Saint-Aubin-de-Nabirat", 44.7319, 1.2843],
    24376: ["Saint-Aulaye", 45.1823, 0.1363],
    24377: ["Saint-Avit-de-Vialard", 44.9453, 0.8621],
    24378: ["Saint-Avit-Rivière", 44.7401, 0.9065],
    24379: ["Saint-Avit-Sénieur", 44.7750, 0.8242],
    24380: ["Saint-Barthélemy-de-Bellegarde", 45.0801, 0.1931],
    24381: ["Saint-Barthélemy-de-Bussière", 45.6415, 0.7470],
    24382: ["Saint-Capraise-de-Lalinde", 44.8463, 0.6552],
    24383: ["Saint-Capraise-d'Eymet", 44.7111, 0.5105],
    24384: ["Saint-Cassien", 44.6794, 0.8453],
    24385: ["Saint-Cernin-de-Labarde", 44.7634, 0.5846],
    24386: ["Saint-Cernin-de-l'Herm", 44.6540, 1.0458],
    24388: ["Saint-Chamassy", 44.8725, 0.9212],
    24389: ["Saint-Cirq", 44.9360, 0.9679],
    24390: ["Saint-Crépin-d'Auberoche", 45.1177, 0.8959],
    24391: ["Saint-Crépin-de-Richemont", 45.4226, 0.6087],
    24392: ["Saint-Crépin-et-Carlucet", 44.9565, 1.2761],
    24393: ["Sainte-Croix", 44.7327, 0.8285],
    24394: ["Sainte-Croix-de-Mareuil", 45.4655, 0.4228],
    24395: ["Saint-Cybranet", 44.7804, 1.1587],
    24396: ["Saint-Cyprien", 44.8825, 1.0253],
    24397: ["Saint-Cyr-les-Champagnes", 45.3724, 1.2910],
    24398: ["Saint-Estèphe", 45.6098, 0.6579],
    24399: ["Saint-étienne-de-Puycorbier", 45.0929, 0.3234],
    24401: ["Sainte-Eulalie-d'Ans", 45.2493, 1.0214],
    24402: ["Sainte-Eulalie-d'Eymet", 44.7093, 0.3593],
    24403: ["Saint-Félix-de-Bourdeilles", 45.4073, 0.5655],
    24404: ["Saint-Félix-de-Reillac-et-Mortemart", 45.0147, 0.8915],
    24405: ["Saint-Félix-de-Villadeix", 44.9319, 0.6772],
    24406: ["Sainte-Foy-de-Belvès", 44.7312, 1.0329],
    24407: ["Sainte-Foy-de-Longas", 44.9286, 0.7556],
    24408: ["Saint-Front-d'Alemps", 45.3245, 0.7897],
    24409: ["Saint-Front-de-Pradoux", 45.0595, 0.3601],
    24410: ["Saint-Front-la-Rivière", 45.4652, 0.7196],
    24411: ["Saint-Front-sur-Nizonne", 45.4774, 0.6273],
    24412: ["Saint-Geniès", 44.9950, 1.2443],
    24413: ["Saint-Georges-Blancaneix", 44.9178, 0.3562],
    24414: ["Saint-Georges-de-Montclard", 44.9331, 0.6108],
    24415: ["Saint-Géraud-de-Corps", 44.9478, 0.2393],
    24416: ["Saint-Germain-de-Belvès", 44.8060, 1.0360],
    24417: ["Saint-Germain-des-Prés", 45.3458, 1.0001],
    24418: ["Saint-Germain-du-Salembre", 45.1279, 0.4377],
    24419: ["Saint-Germain-et-Mons", 44.8286, 0.5912],
    24420: ["Saint-Géry", 44.9760, 0.3260],
    24421: ["Saint-Geyrac", 45.0745, 0.9070],
    24422: ["Saint-Hilaire-d'Estissac", 45.0144, 0.5053],
    24423: ["Sainte-Innocence", 44.7249, 0.3942],
    24424: ["Saint-Jean-d'Ataux", 45.1344, 0.3943],
    24425: ["Saint-Jean-de-Côle", 45.4135, 0.8353],
    24426: ["Saint-Jean-d'Estissac", 45.0375, 0.5048],
    24427: ["Saint-Jean-d'Eyraud", 44.9558, 0.4549],
    24428: ["Saint-Jory-de-Chalais", 45.4894, 0.8970],
    24429: ["Saint-Jory-las-Bloux", 45.3421, 0.9470],
    24430: ["Saint-Julien-de-Bourdeilles", 45.3610, 0.5844],
    24431: ["Saint-Julien-de-Crempse", 44.9499, 0.5219],
    24432: ["Saint-Julien-de-Lampon", 44.8619, 1.3818],
    24433: ["Saint-Julien-d'Eymet", 44.7219, 0.4346],
    24434: ["Saint-Just", 45.3354, 0.4984],
    24435: ["Saint-Laurent-des-Bâtons", 44.9576, 0.7316],
    24436: ["Saint-Laurent-des-Hommes", 45.0440, 0.2532],
    24437: ["Saint-Laurent-des-Vignes", 44.8237, 0.4459],
    24438: ["Saint-Laurent-la-Vallée", 44.7515, 1.1156],
    24439: ["Saint-Laurent-sur-Manoire", 45.1400, 0.7955],
    24441: ["Saint-Léon-d'Issigeac", 44.7210, 0.6993],
    24442: ["Saint-Léon-sur-l'Isle", 45.1169, 0.4995],
    24443: ["Saint-Léon-sur-Vézère", 45.0180, 1.0803],
    24444: ["Saint-Louis-en-l'Isle", 45.0621, 0.3885],
    24445: ["Saint-Marcel-du-Périgord", 44.9163, 0.7034],
    24446: ["Saint-Marcory", 44.7229, 0.9279],
    24447: ["Sainte-Marie-de-Chignac", 45.1154, 0.8255],
    24448: ["Saint-Martial-d'Albarède", 45.3214, 1.0371],
    24449: ["Saint-Martial-d'Artenset", 45.0016, 0.2170],
    24450: ["Saint-Martial-de-Nabirat", 44.7473, 1.2478],
    24451: ["Saint-Martial-de-Valette", 45.5124, 0.6335],
    24452: ["Saint-Martial-Viveyrol", 45.3606, 0.3364],
    24453: ["Saint-Martin-de-Fressengeas", 45.4533, 0.8417],
    24454: ["Saint-Martin-de-Gurson", 44.9613, 0.1162],
    24455: ["Saint-Martin-de-Ribérac", 45.2201, 0.3618],
    24456: ["Saint-Martin-des-Combes", 44.9646, 0.6209],
    24457: ["Saint-Martin-l'Astier", 45.0589, 0.3232],
    24458: ["Saint-Martin-le-Pin", 45.5525, 0.6258],
    24459: ["Saint-Maime-de-Péreyrol", 45.0192, 0.6548],
    24460: ["Saint-Méard-de-Drône", 45.2508, 0.4099],
    24461: ["Saint-Méard-de-Gurçon", 44.9112, 0.1779],
    24462: ["Saint-Médard-de-Mussidan", 45.0220, 0.3318],
    24463: ["Saint-Médard-d'Excideuil", 45.3470, 1.0861],
    24464: ["Saint-Mesmin", 45.3466, 1.2364],
    24465: ["Saint-Michel-de-Double", 45.0913, 0.2749],
    24466: ["Saint-Michel-de-Montaigne", 44.8790, 0.0249],
    24468: ["Saint-Michel-de-Villadeix", 44.9924, 0.7314],
    24470: ["Sainte-Mondane", 44.8354, 1.3467],
    24471: ["Sainte-Nathalène", 44.9135, 1.2809],
    24472: ["Saint-Nexans", 44.8061, 0.5503],
    24473: ["Sainte-Orse", 45.2054, 1.0731],
    24474: ["Saint-Pancrace", 45.4239, 0.6653],
    24475: ["Saint-Pantaly-d'Ans", 45.2448, 0.9939],
    24476: ["Saint-Pantaly-d'Excideuil", 45.3120, 1.0114],
    24477: ["Saint-Pardoux-de-Drône", 45.2231, 0.4190],
    24478: ["Saint-Pardoux-et-Vielvic", 44.7703, 0.9609],
    24479: ["Saint-Pardoux-la-Rivière", 45.5030, 0.7480],
    24480: ["Saint-Paul-de-Serre", 45.0809, 0.6339],
    24481: ["Saint-Paul-la-Roche", 45.4833, 0.9894],
    24482: ["Saint-Paul-Lizonne", 45.3169, 0.2832],
    24483: ["Saint-Perdoux", 44.7366, 0.5299],
    24484: ["Saint-Pierre-de-Chignac", 45.1108, 0.8632],
    24485: ["Saint-Pierre-de-Côle", 45.3807, 0.8076],
    24486: ["Saint-Pierre-de-Frugie", 45.5841, 1.0063],
    24487: ["Saint-Pierre-d'Eyraud", 44.8735, 0.3221],
    24488: ["Saint-Pompont", 44.7148, 1.1358],
    24489: ["Saint-Priest-les-Fougères", 45.5411, 1.0243],
    24490: ["Saint-Privat-des-Prés", 45.2186, 0.1922],
    24491: ["Saint-Rabier", 45.1748, 1.1455],
    24492: ["Sainte-Radegonde", 44.6845, 0.6776],
    24493: ["Saint-Raphaël", 45.3040, 1.0731],
    24494: ["Saint-Rémy", 44.9555, 0.1794],
    24495: ["Saint-Romain-de-Monpazier", 44.7199, 0.8820],
    24496: ["Saint-Romain-et-Saint-Clément", 45.4243, 0.8720],
    24497: ["Sainte-Sabine-Born", 44.6986, 0.7317],
    24498: ["Saint-Saud-Lacoussière", 45.5365, 0.8396],
    24499: ["Saint-Sauveur", 44.8734, 0.5839],
    24500: ["Saint-Sauveur-Lalande", 44.9770, 0.2526],
    24501: ["Saint-Seurin-de-Prats", 44.8322, 0.0683],
    24502: ["Saint-Séverin-d'Estissac", 45.0508, 0.4734],
    24503: ["Saint-Sulpice-de-Mareuil", 45.4663, 0.5217],
    24504: ["Saint-Sulpice-de-Roumagnac", 45.1997, 0.3963],
    24505: ["Saint-Sulpice-d'Excideuil", 45.3955, 1.0172],
    24507: ["Sainte-Trie", 45.2931, 1.2124],
    24508: ["Saint-Victor", 45.2628, 0.4426],
    24509: ["Saint-Vincent-de-Connezac", 45.1654, 0.3922],
    24510: ["Saint-Vincent-de-Cosse", 44.8391, 1.1095],
    24511: ["Saint-Vincent-Jalmoutiers", 45.1841, 0.1936],
    24512: ["Saint-Vincent-le-Paluel", 44.8833, 1.2754],
    24513: ["Saint-Vincent-sur-l'Isle", 45.2416, 0.8997],
    24514: ["Saint-Vivien", 44.8935, 0.1032],
    24515: ["Salagnac", 45.3155, 1.2118],
    24516: ["Salignac-Eyvigues", 44.9451, 1.3594],
    24517: ["Salles-de-Belvès", 44.7165, 0.9989],
    24518: ["Salon", 45.0281, 0.7748],
    24519: ["Sarlande", 45.4503, 1.1155],
    24520: ["Sarlat-la-Canéda", 44.8983, 1.2066],
    24521: ["Sarliac-sur-l'Isle", 45.2398, 0.8663],
    24522: ["Sarrazac", 45.4378, 1.0348],
    24523: ["Saussignac", 44.8026, 0.3203],
    24524: ["Savignac-de-Miremont", 44.9640, 0.9466],
    24525: ["Savignac-de-Nontron", 45.5479, 0.7182],
    24526: ["Savignac-Lédrier", 45.3819, 1.1986],
    24527: ["Savignac-les-églises", 45.2753, 0.9117],
    24528: ["Sceau-Saint-Angel", 45.4836, 0.6739],
    24529: ["Segonzac", 45.2018, 0.4389],
    24530: ["Sencenac-Puy-de-Fourches", 45.3155, 0.6816],
    24531: ["Sergeac", 44.9987, 1.1154],
    24532: ["Serres-et-Montguyard", 44.6757, 0.4442],
    24533: ["Servanches", 45.1333, 0.1647],
    24534: ["Sigoulès", 44.7578, 0.3989],
    24535: ["Simeyrols", 44.9148, 1.3387],
    24536: ["Singleyrac", 44.7349, 0.4624],
    24537: ["Siorac-de-Ribérac", 45.1835, 0.3463],
    24538: ["Siorac-en-Périgord", 44.8168, 0.9793],
    24540: ["Sorges", 45.2917, 0.8499],
    24541: ["Soudat", 45.6218, 0.5679],
    24542: ["Soulaures", 44.6413, 0.9148],
    24543: ["Sourzac", 45.0492, 0.4154],
    24544: ["Tamniès", 44.9766, 1.1496],
    24545: ["Teillots", 45.2592, 1.2194],
    24546: ["Temple-Laguyon", 45.2312, 1.0972],
    24547: ["Terrasson-Lavilledieu", 45.1181, 1.2991],
    24548: ["Teyjat", 45.5905, 0.5795],
    24549: ["Thénac", 44.7458, 0.3548],
    24550: ["Thenon", 45.1324, 1.0636],
    24551: ["Thiviers", 45.4286, 0.9121],
    24552: ["Thonac", 45.0373, 1.1056],
    24553: ["Tocane-Saint-Apre", 45.2373, 0.4969],
    24554: ["La Tour-Blanche", 45.3671, 0.4269],
    24555: ["Tourtoirac", 45.2725, 1.0555],
    24557: ["Trélissac", 45.2097, 0.7784],
    24558: ["Trémolat", 44.8747, 0.8339],
    24559: ["Tursac", 44.9720, 1.0423],
    24560: ["Urval", 44.7991, 0.9408],
    24561: ["Valeuil", 45.3279, 0.6280],
    24562: ["Vallereuil", 45.0715, 0.4995],
    24563: ["Valojoulx", 45.0168, 1.1478],
    24564: ["Vanxains", 45.2121, 0.2897],
    24565: ["Varaignes", 45.6108, 0.5303],
    24566: ["Varennes", 44.8341, 0.6681],
    24567: ["Vaunac", 45.3698, 0.8676],
    24568: ["Vélines", 44.8569, 0.1121],
    24569: ["Vendoire", 45.4123, 0.2888],
    24570: ["Verdon", 44.8143, 0.6304],
    24571: ["Vergt", 45.0414, 0.7103],
    24572: ["Vergt-de-Biron", 44.6363, 0.8494],
    24573: ["Verteillac", 45.3499, 0.3760],
    24574: ["Veyrignac", 44.8228, 1.3254],
    24575: ["Veyrines-de-Domme", 44.8003, 1.1057],
    24576: ["Veyrines-de-Vergt", 44.9902, 0.7732],
    24577: ["Vézac", 44.8405, 1.1729],
    24579: ["Vieux-Mareuil", 45.4339, 0.5014],
    24580: ["Villac", 45.1842, 1.2482],
    24581: ["Villamblard", 45.0273, 0.5579],
    24582: ["Villars", 45.4222, 0.7665],
    24584: ["Villefranche-de-Lonchat", 44.9555, 0.0588],
    24585: ["Villefranche-du-Périgord", 44.6334, 1.1074],
    24586: ["Villetoureix", 45.2729, 0.3590],
    24587: ["Vitrac", 44.8394, 1.2244]
};
