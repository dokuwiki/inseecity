<?php
/**
 * @license    http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html
 * @author     Francois Merciol <dokuplugin@merciol.fr>
 *
 * INSEE city: code database
 */

if (!defined ('DOKU_INC'))
    define ('DOKU_INC', realpath (__DIR__.'/../../../').'/');
if (!defined ('DOKU_PLUGIN'))
    define ('DOKU_PLUGIN', DOKU_INC.'lib/plugins/');
require_once(DOKU_PLUGIN.'syntax.php');

// ============================================================
class syntax_plugin_inseecity extends DokuWiki_Syntax_Plugin {
 
    // ============================================================
    function getType () { return 'substition'; }
    function getPType () { return 'block'; }
    function getSort () { return 299; }
    function connectTo ($mode) {
        $this->Lexer->addSpecialPattern ('\{\{inseecity[^}]*\}\}', $mode, 'plugin_inseecity');
    }

    // ============================================================
    function handle ($match, $state, $pos, Doku_Handler $handler) {
        switch ($state) {
        case DOKU_LEXER_SPECIAL :
            return array ($state, trim (substr ($match, 11, -2))); // "{{inseecity" => 11 "}}" => 2
        }
        return false;
    }

    // ============================================================
    function render ($mode, Doku_Renderer $renderer, $indata) {
        $dumy = "";
        if (empty($indata))
            return false;
        if ($mode != 'xhtml')
            return false;
        list ($instr, $data) = $indata;
        switch ($instr) {
        case DOKU_LEXER_SPECIAL :
            $args = " ".$data." ";
            if (preg_match_all ("#(\"[^\"]*\")* help (\"[^\"]*\")*#", strtolower ($args), $dumy) > 0) {
                $renderer->doc .= $this->inseeHelp ();
                return true;
            }
            if (preg_match_all ("#(\"[^\"]*\")* test (\"[^\"]*\")*#", strtolower ($args), $dumy) > 0) {
                $renderer->doc .= $this->inseeTest ();
                return true;
            }
            // XXX ???
        }
        return true;
    }

    // ============================================================
    function inseeHelp () {
        $url = "http://admin.parlenet.org/plugins/insee/";
        return
            '<h1>Help INSEE</h1>'.NL.
            '<ul>'.NL.
            ' <li><b>{{inseecity</b> help <b>}}</b></li>'.NL.
            ' <li><b>{{inseecity</b> test <b>}}</b></li>'.NL.
            '</ul>'.NL.
            '<p><a class="urlextern" rel="nofollow" title="'.$url.'" href="'.$url.'">'.$url.'</a></p>'.NL;
    }

    // ============================================================
    function inseeTest () {
        return
            '<div>'.NL.
            ' test <form class="insee">'.NL.
            '  <input type="text" name="city" />'.NL.
            ' </form>'.NL.
            '</div>'.NL;
    }

    // ============================================================
}
