/*
  Sources :
  https://www.insee.fr/fr/information/4316069
  http://www.geonames.org/export/codes.html
  http://download.geonames.org/export/dump/
*/

var inseeCityNameLatLonDep;
if (inseeCityNameLatLonDep === undefined)
    inseeCityNameLatLonDep = new Map ();

inseeCityNameLatLonDep[82] = {
    82001: ["Albefeuille-Lagarde", 44.0482, 1.2739],
    82002: ["Albias", 44.0815, 1.4485],
    82003: ["Angeville", 43.9938, 1.0311],
    82004: ["Asques", 44.0092, 0.9550],
    82005: ["Aucamville", 43.7960, 1.2253],
    82006: ["Auterive", 43.8551, 0.9705],
    82007: ["Auty", 44.1889, 1.4694],
    82008: ["Auvillar", 44.0585, 0.8874],
    82009: ["Balignac", 43.9557, 0.8748],
    82010: ["Bardigues", 44.0297, 0.8915],
    82011: ["Barry-d'Islemade", 44.0727, 1.2530],
    82012: ["Les Barthes", 44.0911, 1.1717],
    82013: ["Beaumont-de-Lomagne", 43.8767, 1.0105],
    82014: ["Beaupuy", 43.8187, 1.1160],
    82015: ["Belbèse", 43.9057, 1.0701],
    82016: ["Belvèze", 44.3272, 1.0903],
    82017: ["Bessens", 43.8878, 1.2705],
    82018: ["Bioule", 44.0992, 1.5433],
    82019: ["Boudou", 44.1038, 1.0157],
    82020: ["Bouillac", 43.8471, 1.1154],
    82021: ["Bouloc", 44.3002, 1.1246],
    82022: ["Bourg-de-Visa", 44.2509, 0.9497],
    82023: ["Bourret", 43.9436, 1.1560],
    82024: ["Brassac", 44.2221, 0.9772],
    82025: ["Bressols", 43.9544, 1.3161],
    82026: ["Bruniquel", 44.0521, 1.6600],
    82027: ["Campsas", 43.8873, 1.3288],
    82028: ["Canals", 43.8610, 1.2993],
    82029: ["Castanet", 44.2603, 1.9295],
    82030: ["Castelferrus", 44.0050, 1.0911],
    82031: ["Castelmayran", 44.0227, 1.0470],
    82032: ["Castelsagrat", 44.1767, 0.9602],
    82033: ["Castelsarrasin", 44.0490, 1.1234],
    82034: ["Castéra-Bouzet", 44.0029, 0.9178],
    82035: ["Caumont", 44.0211, 0.9973],
    82036: ["Le Causé", 43.8022, 0.9722],
    82037: ["Caussade", 44.1565, 1.5435],
    82038: ["Caylus", 44.2420, 1.7536],
    82039: ["Cayrac", 44.1039, 1.4785],
    82040: ["Cayriech", 44.2153, 1.6054],
    82041: ["Cazals", 44.1249, 1.6848],
    82042: ["Cazes-Mondenard", 44.2128, 1.2151],
    82043: ["Comberouger", 43.8719, 1.0928],
    82044: ["Corbarieu", 43.9412, 1.3812],
    82045: ["Cordes-Tolosannes", 43.9758, 1.1358],
    82046: ["Coutures", 43.9600, 0.9878],
    82047: ["Cumont", 43.8703, 0.9033],
    82048: ["Dieupentale", 43.8686, 1.2791],
    82049: ["Donzac", 44.1025, 0.8140],
    82050: ["Dunes", 44.0864, 0.7735],
    82051: ["Durfort-Lacapelette", 44.1832, 1.1414],
    82052: ["Escatalens", 43.9817, 1.1936],
    82053: ["Escazeaux", 43.8360, 1.0295],
    82054: ["Espalais", 44.0763, 0.9141],
    82055: ["Esparsac", 43.9115, 0.9473],
    82056: ["Espinas", 44.1967, 1.7923],
    82057: ["Fabas", 43.8617, 1.3391],
    82058: ["Fajolles", 43.9666, 1.0124],
    82059: ["Faudoas", 43.8335, 0.9673],
    82060: ["Fauroux", 44.2527, 1.0058],
    82061: ["Féneyrols", 44.1415, 1.8252],
    82062: ["Finhan", 43.9126, 1.2192],
    82063: ["Garganvillar", 43.9752, 1.0642],
    82064: ["Gariès", 43.8107, 1.0336],
    82065: ["Gasques", 44.1489, 0.9105],
    82066: ["Génébrières", 44.0012, 1.4904],
    82067: ["Gensac", 43.9483, 0.9593],
    82068: ["Gimat", 43.8656, 0.9401],
    82069: ["Ginals", 44.2198, 1.8783],
    82070: ["Glatens", 43.8944, 0.9198],
    82071: ["Goas", 43.8150, 0.9608],
    82072: ["Golfech", 44.1100, 0.8569],
    82073: ["Goudourville", 44.1130, 0.9254],
    82074: ["Gramont", 43.9426, 0.7879],
    82075: ["Grisolles", 43.8224, 1.2867],
    82076: ["L'Honor-de-Cos", 44.1168, 1.3569],
    82077: ["Labarthe", 44.2091, 1.3168],
    82078: ["Labastide-de-Penne", 44.2817, 1.5919],
    82079: ["Labastide-Saint-Pierre", 43.9190, 1.3464],
    82080: ["Labastide-du-Temple", 44.0810, 1.2023],
    82081: ["Labourgade", 43.9566, 1.0923],
    82082: ["Lacapelle-Livron", 44.2719, 1.7901],
    82083: ["Lachapelle", 43.9851, 0.8415],
    82084: ["Lacour", 44.2848, 0.9650],
    82085: ["Lacourt-Saint-Pierre", 43.9885, 1.2716],
    82086: ["Lafitte", 43.9737, 1.1066],
    82087: ["Lafrançaise", 44.1354, 1.2402],
    82088: ["Laguépie", 44.1633, 1.9584],
    82089: ["Lamagistère", 44.1320, 0.8218],
    82090: ["Lamothe-Capdeville", 44.0932, 1.3897],
    82091: ["Lamothe-Cumont", 43.8829, 0.9139],
    82092: ["Lapenche", 44.2236, 1.5730],
    82093: ["Larrazet", 43.9324, 1.0828],
    82094: ["Lauzerte", 44.2642, 1.1271],
    82095: ["Lavaurette", 44.2073, 1.6699],
    82096: ["La Ville-Dieu-du-Temple", 44.0429, 1.2209],
    82097: ["Lavit", 43.9560, 0.9250],
    82098: ["Léojac", 44.0029, 1.4511],
    82099: ["Lizac", 44.1127, 1.1868],
    82100: ["Loze", 44.2922, 1.7634],
    82101: ["Malause", 44.0899, 0.9719],
    82102: ["Mansonville", 44.0154, 0.8511],
    82103: ["Marignac", 43.8418, 0.9162],
    82104: ["Marsac", 43.9414, 0.8294],
    82105: ["Mas-Grenier", 43.8953, 1.1813],
    82106: ["Maubec", 43.8036, 0.9241],
    82107: ["Maumusson", 43.9122, 0.9061],
    82108: ["Meauzac", 44.0954, 1.2304],
    82109: ["Merles", 44.0647, 0.9666],
    82110: ["Mirabel", 44.1424, 1.4113],
    82111: ["Miramont-de-Quercy", 44.2285, 1.0510],
    82112: ["Moissac", 44.1262, 1.0983],
    82113: ["Molières", 44.1933, 1.3931],
    82114: ["Monbéqui", 43.8913, 1.2322],
    82115: ["Monclar-de-Quercy", 43.9880, 1.5823],
    82116: ["Montagudet", 44.2476, 1.0870],
    82117: ["Montaigu-de-Quercy", 44.3406, 1.0325],
    82118: ["Montaïn", 43.9416, 1.1139],
    82119: ["Montalzat", 44.2084, 1.5172],
    82120: ["Montastruc", 44.1037, 1.2943],
    82121: ["Montauban", 44.0222, 1.3640],
    82122: ["Montbarla", 44.2136, 1.0819],
    82123: ["Montbartier", 43.9148, 1.2866],
    82124: ["Montbeton", 44.0135, 1.2703],
    82125: ["Montech", 43.9524, 1.2376],
    82126: ["Monteils", 44.1717, 1.5653],
    82127: ["Montesquieu", 44.1792, 1.0619],
    82128: ["Montfermier", 44.2279, 1.4156],
    82129: ["Montgaillard", 43.9301, 0.8783],
    82130: ["Montjoi", 44.2043, 0.9301],
    82131: ["Montpezat-de-Quercy", 44.2399, 1.4736],
    82132: ["Montricoux", 44.1035, 1.6241],
    82133: ["Mouillac", 44.2755, 1.6730],
    82134: ["Nègrepelisse", 44.0631, 1.5281],
    82135: ["Nohic", 43.8880, 1.4376],
    82136: ["Orgueil", 43.8991, 1.3970],
    82137: ["Parisot", 44.2617, 1.8657],
    82138: ["Perville", 44.1808, 0.8806],
    82139: ["Le Pin", 44.0376, 0.9752],
    82140: ["Piquecos", 44.1044, 1.3163],
    82141: ["Pommevic", 44.0940, 0.9345],
    82142: ["Pompignan", 43.8196, 1.3292],
    82143: ["Poupas", 43.9626, 0.8359],
    82144: ["Puycornet", 44.1546, 1.3219],
    82145: ["Puygaillard-de-Quercy", 44.0245, 1.6316],
    82146: ["Puygaillard-de-Lomagne", 43.9687, 0.8934],
    82147: ["Puylagarde", 44.3066, 1.8463],
    82148: ["Puylaroque", 44.2515, 1.6352],
    82149: ["Réalville", 44.1281, 1.4808],
    82150: ["Reyniès", 43.9261, 1.4145],
    82151: ["Roquecor", 44.3164, 0.9465],
    82152: ["Saint-Aignan", 44.0231, 1.0733],
    82153: ["Saint-Amans-du-Pech", 44.3114, 0.8933],
    82154: ["Saint-Amans-de-Pellagal", 44.2216, 1.1281],
    82155: ["Saint-Antonin-Noble-Val", 44.1560, 1.7364],
    82156: ["Saint-Arroumex", 43.9917, 0.9892],
    82157: ["Saint-Beauzeil", 44.3338, 0.9086],
    82158: ["Saint-Cirice", 44.0616, 0.8388],
    82159: ["Saint-Cirq", 44.1349, 1.6050],
    82160: ["Saint-Clair", 44.1524, 0.9423],
    82161: ["Saint-Etienne-de-Tulmont", 44.0446, 1.4570],
    82162: ["Saint-Georges", 44.2182, 1.6398],
    82163: ["Saint-Jean-du-Bouzet", 43.9866, 0.8757],
    82164: ["Sainte-Juliette", 44.2920, 1.1652],
    82165: ["Saint-Loup", 44.0833, 0.8522],
    82166: ["Saint-Michel", 44.0453, 0.9375],
    82167: ["Saint-Nauphary", 43.9589, 1.4486],
    82168: ["Saint-Nazaire-de-Valentane", 44.2056, 1.0172],
    82169: ["Saint-Nicolas-de-la-Grave", 44.0658, 1.0202],
    82170: ["Saint-Paul-d'Espis", 44.1401, 1.0020],
    82171: ["Saint-Porquier", 44.0077, 1.1822],
    82172: ["Saint-Projet", 44.3109, 1.7708],
    82173: ["Saint-Sardos", 43.9017, 1.1241],
    82174: ["Saint-Vincent", 44.1643, 1.4663],
    82175: ["Saint-Vincent-Lespinasse", 44.1203, 0.9636],
    82176: ["La Salvetat-Belmontet", 43.9666, 1.5127],
    82177: ["Sauveterre", 44.2678, 1.2726],
    82178: ["Savenès", 43.8305, 1.1850],
    82179: ["Septfonds", 44.1765, 1.6173],
    82180: ["Sérignac", 43.9244, 1.0248],
    82181: ["Sistels", 44.0529, 0.7833],
    82182: ["Touffailles", 44.2755, 1.0380],
    82183: ["Tréjouls", 44.2668, 1.2022],
    82184: ["Vaïssac", 44.0316, 1.5706],
    82185: ["Valeilles", 44.3624, 0.9150],
    82186: ["Valence", 44.1068, 0.8979],
    82187: ["Varen", 44.1580, 1.8863],
    82188: ["Varennes", 43.9165, 1.4901],
    82189: ["Vazerac", 44.1952, 1.2765],
    82190: ["Verdun-sur-Garonne", 43.8491, 1.2284],
    82191: ["Verfeil", 44.1847, 1.8746],
    82192: ["Verlhac-Tescou", 43.9330, 1.5327],
    82193: ["Vigueron", 43.8937, 1.0562],
    82194: ["Villebrumier", 43.9202, 1.4512],
    82195: ["Villemade", 44.0801, 1.2899]
};
