<?php
/**
 * @license    http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html
 * @author     Francois Merciol <dokuplugin@merciol.fr>
 *
 * INSEE city: code database
 */
 
if (!defined ('DOKU_INC'))
  die ();
if (!defined ('DOKU_PLUGIN'))
  define ('DOKU_PLUGIN', DOKU_INC.'lib/plugins/');
require_once (DOKU_PLUGIN.'action.php');

class action_plugin_inseecity extends DokuWiki_Action_Plugin {
    var $allDeps;
    var $selectedDeps;

    // ============================================================
    function register (Doku_Event_Handler $controller) {
        $controller->register_hook ('TPL_METAHEADER_OUTPUT', 'BEFORE', $this, 'include_dependencies', array());
        $controller->register_hook ('DOKUWIKI_STARTED', 'AFTER',  $this, 'jsinfo');
    }

    public function include_dependencies (Doku_Event $event, $param) {
        $this->setVars ();
		foreach ($this->selectedDeps as $dep)
            $event->data['script'][] = array (
                'type' => 'text/javascript',
                'src' => DOKU_BASE.'lib/plugins/inseecity/dep/'.$dep.'.js',
                'defer' => 'defer',
                'charset' => 'utf-8',
                '_data' => '',
            );
    }

    // ============================================================
    public function setVars () {
        $pathDirObj = opendir (__DIR__."/dep/");
        $exclude_array = explode ("|", ".|..");
        $this->allDeps = [];
        $this->selectedDeps = [];
        while (false !== ($file = readdir ($pathDirObj))) {
            if (in_array (strtolower ($file), $exclude_array))
                continue;
            $this->allDeps[] = preg_replace ('#(.*)\.js$#i', '$1', $file);
        }
        if ($this->getConf ('allDep'))
            $this->selectedDeps = $this->allDeps;
        else {
            $this->selectedDeps = [];
            foreach (explode (",",  $this->getConf ('selectedDeps')) as $dep) {
                $dep = strtoupper (trim ($dep));
                if (in_array ($dep, $this->allDeps))
                    $this->selectedDeps [] = $dep;
            }
        }
    }

    // ============================================================
    public function jsinfo(Doku_Event $event, $param) {
        $this->setVars ();
        global $JSINFO;
        if (!isset($JSINFO['inseeCity']))
            $JSINFO['inseeCity'] = array();
        $JSINFO ['inseeCity']['selectedDeps'] = $this->selectedDeps;
        $JSINFO ['inseeCity']['maxAutocomplete'] = $this->getConf ('maxAutocomplete');
    }

    // ============================================================
}
