<?php
/**
 * @license    http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html
 * @author     Francois Merciol <dokuplugin@merciol.fr>
 *
 * Options for the INSEE city plugin
 */

$conf['allDep']       = true;
$conf['selectedDeps'] = "";
$conf['maxAutocomplete'] = 200;

