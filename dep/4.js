/*
  Sources :
  https://www.insee.fr/fr/information/4316069
  http://www.geonames.org/export/codes.html
  http://download.geonames.org/export/dump/
*/

var inseeCityNameLatLonDep;
if (inseeCityNameLatLonDep === undefined)
    inseeCityNameLatLonDep = new Map ();

inseeCityNameLatLonDep[4] = {
    4001: ["Aiglun", 44.0638, 6.1384],
    4004: ["Allemagne-en-Provence", 43.7887, 6.0309],
    4005: ["Allons", 43.9803, 6.5903],
    4006: ["Allos", 44.2614, 6.6267],
    4007: ["Angles", 43.9443, 6.5523],
    4008: ["Annot", 43.9620, 6.6639],
    4009: ["Archail", 44.1110, 6.3307],
    4012: ["Aubenas-les-Alpes", 43.9335, 5.6769],
    4013: ["Aubignosc", 44.1182, 5.9573],
    4016: ["Authon", 44.2492, 6.1510],
    4017: ["Auzet", 44.3054, 6.3111],
    4018: ["Banon", 44.0269, 5.6496],
    4019: ["Barcelonnette", 44.3785, 6.6521],
    4020: ["Barles", 44.2730, 6.2532],
    4021: ["Barras", 44.1006, 6.0985],
    4022: ["Barrême", 43.9462, 6.3781],
    4023: ["Bayons", 44.3249, 6.1728],
    4024: ["Beaujeu", 44.2185, 6.3711],
    4025: ["Beauvezer", 44.1439, 6.5977],
    4026: ["Bellaffaire", 44.4043, 6.1942],
    4027: ["Bevons", 44.1779, 5.8789],
    4028: ["Beynes", 43.9834, 6.2402],
    4030: ["Blieux", 43.8746, 6.3627],
    4031: ["Bras-d'Asse", 43.9263, 6.1264],
    4032: ["Braux", 43.9930, 6.6994],
    4033: ["La Bréole", 44.4434, 6.2892],
    4034: ["La Brillanne", 43.9319, 5.8842],
    4035: ["Brunet", 43.8858, 6.0325],
    4036: ["Le Brusquet", 44.1681, 6.3034],
    4037: ["Le Caire", 44.3744, 6.0687],
    4039: ["Castellane", 43.8449, 6.4902],
    4040: ["Le Castellard-Melan", 44.2016, 6.1315],
    4041: ["Le Castellet", 43.9198, 5.9790],
    4042: ["Castellet-lès-Sausses", 44.0403, 6.7345],
    4043: ["Val-de-Chalvagne", 43.9046, 6.7657],
    4045: ["Céreste", 43.8503, 5.5897],
    4046: ["Le Chaffaut-Saint-Jurson", 44.0231, 6.1407],
    4047: ["Champtercier", 44.0969, 6.1445],
    4049: ["Château-Arnoux-Saint-Auban", 44.0854, 5.9923],
    4050: ["Châteaufort", 44.2750, 6.0296],
    4051: ["Châteauneuf-Miravail", 44.1533, 5.7175],
    4053: ["Châteauneuf-Val-Saint-Donat", 44.0954, 5.9305],
    4054: ["Châteauredon", 44.0265, 6.2242],
    4055: ["Chaudon-Norante", 43.9919, 6.3173],
    4057: ["Clamensane", 44.3282, 6.0785],
    4058: ["Claret", 44.3738, 5.9646],
    4059: ["Clumanc", 44.0220, 6.3776],
    4061: ["Colmars", 44.1724, 6.6615],
    4062: ["La Condamine-Châtelard", 44.4627, 6.6962],
    4063: ["Corbières", 43.7606, 5.7459],
    4065: ["Cruis", 44.0831, 5.8384],
    4066: ["Curbans", 44.4083, 6.0300],
    4067: ["Curel", 44.1766, 5.6726],
    4068: ["Dauphin", 43.8932, 5.7764],
    4069: ["Demandolx", 43.8698, 6.5744],
    4070: ["Digne-les-Bains", 44.0908, 6.2359],
    4072: ["Draix", 44.1299, 6.3717],
    4073: ["Enchastrayes", 44.3640, 6.7102],
    4074: ["Entrages", 44.0379, 6.2688],
    4075: ["Entrepierres", 44.1956, 6.0222],
    4076: ["Entrevaux", 43.9502, 6.8023],
    4077: ["Entrevennes", 43.9378, 6.0338],
    4079: ["L'Escale", 44.0810, 6.0345],
    4081: ["Esparron-de-Verdon", 43.7438, 5.9889],
    4084: ["Estoublon", 43.9431, 6.1940],
    4085: ["Faucon-du-Caire", 44.3959, 6.1036],
    4086: ["Faucon-de-Barcelonnette", 44.4135, 6.6786],
    4087: ["Fontienne", 44.0068, 5.7938],
    4088: ["Forcalquier", 43.9599, 5.7882],
    4090: ["Le Fugeret", 44.0145, 6.6656],
    4091: ["Ganagobie", 44.0025, 5.9085],
    4092: ["La Garde", 43.8298, 6.5715],
    4093: ["Gigors", 44.4239, 6.1409],
    4094: ["Gréoux-les-Bains", 43.7676, 5.8649],
    4095: ["L'Hospitalet", 44.1089, 5.7068],
    4096: ["Jausiers", 44.3876, 6.7860],
    4097: ["La Javie", 44.1971, 6.3215],
    4099: ["Lambruisse", 44.0448, 6.4453],
    4100: ["Larche", 44.4272, 6.8726],
    4101: ["Lardiers", 44.0850, 5.7269],
    4102: ["Le Lauzet-Ubaye", 44.4329, 6.4194],
    4104: ["Limans", 43.9866, 5.7236],
    4106: ["Lurs", 43.9698, 5.8804],
    4107: ["Majastres", 43.8980, 6.2836],
    4108: ["Malijai", 44.0336, 6.0504],
    4109: ["Mallefougasse-Augès", 44.0759, 5.8977],
    4110: ["Mallemoisson", 44.0420, 6.1180],
    4111: ["Mane", 43.9388, 5.7423],
    4112: ["Manosque", 43.8354, 5.7910],
    4113: ["Marcoux", 44.1282, 6.2800],
    4115: ["Méailles", 44.0401, 6.6453],
    4116: ["Les Mées", 43.9944, 5.9660],
    4118: ["Melve", 44.3563, 5.9929],
    4120: ["Meyronnes", 44.4730, 6.8111],
    4121: ["Mézel", 43.9980, 6.1787],
    4122: ["Mirabeau", 44.0562, 6.0801],
    4123: ["Mison", 44.2624, 5.8656],
    4124: ["Montagnac-Montpezat", 43.7665, 6.0945],
    4126: ["Montclar", 44.4016, 6.3411],
    4127: ["Montfort", 44.0591, 5.9528],
    4128: ["Montfuron", 43.8315, 5.6885],
    4129: ["Montjustin", 43.8452, 5.6502],
    4130: ["Montlaux", 44.0407, 5.8501],
    4132: ["Montsalier", 44.0299, 5.5957],
    4133: ["Moriez", 43.9640, 6.4625],
    4134: ["La Motte-du-Caire", 44.3408, 6.0305],
    4135: ["Moustiers-Sainte-Marie", 43.8383, 6.2281],
    4136: ["La Mure-Argens", 44.0166, 6.5319],
    4137: ["Nibles", 44.2892, 6.0087],
    4138: ["Niozelles", 43.9318, 5.8473],
    4139: ["Noyers-sur-Jabron", 44.1686, 5.8099],
    4140: ["Les Omergues", 44.1527, 5.5893],
    4141: ["Ongles", 44.0314, 5.7248],
    4142: ["Oppedette", 43.9306, 5.5927],
    4143: ["Oraison", 43.9111, 5.9245],
    4144: ["La Palud-sur-Verdon", 43.7966, 6.3270],
    4145: ["Peipin", 44.1413, 5.9437],
    4148: ["Peyroules", 43.8169, 6.6431],
    4149: ["Peyruis", 44.0351, 5.9283],
    4150: ["Piégut", 44.4533, 6.1317],
    4151: ["Pierrerue", 43.9623, 5.8360],
    4152: ["Pierrevert", 43.8065, 5.7227],
    4154: ["Pontis", 44.4956, 6.3652],
    4155: ["Prads-Haute-Bléone", 44.2134, 6.4637],
    4156: ["Puimichel", 43.9791, 6.0253],
    4157: ["Puimoisson", 43.8708, 6.1220],
    4158: ["Quinson", 43.7080, 6.0226],
    4159: ["Redortiers", 44.0981, 5.6141],
    4160: ["Reillanne", 43.8861, 5.6593],
    4161: ["Méolans-Revel", 44.3714, 6.4984],
    4162: ["Revest-des-Brousses", 43.9754, 5.6750],
    4163: ["Revest-du-Bion", 44.0940, 5.5405],
    4164: ["Revest-Saint-Martin", 44.0198, 5.8229],
    4166: ["Riez", 43.8284, 6.0823],
    4167: ["La Robine-sur-Galabre", 44.1927, 6.2303],
    4169: ["La Rochegiron", 44.1094, 5.6638],
    4170: ["La Rochette", 43.9122, 6.8807],
    4171: ["Rougon", 43.7966, 6.3931],
    4172: ["Roumoules", 43.8210, 6.1486],
    4173: ["Saint-André-les-Alpes", 43.9713, 6.5014],
    4174: ["Saint-Benoît", 43.9629, 6.7238],
    4175: ["Sainte-Croix-à-Lauze", 43.9042, 5.6156],
    4176: ["Sainte-Croix-du-Verdon", 43.7642, 6.1515],
    4177: ["Hautes-Duyes", 44.1949, 6.1742],
    4178: ["Saint-étienne-les-Orgues", 44.0670, 5.7804],
    4179: ["Saint-Geniez", 44.2326, 6.0626],
    4180: ["Saint-Jacques", 43.9769, 6.3712],
    4181: ["Saint-Jeannet", 43.9793, 6.1180],
    4182: ["Saint-Julien-d'Asse", 43.9266, 6.0780],
    4183: ["Saint-Julien-du-Verdon", 43.9097, 6.5460],
    4184: ["Saint-Jurs", 43.8990, 6.1928],
    4186: ["Saint-Laurent-du-Verdon", 43.7216, 6.0685],
    4187: ["Saint-Lions", 43.9786, 6.4062],
    4188: ["Saint-Maime", 43.9006, 5.8015],
    4189: ["Saint-Martin-de-Brômes", 43.7774, 5.9625],
    4190: ["Saint-Martin-les-Eaux", 43.8719, 5.7413],
    4191: ["Saint-Martin-lès-Seyne", 44.3827, 6.2555],
    4192: ["Saint-Michel-l'Observatoire", 43.9098, 5.7200],
    4193: ["Saint-Paul-sur-Ubaye", 44.5588, 6.8207],
    4194: ["Saint-Pierre", 43.9088, 6.9218],
    4195: ["Saint-Pons", 44.4172, 6.6150],
    4197: ["Sainte-Tulle", 43.7830, 5.7730],
    4198: ["Saint-Vincent-les-Forts", 44.4374, 6.3688],
    4199: ["Saint-Vincent-sur-Jabron", 44.1697, 5.7479],
    4200: ["Salignac", 44.1571, 5.9882],
    4201: ["Saumane", 44.0812, 5.6821],
    4202: ["Sausses", 44.0122, 6.7793],
    4203: ["Selonnet", 44.3717, 6.2937],
    4204: ["Senez", 43.9224, 6.3690],
    4205: ["Seyne", 44.3390, 6.3848],
    4206: ["Sigonce", 44.0045, 5.8565],
    4207: ["Sigoyer", 44.3223, 5.9610],
    4208: ["Simiane-la-Rotonde", 43.9838, 5.5624],
    4209: ["Sisteron", 44.2001, 5.9308],
    4210: ["Soleilhas", 43.8599, 6.6486],
    4211: ["Sourribes", 44.1588, 6.0443],
    4214: ["Tartonne", 44.0784, 6.3839],
    4216: ["Thèze", 44.3297, 5.9250],
    4217: ["Thoard", 44.1473, 6.1240],
    4218: ["Thorame-Basse", 44.1071, 6.4893],
    4219: ["Thorame-Haute", 44.0855, 6.6012],
    4220: ["Les Thuiles", 44.3903, 6.5613],
    4222: ["Turriers", 44.3915, 6.1591],
    4224: ["Ubraye", 43.9070, 6.6871],
    4226: ["Uvernet-Fours", 44.3191, 6.6702],
    4227: ["Vachères", 43.9467, 5.6352],
    4228: ["Valavoire", 44.2795, 6.0761],
    4229: ["Valbelle", 44.1371, 5.8762],
    4230: ["Valensole", 43.8380, 5.9419],
    4231: ["Valernes", 44.2515, 5.9695],
    4233: ["Vaumeilh", 44.2903, 5.9565],
    4234: ["Venterol", 44.4423, 6.0873],
    4235: ["Verdaches", 44.2677, 6.3433],
    4236: ["Vergons", 43.9152, 6.6046],
    4237: ["Le Vernet", 44.2766, 6.4029],
    4240: ["Villars-Colmars", 44.1982, 6.5687],
    4241: ["Villemus", 43.8613, 5.7093],
    4242: ["Villeneuve", 43.8957, 5.8573],
    4244: ["Volonne", 44.1239, 6.0346],
    4245: ["Volx", 43.8706, 5.8315]
};
