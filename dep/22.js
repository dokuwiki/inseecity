/*
  Sources :
  https://www.insee.fr/fr/information/4316069
  http://www.geonames.org/export/codes.html
  http://download.geonames.org/export/dump/
*/

var inseeCityNameLatLonDep;
if (inseeCityNameLatLonDep === undefined)
    inseeCityNameLatLonDep = new Map ();

inseeCityNameLatLonDep[22] = {
    22001: ["Allineuc", 48.31083, -2.8725],
    22002: ["Andel", 48.49, -2.56778],
    22003: ["Aucaleuc", 48.45611, -2.12972],
    22004: ["Bégard", 48.62806, -3.30083],
    22005: ["Belle-Isle-en-Terre", 48.54472, -3.39444],
    22006: ["Berhet", 48.69639, -3.30417],
    22007: ["Binic", 48.60194, -2.82528],
    22008: ["Bobital", 48.41333, -2.10306],
    22009: ["Le Bodéo", 48.32222, -2.93333],
    22011: ["Boqueho", 48.48306, -2.96111],
    22012: ["La Bouillie", 48.57389, -2.43528],
    22013: ["Bourbriac", 48.4731, -3.1879],
    22014: ["Bourseul", 48.48722, -2.25972],
    22015: ["Bréhand", 48.40278, -2.57417],
    22016: ["Île-de-Bréhat", 48.8466, -2.9989],
    22018: ["Brélidy", 48.65944, -3.21806],
    22019: ["Bringolo", 48.57611, -3.0025],
    22020: ["Broons", 48.31722, -2.26083],
    22021: ["Brusvily", 48.39083, -2.12722],
    22023: ["Bulat-Pestivien", 48.42861, -3.33056],
    22024: ["Calanhel", 48.43598, -3.47975],
    22025: ["Callac", 48.40417, -3.42778],
    22026: ["Calorguen", 48.4101, -2.0278],
    22027: ["Le Cambout", 48.05889, -2.61056],
    22028: ["Camlez", 48.77778, -3.30472],
    22029: ["Canihuel", 48.33806, -3.10528],
    22030: ["Caouënnec-Lanvézéac", 48.7035, -3.3751],
    22031: ["Carnoët", 48.36778, -3.52139],
    22032: ["Caulnes", 48.28889, -2.15472],
    22033: ["Caurel", 48.21611, -3.03833],
    22034: ["Cavan", 48.67194, -3.34472],
    22035: ["Les Champs-Géraux", 48.41667, -1.97056],
    22036: ["La Chapelle-Blanche", 48.26583, -2.14472],
    22037: ["La Chapelle-Neuve", 48.4623, -3.4201],
    22038: ["Châtelaudren", 48.54222, -2.97111],
    22039: ["La Chèze", 48.13139, -2.65611],
    22040: ["Coadout", 48.51722, -3.18806],
    22041: ["Coatascorn", 48.67389, -3.25028],
    22042: ["Coatréven", 48.76833, -3.34278],
    22043: ["Coëtlogon", 48.14167, -2.54417],
    22044: ["Coëtmieux", 48.49194, -2.60056],
    22045: ["Cohiniac", 48.46139, -2.94861],
    22046: ["Collinée", 48.30083, -2.52],
    22047: ["Corlay", 48.31694, -3.05722],
    22048: ["Corseul", 48.48194, -2.17],
    22049: ["Créhen", 48.54556, -2.21306],
    22050: ["Dinan", 48.45556, -2.05028],
    22051: ["Dolo", 48.38389, -2.32944],
    22052: ["Duault", 48.36111, -3.43528],
    22053: ["Éréac", 48.27389, -2.3475],
    22054: ["Erquy", 48.63167, -2.46417],
    22055: ["Étables-sur-Mer", 48.6261, -2.8342],
    22056: ["Évran", 48.3825, -1.98083],
    22057: ["Le Faouët", 48.6828, -3.0727],
    22058: ["La Ferrière", 48.14194, -2.60667],
    22059: ["Le Fœil", 48.42861, -2.915],
    22060: ["Gausson", 48.29944, -2.75472],
    22061: ["Glomel", 48.22278, -3.39639],
    22062: ["Gomené", 48.17389, -2.48722],
    22063: ["Gommenec'h", 48.6396, -3.0484],
    22064: ["Gouarec", 48.22667, -3.18083],
    22065: ["Goudelin", 48.60306, -3.01722],
    22066: ["Le Gouray", 48.32722, -2.48861],
    22067: ["Grâces", 48.5559, -3.1843],
    22068: ["Grâce-Uzel", 48.24139, -2.80083],
    22069: ["Guenroc", 48.31722, -2.075],
    22070: ["Guingamp", 48.5622, -3.1501],
    22071: ["Guitté", 48.29639, -2.09444],
    22072: ["Gurunhuel", 48.51667, -3.3],
    22073: ["La Harmoye", 48.33556, -2.96139],
    22074: ["Le Haut-Corlay", 48.32139, -3.05694],
    22075: ["Hémonstoir", 48.15861, -2.83083],
    22076: ["Hénanbihen", 48.56056, -2.37694],
    22077: ["Hénansal", 48.54167, -2.43472],
    22078: ["Hengoat", 48.74306, -3.19833],
    22079: ["Hénon", 48.38472, -2.68361],
    22080: ["L'Hermitage-Lorge", 48.33167, -2.82889],
    22081: ["Hillion", 48.51361, -2.66889],
    22082: ["Le Hinglé", 48.39278, -2.07833],
    22083: ["Illifaut", 48.14611, -2.34833],
    22084: ["Jugon-les-Lacs", 48.4092, -2.3212],
    22085: ["Kerbors", 48.82778, -3.18306],
    22086: ["Kerfot", 48.73639, -3.03],
    22087: ["Kergrist-Moëlou", 48.30944, -3.3175],
    22088: ["Kerien", 48.39, -3.21222],
    22090: ["Kermaria-Sulard", 48.77167, -3.37083],
    22091: ["Kermoroc'h", 48.62167, -3.20667],
    22092: ["Kerpert", 48.37639, -3.13389],
    22093: ["Lamballe", 48.46861, -2.51778],
    22094: ["Lancieux", 48.60778, -2.15],
    22095: ["Landebaëron", 48.63444, -3.20917],
    22096: ["Landébia", 48.51417, -2.33639],
    22097: ["La Landec", 48.43556, -2.17972],
    22098: ["Landéhen", 48.42833, -2.54083],
    22099: ["Lanfains", 48.35306, -2.91417],
    22100: ["Langast", 48.28, -2.66389],
    22101: ["Langoat", 48.75056, -3.28083],
    22102: ["Langourla", 48.285, -2.41583],
    22103: ["Langrolay-sur-Rance", 48.5541, -2.0017],
    22104: ["Languédias", 48.38806, -2.21306],
    22105: ["Languenan", 48.51056, -2.12722],
    22106: ["Langueux", 48.495, -2.7175],
    22107: ["Laniscat", 48.24234, -3.12372],
    22108: ["Lanleff", 48.69278, -3.04444],
    22109: ["Lanloup", 48.71278, -2.96556],
    22110: ["Lanmérin", 48.74139, -3.34972],
    22111: ["Lanmodez", 48.84111, -3.10583],
    22112: ["Lannebert", 48.65806, -3.00917],
    22113: ["Lannion", 48.7325, -3.45528],
    22114: ["Lanrelas", 48.25194, -2.29389],
    22115: ["Lanrivain", 48.34694, -3.21417],
    22116: ["Lanrodec", 48.51611, -3.03083],
    22117: ["Lantic", 48.59889, -2.89806],
    22118: ["Lanvallay", 48.45528, -2.02833],
    22119: ["Lanvellec", 48.61889, -3.53611],
    22121: ["Lanvollon", 48.63, -2.985],
    22122: ["Laurenan", 48.19917, -2.53556],
    22123: ["Léhon", 48.44194, -2.03944],
    22124: ["Lescouët-Gouarec", 48.16, -3.24472],
    22126: ["Le Leslay", 48.42944, -2.96583],
    22127: ["Lézardrieux", 48.7854, -3.106],
    22128: ["Locarn", 48.31972, -3.42222],
    22129: ["Loc-Envel", 48.51639, -3.40917],
    22131: ["Loguivy-Plougras", 48.52194, -3.48778],
    22132: ["Lohuec", 48.46028, -3.52167],
    22133: ["Loscouët-sur-Meu", 48.17778, -2.24194],
    22134: ["Louannec", 48.79389, -3.41056],
    22135: ["Louargat", 48.56556, -3.33889],
    22136: ["Loudéac", 48.1779, -2.7523],
    22137: ["Maël-Carhaix", 48.28333, -3.42444],
    22138: ["Maël-Pestivien", 48.3948, -3.2959],
    22139: ["Magoar", 48.3966, -3.1861],
    22140: ["La Malhoure", 48.39722, -2.49444],
    22141: ["Mantallot", 48.70722, -3.29889],
    22143: ["Matignon", 48.59583, -2.29167],
    22144: ["La Méaugon", 48.49861, -2.83833],
    22145: ["Mégrit", 48.37472, -2.24861],
    22146: ["Mellionnec", 48.17472, -3.29639],
    22147: ["Merdrignac", 48.1925, -2.41444],
    22148: ["Mérillac", 48.25583, -2.39417],
    22149: ["Merléac", 48.27722, -2.89889],
    22150: ["Le Merzer", 48.57583, -3.06833],
    22151: ["Meslin", 48.44472, -2.56778],
    22152: ["Minihy-Tréguier", 48.77528, -3.22806],
    22153: ["Moncontour", 48.35917, -2.63306],
    22154: ["Morieux", 48.52167, -2.60889],
    22155: ["La Motte", 48.235, -2.73222],
    22156: ["Moustéru", 48.51722, -3.23889],
    22157: ["Le Moustoir", 48.26556, -3.50222],
    22158: ["Mûr-de-Bretagne", 48.20028, -2.98611],
    22160: ["Noyal", 48.4475, -2.48722],
    22161: ["Pabu", 48.5875, -3.13611],
    22162: ["Paimpol", 48.7782, -3.0457],
    22163: ["Paule", 48.23611, -3.44528],
    22164: ["Pédernec", 48.59694, -3.27],
    22165: ["Penguily", 48.3723, -2.494],
    22166: ["Penvénan", 48.8115, -3.2941],
    22167: ["Perret", 48.17639, -3.15972],
    22168: ["Perros-Guirec", 48.81333, -3.44333],
    22169: ["Peumerit-Quintin", 48.36083, -3.27278],
    22170: ["Plaine-Haute", 48.44528, -2.85361],
    22171: ["Plaintel", 48.40806, -2.81833],
    22172: ["Plancoët", 48.52306, -2.23417],
    22173: ["Planguenoual", 48.53278, -2.57778],
    22174: ["Pléboulle", 48.60861, -2.33833],
    22175: ["Plédéliac", 48.44917, -2.38778],
    22176: ["Plédran", 48.44583, -2.74611],
    22177: ["Pléguien", 48.63472, -2.94],
    22178: ["Pléhédel", 48.69444, -3.00833],
    22179: ["Fréhel", 48.6279, -2.3653],
    22180: ["Plélan-le-Petit", 48.43417, -2.22],
    22181: ["Plélauff", 48.20639, -3.20972],
    22182: ["Plélo", 48.55639, -2.94667],
    22183: ["Plémet", 48.17694, -2.595],
    22184: ["Plémy", 48.33639, -2.68333],
    22185: ["Plénée-Jugon", 48.36417, -2.40056],
    22186: ["Pléneuf-Val-André", 48.59083, -2.54806],
    22187: ["Plérin", 48.53444, -2.77083],
    22188: ["Plerneuf", 48.51472, -2.885],
    22189: ["Plésidy", 48.4475, -3.12167],
    22190: ["Pleslin-Trigavou", 48.5352, -2.0539],
    22191: ["Plessala", 48.27583, -2.61944],
    22192: ["Plessix-Balisson", 48.5375, -2.1425],
    22193: ["Plestan", 48.42389, -2.44694],
    22194: ["Plestin-les-Grèves", 48.65694, -3.63111],
    22195: ["Pleubian", 48.84167, -3.13972],
    22196: ["Pleudaniel", 48.76611, -3.14444],
    22197: ["Pleudihen-sur-Rance", 48.5109, -1.9513],
    22198: ["Pleumeur-Bodou", 48.77306, -3.51778],
    22199: ["Pleumeur-Gautier", 48.80222, -3.15722],
    22200: ["Pléven", 48.49, -2.32028],
    22201: ["Plévenon", 48.65417, -2.33139],
    22202: ["Plévin", 48.22639, -3.50528],
    22203: ["Plœuc-sur-Lié", 48.3462, -2.7568],
    22204: ["Ploëzal", 48.71639, -3.20278],
    22205: ["Plorec-sur-Arguenon", 48.47972, -2.29722],
    22206: ["Plouagat", 48.53667, -2.99889],
    22207: ["Plouaret", 48.61194, -3.4725],
    22208: ["Plouasne", 48.30111, -2.00722],
    22209: ["Ploubalay", 48.58028, -2.14028],
    22210: ["Ploubazlanec", 48.8, -3.03333],
    22211: ["Ploubezre", 48.70444, -3.44889],
    22212: ["Plouëc-du-Trieux", 48.6738, -3.1906],
    22213: ["Plouër-sur-Rance", 48.52778, -2.00333],
    22214: ["Plouézec", 48.74833, -2.985],
    22215: ["Ploufragan", 48.48944, -2.79583],
    22216: ["Plougonver", 48.48472, -3.37861],
    22217: ["Plougras", 48.51083, -3.56139],
    22218: ["Plougrescant", 48.84028, -3.22861],
    22219: ["Plouguenast", 48.28139, -2.70444],
    22220: ["Plouguernével", 48.24028, -3.25611],
    22221: ["Plouguiel", 48.79722, -3.24083],
    22222: ["Plouha", 48.67556, -2.92944],
    22223: ["Plouisy", 48.57778, -3.18389],
    22224: ["Ploulec'h", 48.7179, -3.5038],
    22225: ["Ploumagoar", 48.54528, -3.1325],
    22226: ["Ploumilliau", 48.68, -3.52389],
    22227: ["Plounérin", 48.56667, -3.54111],
    22228: ["Plounévez-Moëdec", 48.55639, -3.44472],
    22229: ["Plounévez-Quintin", 48.29, -3.23167],
    22231: ["Plourac'h", 48.41611, -3.54722],
    22232: ["Plourhan", 48.6306, -2.8711],
    22233: ["Plourivo", 48.74389, -3.07278],
    22234: ["Plouvara", 48.50694, -2.91528],
    22235: ["Plouzélambre", 48.6425, -3.54194],
    22236: ["Pludual", 48.66444, -2.98417],
    22237: ["Pluduno", 48.53083, -2.26806],
    22238: ["Plufur", 48.60778, -3.57389],
    22239: ["Plumaudan", 48.35778, -2.12472],
    22240: ["Plumaugat", 48.255, -2.23889],
    22241: ["Plumieux", 48.10306, -2.58417],
    22242: ["Plurien", 48.62583, -2.40417],
    22243: ["Plusquellec", 48.38583, -3.48528],
    22244: ["Plussulien", 48.28278, -3.07056],
    22245: ["Pluzunet", 48.64139, -3.36972],
    22246: ["Pommeret", 48.4625, -2.62639],
    22247: ["Pommerit-Jaudy", 48.73194, -3.24222],
    22248: ["Pommerit-le-Vicomte", 48.61917, -3.08833],
    22249: ["Pont-Melvez", 48.4605, -3.3049],
    22250: ["Pontrieux", 48.69833, -3.15944],
    22251: ["Pordic", 48.5703, -2.8171],
    22253: ["Pouldouran", 48.76444, -3.19917],
    22254: ["Prat", 48.67694, -3.2975],
    22255: ["La Prénessaye", 48.18306, -2.63389],
    22256: ["Quemper-Guézennec", 48.70139, -3.10556],
    22257: ["Quemperven", 48.725, -3.32194],
    22258: ["Quessoy", 48.42111, -2.65833],
    22259: ["Quévert", 48.46361, -2.08722],
    22260: ["Le Quillio", 48.24111, -2.88333],
    22261: ["Quintenic", 48.51528, -2.42833],
    22262: ["Quintin", 48.40361, -2.90944],
    22263: ["Le Quiou", 48.35111, -2.00611],
    22264: ["La Roche-Derrien", 48.74639, -3.26],
    22265: ["Rospez", 48.72944, -3.38389],
    22266: ["Rostrenen", 48.23639, -3.31694],
    22267: ["Rouillac", 48.30944, -2.36611],
    22268: ["Ruca", 48.56722, -2.33944],
    22269: ["Runan", 48.69333, -3.21167],
    22271: ["Saint-Adrien", 48.48917, -3.13028],
    22272: ["Saint-Agathon", 48.55917, -3.10472],
    22273: ["Saint-Alban", 48.55722, -2.535],
    22274: ["Saint-André-des-Eaux", 48.37167, -2.01],
    22275: ["Saint-Barnabé", 48.1375, -2.70278],
    22276: ["Saint-Bihy", 48.37889, -2.97056],
    22277: ["Saint-Brandan", 48.38944, -2.87],
    22278: ["Saint-Brieuc", 48.51361, -2.76028],
    22279: ["Saint-Caradec", 48.19083, -2.84917],
    22280: ["Saint-Carné", 48.41583, -2.06556],
    22281: ["Saint-Carreuc", 48.39833, -2.73139],
    22282: ["Saint-Cast-le-Guildo", 48.62944, -2.25778],
    22283: ["Saint-Clet", 48.6625, -3.1325],
    22284: ["Saint-Connan", 48.41806, -3.06389],
    22285: ["Saint-Connec", 48.1775, -2.92139],
    22286: ["Saint-Denoual", 48.52722, -2.40056],
    22287: ["Saint-Donan", 48.46972, -2.88528],
    22288: ["Saint-Étienne-du-Gué-de-l'Isle", 48.10361, -2.64778],
    22289: ["Saint-Fiacre", 48.46139, -3.06083],
    22290: ["Saint-Gelven", 48.22696, -3.09711],
    22291: ["Saint-Gildas", 48.42222, -3.00306],
    22292: ["Saint-Gilles-du-Mené", 48.24806, -2.54806],
    22293: ["Saint-Gilles-les-Bois", 48.64972, -3.10278],
    22294: ["Saint-Gilles-Pligeaux", 48.38, -3.09472],
    22295: ["Saint-Gilles-Vieux-Marché", 48.2436, -2.9737],
    22296: ["Saint-Glen", 48.35944, -2.52333],
    22297: ["Saint-Gouéno", 48.2675, -2.56861],
    22298: ["Saint-Guen", 48.21722, -2.93667],
    22299: ["Saint-Hélen", 48.47111, -1.95889],
    22300: ["Saint-Hervé", 48.27694, -2.8275],
    22302: ["Saint-Jacut-de-la-Mer", 48.59722, -2.19028],
    22303: ["Saint-Jacut-du-Mené", 48.28361, -2.48167],
    22304: ["Saint-Jean-Kerdaniel", 48.56528, -3.02139],
    22305: ["Saint-Jouan-de-l'Isle", 48.2675, -2.15917],
    22306: ["Saint-Judoce", 48.36333, -1.95417],
    22307: ["Saint-Julien", 48.4525, -2.81639],
    22308: ["Saint-Juvat", 48.35333, -2.04389],
    22309: ["Saint-Launeuc", 48.23361, -2.37556],
    22310: ["Saint-Laurent", 48.6189, -3.232],
    22311: ["Saint-Lormel", 48.54611, -2.22944],
    22312: ["Saint-Maden", 48.33056, -2.07722],
    22313: ["Saint-Martin-des-Prés", 48.30694, -2.95444],
    22314: ["Saint-Maudan", 48.11361, -2.77417],
    22315: ["Saint-Maudez", 48.45278, -2.18222],
    22316: ["Saint-Mayeux", 48.25583, -3.00639],
    22317: ["Saint-Méloir-des-Bois", 48.4578, -2.2501],
    22318: ["Saint-Michel-de-Plélan", 48.46611, -2.21528],
    22319: ["Saint-Michel-en-Grève", 48.68333, -3.56389],
    22320: ["Saint-Nicodème", 48.3426, -3.3382],
    22321: ["Saint-Nicolas-du-Pélem", 48.31306, -3.16444],
    22322: ["Saint-Péver", 48.48111, -3.10222],
    22323: ["Saint-Pôtan", 48.55778, -2.29139],
    22324: ["Saint-Quay-Perros", 48.7925, -3.44778],
    22325: ["Saint-Quay-Portrieux", 48.65194, -2.83111],
    22326: ["Saint-Rieul", 48.44222, -2.41944],
    22327: ["Saint-Samson-sur-Rance", 48.49139, -2.03083],
    22328: ["Saint-Servais", 48.3867, -3.3868],
    22330: ["Saint-Thélo", 48.22833, -2.85417],
    22331: ["Sainte-Tréphine", 48.27, -3.15417],
    22332: ["Saint-Trimoël", 48.38611, -2.54889],
    22333: ["Saint-Vran", 48.23778, -2.44222],
    22334: ["Saint-Igeaux", 48.27194, -3.10528],
    22335: ["Senven-Léhart", 48.425, -3.06917],
    22337: ["Sévignac", 48.33306, -2.33889],
    22338: ["Squiffiec", 48.62722, -3.15417],
    22339: ["Taden", 48.47556, -2.01778],
    22340: ["Tonquédec", 48.66917, -3.39528],
    22341: ["Tramain", 48.40111, -2.40194],
    22342: ["Trébédan", 48.39972, -2.17139],
    22343: ["Trébeurden", 48.76944, -3.56806],
    22344: ["Trébrivan", 48.30889, -3.47528],
    22345: ["Trébry", 48.35528, -2.55222],
    22346: ["Trédaniel", 48.35806, -2.61889],
    22347: ["Trédarzec", 48.78694, -3.20028],
    22348: ["Trédias", 48.35778, -2.23694],
    22349: ["Trédrez-Locquémeau", 48.69778, -3.56583],
    22350: ["Tréduder", 48.65056, -3.56222],
    22351: ["Treffrin", 48.29889, -3.51694],
    22352: ["Tréfumel", 48.33806, -2.02611],
    22353: ["Trégastel", 48.81694, -3.51361],
    22354: ["Tréglamus", 48.55694, -3.27472],
    22356: ["Trégomeur", 48.56556, -2.88194],
    22357: ["Trégon", 48.56917, -2.18361],
    22358: ["Trégonneau", 48.61167, -3.16389],
    22359: ["Trégrom", 48.60083, -3.40528],
    22360: ["Trégueux", 48.4911, -2.737],
    22361: ["Tréguidel", 48.60167, -2.94361],
    22362: ["Tréguier", 48.785, -3.2325],
    22363: ["Trélévern", 48.80833, -3.37167],
    22364: ["Trélivan", 48.43333, -2.11667],
    22365: ["Trémargat", 48.33222, -3.2675],
    22366: ["Trémel", 48.60306, -3.61139],
    22367: ["Tréméloir", 48.55556, -2.85833],
    22368: ["Tréméreuc", 48.55833, -2.06556],
    22369: ["Trémeur", 48.3475, -2.26389],
    22370: ["Tréméven", 48.67278, -3.02889],
    22371: ["Trémorel", 48.19917, -2.28889],
    22372: ["Trémuson", 48.52389, -2.84944],
    22373: ["Tréogan", 48.18917, -3.52],
    22375: ["Tressignaux", 48.61667, -2.98333],
    22376: ["Trévé", 48.21333, -2.79556],
    22377: ["Tréveneuc", 48.66389, -2.87],
    22378: ["Trévérec", 48.65472, -3.05917],
    22379: ["Trévou-Tréguignec", 48.815, -3.35861],
    22380: ["Trévron", 48.39139, -2.06306],
    22381: ["Trézény", 48.7575, -3.36611],
    22383: ["Troguéry", 48.75389, -3.22583],
    22384: ["Uzel", 48.27944, -2.84139],
    22385: ["La Vicomté-sur-Rance", 48.4884, -1.981],
    22386: ["Le Vieux-Bourg", 48.38861, -3.00056],
    22387: ["Le Vieux-Marché", 48.60694, -3.44806],
    22388: ["Vildé-Guingalan", 48.43778, -2.15861],
    22389: ["Yffiniac", 48.485, -2.6775],
    22390: ["Yvias", 48.71333, -3.0525],
    22391: ["Yvignac-la-Tour", 48.34833, -2.17583]
};
